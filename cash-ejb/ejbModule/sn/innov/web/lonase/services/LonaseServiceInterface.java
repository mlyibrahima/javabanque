package sn.innov.web.lonase.services;

import java.util.List;

import javax.ejb.Remote;

import sn.innov.web.lonase.entities.ChiffreAffaire;


@Remote
public interface LonaseServiceInterface {
	
	public ChiffreAffaire peutPayer(Long idVendeur);
	public List<ChiffreAffaire> listVendeurs();

}
