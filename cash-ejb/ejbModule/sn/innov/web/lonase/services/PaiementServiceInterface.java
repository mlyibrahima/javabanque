package sn.innov.web.lonase.services;

import java.util.List;

import javax.ejb.Remote;

import sn.innov.web.lonase.entities.Paiement;


@Remote
public interface PaiementServiceInterface {
	
	public Paiement aPayer(Long idVendeur);
	public List<Paiement> listAPayer();

}

