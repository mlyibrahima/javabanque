package sn.innov.web.lonase.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import sn.innov.web.lonase.entities.ChiffreAffaire;
import sn.innov.web.lonase.entities.Paiement;

@Stateless
public class PaiementBean implements PaiementServiceInterface{
	@PersistenceContext
	private EntityManager em;

	@Override
	public Paiement aPayer(Long idVendeur) {
		try {
					
					
			Paiement p=em.find(Paiement.class, idVendeur);
			if(p==null) {
				//throw new RuntimeException("Vendeur introuvable");
				return null;
			}
			return p;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Paiement> listAPayer() {
		Query req=em.createQuery("select p from Paiement p");
		return req.getResultList();
	}

}
