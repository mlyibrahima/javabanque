package sn.innov.web.lonase.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import sn.innov.web.banque.entities.Client;
import sn.innov.web.lonase.entities.ChiffreAffaire;



@Stateless
public class LonaseService implements LonaseServiceInterface{
	@PersistenceContext
	private EntityManager em;

	@Override
	public ChiffreAffaire peutPayer(Long idVendeur) {
		try {
			
			
			ChiffreAffaire ca=em.find(ChiffreAffaire.class, idVendeur);
			if(ca==null) {
				//throw new RuntimeException("Vendeur introuvable");
				return null;
			}
			return ca;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ChiffreAffaire> listVendeurs() {
		Query req=em.createQuery("select ca from ChiffreAffaire ca");
		return req.getResultList();
	}

}
