package sn.innov.web.lonase.entities;

import java.io.Serializable;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.*;
import javax.persistence.*;

@Entity
public class PaiementLots implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
	private Date date;
	private String numTicket;
	private Long terminalDeVente;
	private Long terminalDePaiement;
	private double mise;
	private double gain;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getNumTicket() {
		return numTicket;
	}

	public void setNumTicket(String numTicket) {
		this.numTicket = numTicket;
	}

	public Long getTerminalDeVente() {
		return terminalDeVente;
	}

	public void setTerminalDeVente(Long terminalDeVente) {
		this.terminalDeVente = terminalDeVente;
	}

	public Long getTerminalDePaiement() {
		return terminalDePaiement;
	}

	public void setTerminalDePaiement(Long terminalDePaiement) {
		this.terminalDePaiement = terminalDePaiement;
	}

	public double getMise() {
		return mise;
	}

	public void setMise(double mise) {
		this.mise = mise;
	}

	public double getGain() {
		return gain;
	}

	public void setGain(double gain) {
		this.gain = gain;
	}

	public PaiementLots() {
		
	}
}
