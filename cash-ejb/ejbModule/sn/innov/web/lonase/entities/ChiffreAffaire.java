package sn.innov.web.lonase.entities;

import java.io.Serializable;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.*;
import javax.persistence.*;

@Entity
public class ChiffreAffaire implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long codeVD;
	private String nom;
	private String prenom;
	private String telephone;
	private double montant_CA;
	
	//@OneToMany(mappedBy="client", fetch=FetchType.LAZY,cascade=CascadeType.ALL) 
	//private List<Compte> comptes;
	
	public ChiffreAffaire() {
		
	}

    public Long getCodeVD() {
		return codeVD;
	}

	public void setCodeVD(Long codeVD) {
		this.codeVD = codeVD;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public double getMontant_CA() {
		return montant_CA;
	}

	public void setMontant_CA(double montant_CA) {
		this.montant_CA = montant_CA;
	}

	public ChiffreAffaire(String nom, String prenom, String telephone, double montant_CA) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
		this.montant_CA = montant_CA;
	}


	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
}
