package sn.innov.web.banque.entities;

import java.io.Serializable;
import javax.persistence.*; 


@Entity
public class Operation implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Operation(double montant, TypeOperation type, Compte compte) {
		super();
		this.montant = montant;
		this.type = type;
		this.compte = compte;
	}

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long numOperation;
	private double montant;
	@Enumerated(EnumType.STRING)
	private TypeOperation type;
	@ManyToOne 
	@JoinColumn(name="idCompte") 
	private Compte compte; 

	public Operation() {
		super();
	}
	
	public Operation(TypeOperation type) {
		super();
		this.type = type;
	}
	
	public TypeOperation getType() {
		return type;
	}

	public void setType(TypeOperation type) {
		this.type = type;
	}
	
	public Long getNumOperation() {
		return numOperation;
	}

	public void setNumOperation(Long numOperation) {
		this.numOperation = numOperation;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}

	public Compte getCompte() {
		return compte;
	}

	public void setCompte(Compte compte) {
		this.compte = compte;
	}
}
