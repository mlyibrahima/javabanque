package sn.innov.web.banque.entities;

import java.io.Serializable;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.*;
import javax.persistence.*;

@Entity
public class Client implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long idClient;
	private String nom;
	private String prenom;
	private String numTelephone;
	private String adresse;
	
	@OneToMany(mappedBy="client", fetch=FetchType.LAZY,cascade=CascadeType.ALL) 
	private List<Compte> comptes; 
	
	public Client() {
		
	}

    public Client(String nom, String prenom, String numTelephone, String adresse) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.numTelephone = numTelephone;
		this.adresse = adresse;
	}


	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNumTelephone() {
		return numTelephone;
	}

	public void setNumTelephone(String numTelephone) {
		this.numTelephone = numTelephone;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public Long getidClient() {
        return idClient;
    }

    public void setidClient(Long idClient) {
        this.idClient = idClient;
    }
}
