package sn.innov.web.banque.entities;

import java.io.Serializable;

import javax.persistence.*; 
import javax.persistence.Entity;

@Entity 
public class Compte implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long idCompte;
	@Enumerated(EnumType.STRING)
	private TypeCompte type;
	private double solde;
	private String numCompte;
	
	
	@ManyToOne 
	@JoinColumn(name="idClient") 
	private Client client;
	
	public Compte() {
		
	}
	public Compte(TypeCompte type) {
		super();
		this.type = type;
	}
	
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public Long getidCompte() {
		return idCompte;
	}
	public void setidCompte(Long idCompte) {
		this.idCompte = idCompte;
	}
	public TypeCompte getType() {
		return type;
	}
	public void setType(TypeCompte type) {
		this.type = type;
	}
	public String getNumCompte() {
		return numCompte;
	}
	public void setNumCompte(String numCompte) {
		this.numCompte = numCompte;
	}

}
