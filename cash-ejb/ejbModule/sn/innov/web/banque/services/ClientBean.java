package sn.innov.web.banque.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import sn.innov.web.banque.entities.Client;
import sn.innov.web.banque.entities.Compte;
import sn.innov.web.banque.entities.Operation;
import sn.innov.web.banque.entities.TypeOperation;

/**
 * Session Bean implementation class ClientRemote
 */
@Stateless
public class ClientBean implements ClientRemote{
	@PersistenceContext
	private EntityManager em;

	
	/* CLIENT */
	@Override
	public void addClient(Client c) {
		em.persist(c);
	}
	@Override
	public List<Client> consulterClientsParNom(String mc) {
		Query req=em.createQuery("select c from Client c where c.nom = :nom"); 
		req.setParameter("nom", mc);
		return req.getResultList();
	}
	@Override
	public Client consulterClient(Long idClient) {
		try {
			
	
			Client cp=em.find(Client.class, idClient);
			if(cp==null) {
				//throw new RuntimeException("Client introuvable");
				return null;
			}
			return cp;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	@Override
	public List<Client> consulterClients() {
		Query req=em.createQuery("select c from Client c");
		return req.getResultList();
	}
	@Override
	public void supprimerClient(Long idClient) {
			Client c = consulterClient(idClient);
			em.remove(c);
	}

	/* COMPTE */
	@Override
	public void addCompte(Compte c) {
		em.persist(c);
	}
	
	@Override
	public Compte consulterCompte(Long idCompte) {
		try {
			Compte cp=em.find(Compte.class, idCompte);
			if(cp==null) {
				//throw new RuntimeException("Client introuvable");
				return null;
			}
			return cp;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		
		}
	}
	@Override
	public List<Compte> consulterComptes() {
		Query req=em.createQuery("select c from Compte c");
		return req.getResultList();
	}
	@Override
	public List<Compte> consulterComptes(Long idClient) {
		Query req=em.createQuery("select c from Compte c where c.client = :id");
		req.setParameter("id", idClient);
		return req.getResultList();
	}
	@Override
	public void verser(Long idCompte, double montant) {
		Compte cp = em.find(Compte.class, idCompte);
		cp.setSolde(cp.getSolde()+montant);
		em.merge(cp);
		Operation o = new Operation(montant, TypeOperation.DEPOT, cp);
		addOperation(o);
	}
	@Override
    public void retirer(Long idCompte, double montant) {
    	Compte cp = em.find(Compte.class, idCompte);
    	if(cp.getSolde()<montant) throw new RuntimeException("Solde insufisant");
		cp.setSolde(cp.getSolde()-montant);
		em.merge(cp);
		Operation o = new Operation(montant, TypeOperation.RETRAIT, cp);
		addOperation(o);
    }

	/* OPERATION */
	@Override
	public List<Operation> consulterOperations() {
		Query req=em.createQuery("select o from Operation o");
		return req.getResultList();
	}
	@Override
	public void virement(Compte compte1, Compte compte2, Double montant) {
		retirer(compte1.getidCompte(), montant);
		verser(compte2.getidCompte(), montant);
	}
	
	/*
	 * @Override public void prelevement(Compte compte1, Compte compte2, Double
	 * montant) { if(compte2.getSolde() >= montant) { Operation o = new
	 * Operation(montant, TypeOperation.PRELEVEMENT, compte1); addOperation(o);
	 * compte2.setSolde(compte2.getSolde()-montant);
	 * compte1.setSolde(compte1.getSolde()+montant); } }
	 */
	@Override
	public Client modifierClient(Client client) {
		em.merge(client);
		return client;
	}
	@Override
	public void supprimerCompte(Long idCompte) {
		Compte c = consulterCompte(idCompte);
		em.remove(c);		
	}
	@Override
	public Compte modifierCompte(Compte compte) {
		em.merge(compte);
		return compte;
	}
	
	@Override
	public void addOperation(Operation o) {
		em.persist(o);
		
	}
}