package sn.innov.web.banque.services;

import java.util.List;

import javax.ejb.Remote;

import sn.innov.web.banque.entities.Client;
import sn.innov.web.banque.entities.Compte;
import sn.innov.web.banque.entities.Operation;
import sn.innov.web.banque.entities.TypeCompte;

@Remote
public interface ClientRemote {

	
	/* CLIENT */
    public void addClient(Client c); 
    public List<Client> consulterClientsParNom(String mc); 
    public Client consulterClient(Long idClient);
    public List<Client> consulterClients();
    public void supprimerClient(Long idClient);
    public Client modifierClient(Client client);
    
    /* COMPTE */
    public void addCompte(Compte c); 
    public Compte consulterCompte(Long idCompte);
    public List<Compte> consulterComptes();
    public List<Compte> consulterComptes(Long idClient);
    public void verser(Long idCompte, double montant);
    public void retirer(Long idCompte, double montant);
    public void supprimerCompte(Long idCompte);
    public Compte modifierCompte(Compte compte);

    /* OPERATION */ 
    public void addOperation(Operation o);
    public List<Operation> consulterOperations();  
    //public List<Operation> consulterOperationsParCompte(TypeCompte type);
    public void virement(Compte compte1, Compte compte2, Double montant);
    //public void prelevement(Compte compte1, Compte compte2, Double montant);
}
