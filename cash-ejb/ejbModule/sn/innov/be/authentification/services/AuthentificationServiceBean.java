package sn.innov.be.authentification.services;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import sn.innov.be.authentification.entities.Fonction;
import sn.innov.be.authentification.entities.SessionUtilisateur;
import sn.innov.be.authentification.entities.Utilisateur;

/**
 * Session Bean implementation class AuthentificationServiceBean
 */
@Stateless
public class AuthentificationServiceBean implements AuthentificationService {

	@PersistenceContext
	EntityManager em;


	@Override
	public SessionUtilisateur login(String login, String password) {
		Utilisateur utilisateur = null;
		try {
			String jpql = "from Utilisateur u where u.login = :login and u.password = :password";
			Query query = em.createQuery(jpql);
			query.setParameter("login", login);
			query.setParameter("password", getEncodedPassword(password));
			utilisateur = (Utilisateur) query.getSingleResult();

			SessionUtilisateur sessionUtilisateur = new SessionUtilisateur();
			sessionUtilisateur.setDebut(new Date());
			sessionUtilisateur.setFin(new Date());
	//		sessionUtilisateur.setUniteOrganisationnelle(utilisateur.);
			sessionUtilisateur.setFonction(utilisateur.getFonction());
			sessionUtilisateur.setUtilisateur(utilisateur);

			em.persist(sessionUtilisateur);
			return sessionUtilisateur;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public Utilisateur findUtilisateurByLoginPassword(String login, String password) {
		Utilisateur utilisateur = null;
		try {
			String jpql = "from Utilisateur u where u.login = :login and u.password = :password";
			Query query = em.createQuery(jpql);
			query.setParameter("login", login);
			query.setParameter("password", getEncodedPassword(password));
			utilisateur = (Utilisateur) query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateur;
	}

	public static String getEncodedPassword(String key) throws NoSuchAlgorithmException {
		byte[] uniqueKey = key.getBytes();
		byte[] hash = null;
		hash = MessageDigest.getInstance("MD5").digest(uniqueKey);
		StringBuffer hashString = new StringBuffer();
		for (int i = 0; i < hash.length; ++i) {
			String hex = Integer.toHexString(hash[i]);
			if (hex.length() == 1) {
				hashString.append('0');
				hashString.append(hex.charAt(hex.length() - 1));
			} else {
				hashString.append(hex.substring(hex.length() - 2));
			}
		}
		return hashString.toString();
	}

	@Override
	public void save(Utilisateur u) {
		em.merge(u);

	}

}
