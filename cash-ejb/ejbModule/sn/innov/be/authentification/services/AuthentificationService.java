package sn.innov.be.authentification.services;

import javax.ejb.Remote;

import sn.innov.be.authentification.entities.SessionUtilisateur;
import sn.innov.be.authentification.entities.Utilisateur;


public interface AuthentificationService {
	
public Utilisateur findUtilisateurByLoginPassword(String login, String password);

public void save(Utilisateur u);

public SessionUtilisateur login(String login, String password);

}
