package sn.innov.be.authentification.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import sn.innov.be.uniteorganisationnelle.entities.UO;


@Entity
@Table(name = "utilisateurs")
public class Utilisateur implements Serializable {
	Long id, compteur;
 	String login, password, nom, prenom, poste, imageProfil  ; 	
	Fonction fonction;
	Boolean supprime = false ;	
	Set<Utilisateur> utilisateursLogges = new HashSet<Utilisateur>() ;	
  	
	@Id
	@Column(name = "usr_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "usr_login")
	public String getLogin() {
		return login;
	}
	
	
	@Column
	public String getImageProfil() {
		return imageProfil;
	}

	public void setImageProfil(String imageProfil) {
		this.imageProfil = imageProfil;
	}

	@Column(name="usr_poste")
	public String getPoste() {
		return poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	@Column(name = "usr_compteur")
	public Long getCompteur() {
		if(compteur == null ) compteur = 0l ;
		return compteur;
	}

	public void setCompteur(Long compteur) {
		this.compteur = compteur;
	}

	@Column(name = "usr_password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "usr_nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Column(name = "usr_prenom")
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "usr_fct_code")
	public Fonction getFonction() {
		return fonction;
	}

	public void setFonction(Fonction fonction) {
		this.fonction = fonction;
	}
	

	@Column( name = "usr_supprime")
	public Boolean getSupprime() {
		return supprime;
	}

	public void setSupprime(Boolean supprime) {
		this.supprime = supprime;
	}

	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name = "utilisateurs_logges", 
		joinColumns = { @JoinColumn(name = "ul_controleur_id") }, inverseJoinColumns = { @JoinColumn(name = "ul_controle_id") })
	public Set<Utilisateur> getUtilisateursLogges() {
		return utilisateursLogges;
	}

	public void setUtilisateursLogges(Set<Utilisateur> utilisateursLogges) {
		this.utilisateursLogges = utilisateursLogges;
	}

	 
	
	
}
