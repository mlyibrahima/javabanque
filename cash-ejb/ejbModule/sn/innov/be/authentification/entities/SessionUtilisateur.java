package sn.innov.be.authentification.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sn.innov.be.uniteorganisationnelle.entities.UO;

@Entity
@Table(name="SESSIONS_UTILISATEUR")
public class SessionUtilisateur implements Serializable {
Long id;
Date debut,fin;
String media,gsmToken,agent;
LoginMode mode;
Utilisateur utilisateur;
UO uniteOrganisationnelle;
Fonction fonction;
String langue;


@Id
@Column(name="SU_ID")
@GeneratedValue(strategy=GenerationType.AUTO)
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
@Column(name="SU_DEBUT")
@Temporal(TemporalType.TIMESTAMP)
public Date getDebut() {
	return debut;
}
public void setDebut(Date debut) {
	this.debut = debut;
}
@Column(name="SU_FIN")
@Temporal(TemporalType.TIMESTAMP)
public Date getFin() {
	return fin;
}
public void setFin(Date fin) {
	this.fin = fin;
}
@Column(name="SU_MODE")
@Enumerated(EnumType.STRING)
public LoginMode getMode() {
	return mode;
}
public void setMode(LoginMode mode) {
	this.mode = mode;
}
@Column(name="SU_MEDIA")
public String getMedia() {
	return media;
}
public void setMedia(String media) {
	this.media = media;
}
@Column(name="SU_GSM_TOKEN")
public String getGsmToken() {
	return gsmToken;
}
public void setGsmToken(String gsmToken) {
	this.gsmToken = gsmToken;
}
@Column(name="SU_AGENT")
public String getAgent() {
	return agent;
}
public void setAgent(String agent) {
	this.agent = agent;
}
@ManyToOne
@JoinColumn(name="SU_USR_ID")
public Utilisateur getUtilisateur() {
	return utilisateur;
}
public void setUtilisateur(Utilisateur utilisateur) {
	this.utilisateur = utilisateur;
}
@ManyToOne
@JoinColumn(name="SU_UO_ID")
public UO getUniteOrganisationnelle() {
	return uniteOrganisationnelle;
}
public void setUniteOrganisationnelle(
		UO uniteOrganisationnelle) {
	this.uniteOrganisationnelle = uniteOrganisationnelle;
}
@ManyToOne
@JoinColumn(name="SU_FCT_CODE")
public Fonction getFonction() {
	return fonction;
}
public void setFonction(Fonction fonction) {
	this.fonction = fonction;
}

@Column(name="SU_LANGUE")
public String getLangue() {
	return langue;
}
public void setLangue(String langue) {
	this.langue = langue;
}




}
