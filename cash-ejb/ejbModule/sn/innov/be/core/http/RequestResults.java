/*
 * Created on 16 sept. 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package sn.innov.be.core.http;

import java.io.InputStream;

public final class RequestResults {
	public InputStream stream;
	public String string;
	public int status;
	public int length;
}
