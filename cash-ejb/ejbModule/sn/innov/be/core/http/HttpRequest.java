package sn.innov.be.core.http;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;




public class HttpRequest {
	private HttpURLConnection connection;
	
	Logger logger = Logger.getLogger("HttpRequest");

	
	public RequestResults doRequest (String surl, String content)
	{	
		RequestResults res = new RequestResults();
		res.length = 0;
		res.status = 0;
		res.stream = null;
		String resp = null;
		try {		
			// send the request			
			URL url = new URL(surl);		
			connection = (HttpURLConnection)url.openConnection();
			connection.setDoOutput(true);
			
			// in case content is to send
			if (content != null)
			{		
				connection.setRequestProperty("Content-Type", "text/xml");
	
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
				bw.write(content);
				bw.close();
			}

			// read the results of the request
			res.status = connection.getResponseCode();
			
			//this.logger.debug("doRequest return code: " + res.status);
			
			resp= connection.getResponseMessage();
			
			res.length = connection.getContentLength();
			res.string = connection.getContentType();
			
			//this.logger.debug("doRequest return answer length: <" + res.length + "> response message: " + resp);
			
			res.stream = connection.getInputStream();
			
			 
		} catch (Exception e) {
			this.logger.info("doRequest url: <" +  surl
					+ "> response: <" + resp
					+ "> return code: <" + res.status
					+ "> Exception thrown: " + e.getMessage());
			//e.printStackTrace();
		}
		return res;
	}

	public boolean writeResults(RequestResults res, HttpServletResponse response) {
		// send back map image if response OK
		final int BUFFER_SIZE = 1024 * 5;
		if (res.status == 200)
		{
            int dataLen;
            byte [] buffer = new byte[BUFFER_SIZE];
			BufferedInputStream in;
			OutputStream out = null;
            try {
               out = response.getOutputStream();
               in = new BufferedInputStream(res.stream);

               while ((dataLen = in.read(buffer)) != -1) {
            	   out.write(buffer, 0, dataLen);
               }
               in.close();
               out.flush();
               out.close();
            } catch (IOException cae) {
            	this.logger.info("User Aborted Query: ");
            } catch (Exception e) {
            	this.logger.info("writeResults Exception thrown:");
            } finally {
	           if (out != null) {
	               try {
	            	   out.close();
	               } catch (Exception e1) {
	               }
	           }
            }
		}
		else
		{
			return false;
		}
		return true;
	}
	// close HttpConnection
	public void close() {
		connection.disconnect();
	}
}
