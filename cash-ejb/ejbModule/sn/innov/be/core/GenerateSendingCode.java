package sn.innov.be.core;

import java.util.Date;

import sn.innov.be.commons.utils.DateUtils;


/**
 * Class allowing to <br>
 * <br>
 * Classe permettant de
 * 
 */

public final class GenerateSendingCode
{
	
	private static final String	DEFINED_KEYSTRINGLIST	= "0123456789";
	
	private static final int CODE_SIZE	= 8;
	
	public static final String getCodeTransaction()
	{		
		return generate(CODE_SIZE);
	} 
 
	private static final String generate(int length)
	{
		return RandomizeGenerationString.generate(length, GenerateSendingCode.DEFINED_KEYSTRINGLIST);
	}
	
	public static final String getNumeroBeneficiaire()
	{		
		return "B"+generate(CODE_SIZE);
	}
	
	public static final String getReferenceContrat(Date date)
	{		
		return "C"+(DateUtils.format(date, "dd/MM/yyyy")).subSequence(6,10)+generate(CODE_SIZE);
	}
	
	public static final String getnumeroCommande()
	{		
		return generate(CODE_SIZE);
	}
	public static final String getnumeroFacture()
	{		
		return generate(CODE_SIZE);
	}	
	public static final String getnumeroVirement(Date date)
	{		
		return "C"+(DateUtils.format(date, "dd/MM/yyyy")).subSequence(6,10)+generate(CODE_SIZE);
	}	
	public static final String generatePinCode()
	{		
		return generate(4);
	}
	
	public static String format(String code)
	{
		String formatted="";
		if(code==null)
			return null;
		else
		{
			 if(code.length()==9)
			{
				formatted = code.format("%1s %2s %3s", code.substring(0, 3), code.substring(3, 6),code.substring(6, 9));
				return formatted;
			}
			else if(code.length()==10)
			{
				formatted = code.format("%1s %2s %3s %4s", code.substring(0, 1), code.substring(1, 4),code.substring(4, 7),code.substring(7, 10));
				return formatted;
			}
			else
			return code;				
		}				
	}

}
