package sn.innov.be.core ;



/**
 * Classe permettant de g�n�rer des chaines de caract�res au hasard.
 * 
 */

public final class RandomizeGenerationString {
	
	/**
	 * Cette mode de g�n�ration retourne une chaine de caract�res contenant des
	 * chiffres.
	 */
	
	public static final int		RANDOMIZE_INT_MODE		= 0 ;
	
	/**
	 * Cette mode de g�n�ration retourne une chaine de caract�res contenant des
	 * lettres et des chiffres.
	 */
	
	public static final int		RANDOMIZE_STRING_MODE	= 1 ;
	
	/**
	 * La liste des lettres.
	 */
	
	private static final String	keyStringList			= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" ;
	
	/**
	 * La liste des chiffres.
	 */
	
	private static final String	keyIntList				= "0123456789" ;
	
	/**
	 * 
	 * @param mode
	 *            le mode de g�n�ration.
	 * @param length
	 *            la longueure de la chaine � g�n�rer.
	 * @return ganeratedString La chaine g�n�r�e.
	 * @throws RandomizeGenerationStringException
	 * 
	 */
	
	public static String generate( int mode , int length )
			throws Exception {

		if ( mode != RANDOMIZE_INT_MODE && mode != RANDOMIZE_STRING_MODE )
			
			throw new Exception("Erreur generation code");
		
		String keyList = mode == RANDOMIZE_INT_MODE ? keyIntList
				: keyStringList + keyIntList ;
		
		String ganeratedString = new String ( ) ;
		
		for ( int i = 0 ; i < length ; i ++ )
			
			ganeratedString += keyList.charAt ( new Double ( Math.floor ( Math
					.random ( )
					* keyList.length ( ) ) ).intValue ( ) ) ;
		
		return ganeratedString ;
		
	}
	
	/**
	 * 
	 * @param mode
	 *            le mode de g�n�ration.
	 * @param length
	 *            la longueure de la chaine � g�n�rer.
	 * @return ganeratedString La chaine g�n�r�e.
	 * @throws RandomizeGenerationStringException
	 * 
	 */
	
	public static String generate ( int length , String keyList ) {

		String ganeratedString = new String ( ) ;		
		for ( int i = 0 ; i < length ; i ++ )
			
			ganeratedString += keyList.charAt ( new Double ( Math.floor ( Math
					.random ( )
					* keyList.length ( ) ) ).intValue ( ) ) ;
		
		return ganeratedString ;
		
	}
	
}
