package sn.innov.be.core.utils;

public class BeConstants {

	/**
	 * Status
	 */
	public static char STATUT_ACTIF = 'A' ;
	public static char STATUT_INACTIF = 'I' ;
	public static char STATUT_SUPPRIME = 'S' ;
	
	
	/**
	 * Objets Workflow
	 */
	public static String CODE_DEMANDE_INFO = "DI" ;
	
	
	/**
	 * Etats Workflow
	 */
	
	public static String CODE_DEPOT_DEMANDE_INFOS = "DI_DEPOS" ;
	
	public static char ETAT_INITIAL = 'I';
	public static char ETAT_FINAL = 'F';

}
