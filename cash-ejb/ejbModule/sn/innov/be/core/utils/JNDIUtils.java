package sn.innov.be.core.utils;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;

public class JNDIUtils {

	public static Object lookUpEJB(EJBRegistry ejbRegistry) {
		try {
			InitialContext initialContext = new InitialContext();
			return initialContext.lookup("java:app/cash-ejb/" + ejbRegistry.name());
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static EntityManager lookUpEntityManager() {
		try {
			InitialContext initialContext = new InitialContext();
			return (EntityManager) initialContext.lookup("java:comp/env/PU");
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
