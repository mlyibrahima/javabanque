package sn.innov.be.core.utils;

public enum EJBRegistry {
	DroitsAccesServiceBean, FonctionServiceBean, PromoFideliteServiceBean, ArticleServiceBean, 
	FamilleServiceBean, SousFamilleServiceBean, GroupeServiceBean, ClientServiceBean, ClientGroupeServiceBean, OperationsServiceBean, 
	StationServiceBean, CiblageServiceBean, RegionServiceBean, ProduitServiceBean,  CampagneServiceBean, MoyenServiceBean,
	BandeauServiceBean, UtilisateurServiceBean, ActionServiceBean, AgentServiceBean, AuthentificationServiceBean, RegionTotalServiceBean,
	SecteurServiceBean, CommandeUVServiceBean, FonctionnaliteServiceBean,UOServiceBean, ReferentielCommonServiceBean, ModuleServiceBean, LogActionServiceBean, WorkFlowServiceBean, 
	DemandeInfosServiceBean, AnnonceServiceBean, AlerteServiceBean, ActiviteServiceBean, SondageServiceBean, ShippingLineServiceBean,PieceJointeServiceBean,
	DepartementServiceBean, PlainteServiceBean, RendezVousServiceBean, DetailServiceBean,ContratServiceBean, ParamWorkflowServiceBean, GestionClientServiceBean, ContactServiceBean, PushServiceBean, PackServiceBean, ComptabiliteServiceBean, AccountServiceBean, 
	MembreServiceBean, ProspectServiceBean, AffectationServiceBean, NdiguelServiceBean, MatriculeServiceBean, NdiguelServiceServiceBean, GrilleServiceBean, ParametrageServiceBean, CashServiceServiceBean, TransactionClientSessionBean, CommonTransactionServiceBean, VirementServiceBean, ProjetServiceBean, TransactionServiceBean, LotVirementServiceBean, ClientBean, LonaseService,PaiementBean;
}
