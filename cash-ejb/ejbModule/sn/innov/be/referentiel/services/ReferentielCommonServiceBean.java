package sn.innov.be.referentiel.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;



/**
 * Session Bean implementation class ReferentielCommonServiceBean
 * @param <T>
 */
@Stateless
public class ReferentielCommonServiceBean implements ReferentielCommonService {

	@PersistenceContext
	EntityManager em;

	@Override
	public <T> List<T> lister(Class<T> clazz) {
		String jpql = " from "+clazz.getName()+" a " ;  ;
		Query query = em.createQuery(jpql);
		List<T> result = new ArrayList<T>();
		try {
			result = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public <T> List<T> lister(Class<T> clazz, String key, int maxRows) {
		String jpql = " from "+clazz.getName()+ " a where upper(a.libelle) like :key " ;
		Query query = em.createQuery(jpql);
		query.setParameter("key","%"+key.toUpperCase()+"%");
		List<T> result = new ArrayList<T>();
		try {
			query.setMaxResults(maxRows);
			result = query.getResultList();
		} catch (Exception e) {
			
		}
		return result;
	}

	@Override
	public <T> T save(T t) {
		 T o =  em.merge(t);
		 
		 return o;
		
		
	}

	@Override
	public <T> List<T> lister(Class<T> clazz, int start, int limit) {
		String jpql = " from "+clazz.getName()+" a  " ;  ;
		Query query = em.createQuery(jpql);
		List<T> result = new ArrayList<T>();
		try {
			if(start > 0)
				query.setFirstResult(start);
			if(limit > 0)
				query.setMaxResults(limit);
			result = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public <T> long compter(Class<T> clazz) {
		String jpql = " select count(a) from "+clazz.getName()+" a  " ;  ;
		Query query = em.createQuery(jpql);
		long result = 0L;
		try {
			result = (Long) query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public <T> boolean supprimer(Class<T> clazz, String id) {
	try {
			
			javax.persistence.Query query = em.createQuery("DELETE from "+clazz.getName()+" v where v.code=:ID");
			query.setParameter("ID", id);
			query.executeUpdate();
			
			
		} catch (Exception e) {
 			//e.printStackTrace();
			return false;
		} 
		
		return true;
	}

	@Override
	public <T> boolean supprimer(Class<T> clazz, Long id) {
	try {
			
			javax.persistence.Query query = em.createQuery("DELETE from "+clazz.getName()+" v where v.id=:ID");
			query.setParameter("ID", id);
			query.executeUpdate();
			
			
		} catch (Exception e) {
 			//e.printStackTrace();
			return false;
		} 
		
		return true;
	}

	@Override
	public <T> Object findObject(Class<T> clazz, String id) {
	
		Object result=null;
	
		try {
			
			javax.persistence.Query query = em.createQuery("select v from "+clazz.getName()+" v where v.code=:CODE");
			query.setParameter("CODE", id);
			
			result=(Object)query.getSingleResult();
			
			
			} catch (Exception e) {
	 			//e.printStackTrace();
				return null;
			} 
		
		return result;
	}

	@Override
	public <T> Object findObject(Class<T> clazz, Long id) {
	
		Object result=null;
	
		try {
			
			javax.persistence.Query query = em.createQuery("select v from "+clazz.getName()+" v where v.id=:ID");
			query.setParameter("ID", id);
			
			result=(Object)query.getSingleResult();
			
			
			} catch (Exception e) {
	 			//e.printStackTrace();
				return null;
			} 
		
		return result;
	}
	
	
}
