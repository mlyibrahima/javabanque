package sn.innov.be.referentiel.services;

import java.util.List;

import javax.ejb.Remote;



@Remote
public interface ReferentielCommonService {

	/* Generiques */
	<T> List<T> lister(Class<T> clazz);
	<T> long compter(Class<T> clazz);
	<T> List<T> lister(Class<T> clazz, int start, int limit);
	<T> List<T> lister(Class<T> clazz, String key, int maxRows) ;
	<T> T save(T t);
	
	<T> boolean supprimer(Class<T> clazz, Long id);
	<T> boolean supprimer(Class<T> clazz, String id);

	<T> Object findObject(Class<T> clazz, String id);
	<T> Object findObject(Class<T> clazz, Long id);

		
}
