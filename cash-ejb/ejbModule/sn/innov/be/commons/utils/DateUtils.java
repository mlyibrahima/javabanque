/**
 * @author M.K.
 */
package sn.innov.be.commons.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Some utilities to work with Dates.
 * 
 */
public class DateUtils {

	private static final int EQUAL = 1;

	private static final int BEFORE = 2;

	private static final int AFTER = 3;

	/**
	 * Constructor.
	 */
	public DateUtils() {
	}

	/**
	 * Returns a new date of the same day as the given date but hour set to
	 * 23:59:59.59
	 * 
	 * @param day
	 * @return a new date of the same day as the given date but hour set to
	 *         23:59:59.59
	 */
	public static Date getLastTimeOfDay(Date day) {
		// Compute end Date.
		Calendar cal = Calendar.getInstance();
		cal.setTime(day);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		//
		return cal.getTime();
	}

	/**
	 * equal return true if begin date equals end date, false if not
	 * 
	 * @param begin
	 * @param end
	 * @return
	 */
	public static boolean equal(Date begin, Date end) {
		return compare(EQUAL, begin, end);
	}

	/**
	 * equal return true if begin date equals end date, false if not
	 * 
	 * @param begin
	 * @param end
	 * @return
	 */
	public static boolean isBefore(Date begin, Date end) {
		return compare(BEFORE, begin, end);
	}

	/**
	 * equal return true if begin date equals end date, false if not
	 * 
	 * @param begin
	 * @param end
	 * @return
	 */
	public static boolean isAfter(Date begin, Date end) {
		return compare(AFTER, begin, end);
	}

	/**
	 * compare
	 * 
	 * @param compare
	 * @param begin
	 * @param end
	 * @return
	 */
	private static boolean compare(int compare, Date begin, Date end) {
		boolean result = false;
		//
		switch (compare) {
		case EQUAL: // degin and end equal
			if (begin.compareTo(end) == 0) {
				result = true;
			}
			break;
		case BEFORE: // degin before end
			if (begin.compareTo(end) < 0) {
				result = true;
			}
			break;
		case AFTER: // degin after end
			if (begin.compareTo(end) > 0) {
				result = true;
			}
			break;
		default:
			return false;
		}
		return result;
	}
	
	public static String format(Date d, String format)
	{
		String formatted = "";
		SimpleDateFormat df = new SimpleDateFormat(format);
		if(d!=null)
		{
			formatted = df.format(d);
		}
		
		return formatted;
	}
	
	public static String numeroformat(String id,int nb){
		
		if(id.length()==nb)
			return id;
		else
		{
			while(id.length()<nb)
				id="0"+id;
		}
		
		return id;
		
	}
	
	public static Date format(String value, String format) throws ParseException
	{
		String formatted = "";
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat(format);
		try
		{
			date = df.parse(value);
		}
		catch(ParseException e)
		{
			e.printStackTrace();
			
		}
		
		
		return date;
	}
}