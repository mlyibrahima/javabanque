package sn.innov.be.security.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import sn.innov.be.authentification.entities.Utilisateur;

/**
 *Session Bean implementation class UtilisateurServiceBean
 */
@Stateless
public class UtilisateurServiceBean implements UtilisateurService {

	@PersistenceContext
	EntityManager em;

	@Override
	public List<Utilisateur> listerUtilisateur() {
		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
		try {
			Query query = em.createQuery("from Utilisateur a where a.supprime = false ");
			utilisateurs= query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateurs;
	}

	@Override
	public void ajouterUtilisateur(Utilisateur utilisateur) {
		em.persist(utilisateur);
	}

	@Override
	public Utilisateur supprimerUtilisateur(Utilisateur utilisateur) {
		utilisateur.setSupprime(true);
		return em.merge(utilisateur);
	}

	@Override
	public Utilisateur modifierUtilisateur(Utilisateur utilisateur) {
		return em.merge(utilisateur);
	}

	@Override
	public Utilisateur find(Utilisateur u) {
		return em.find(Utilisateur.class, u.getId());
	}

	@Override
	public List<Utilisateur> listerUtilisateur(Long departementid) {
		return em.createQuery("from Utilisateur a where a.supprime = false and a.departement.id = :departementid ", Utilisateur.class)
					.setParameter("departementid", departementid).getResultList();
	}

	 
	@Override
	public List<Utilisateur> findUserClient(Long idClient) {
		// TODO Auto-generated method stub
		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
		try {
			Query query = em.createQuery("select a from Utilisateur a left join a.client cli where cli.id=:idClient ");
			query.setParameter("idClient", idClient);
			utilisateurs= query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateurs;	}


}
