package sn.innov.be.security.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import sn.innov.be.security.entities.Action;
import sn.innov.be.security.entities.Fonctionnalite;
import sn.innov.be.security.entities.Module;

/**
 * Session Bean implementation class DroitsAccesServiceBean
 */
@Stateless
public class DroitsAccesServiceBean implements DroitsAccesService {

	@PersistenceContext
	EntityManager em;
    /**
     * Default constructor. 
     */
    public DroitsAccesServiceBean() {
        // TODO Auto-generated constructor stub
    }
    
	@Override
	public Module findModuleByBookmark(String bookmark) {
		String jpql = "select m from Fonctionnalite f left join f.module m where f.bookmark = :bookmark";
		Query query = em.createQuery(jpql);
		query.setParameter("bookmark", bookmark);	
		query.setHint("org.hibernate.cacheable", "true");
		Module module = null;
		try {
			module = (Module) query.getSingleResult();
		} catch (Exception e) {
			
		}
		return module;
	}


    @Override
	public List<Module> findFonctionModules(String fCode) {
		
		String jpql = "select distinct(m) from Fonction f " +
				"left join f.actions act left join act.fonctionnalite fo " +
				"left join fo.module m where f.supprime = false and fo.supprime = false and m.supprime = false and f.code = ?   " +
				"order by m.sequence";
		Query query = em.createQuery(jpql);
		query.setParameter(1, fCode);			
		query.setHint("org.hibernate.cacheable", "true");
		return query.getResultList();
	}

	@Override
	public List<Fonctionnalite> findFonctionFonctionnalites(String fCode,
			String moduleCode) {
		String jpql = "select distinct(fo) from Fonction f left join f.actions act  left join act.fonctionnalite fo " +
				" where f.supprime = false and act.supprime = false and fo.supprime = false and fo.module.supprime = false and  f.code = ? and fo.module.code = ? " +
				"order by fo.sequence";
		Query query = em.createQuery(jpql);
		query.setParameter(1, fCode);
		query.setParameter(2, moduleCode);	
		query.setHint("org.hibernate.cacheable", "true");
		return query.getResultList();
	}

	@Override
	public List<Action> findFonctionActions(String fCode,
			String fonctionnaliteCode) {
		String jpql = "select act from Fonction f left join f.actions act left join act.fonctionnalite fo " +
				" where act.supprime=false and fo.supprime = false and f.supprime = false and  f.code = ?  and fo.code = ? " +
				" order by act.sequence";
		Query query = em.createQuery(jpql);
		query.setParameter(1, fCode);
		query.setParameter(2, fonctionnaliteCode);	
		query.setHint("org.hibernate.cacheable", "true");
		return query.getResultList();
	}

	@Override
	public boolean hasAccess(String code, String action) {
		System.out.println("Has Access "+code+" "+action);
		String jpql = "select act from Fonction f left join f.actions act  where f.supprime = false and act.supprime = false and f.code = ?  and act.code = ?";
		Query query = em.createQuery(jpql);
		query.setParameter(1, code);
		query.setParameter(2, action);	
		query.setHint("org.hibernate.cacheable", "true");
		try {
			Action action2 =  (Action) query.getSingleResult();
		} catch (Exception e) {
			System.out.println("False");
			return false;
		}
		System.out.println("True");
		return true;
	}
}
