package sn.innov.be.security.services;

import java.util.List;

import javax.ejb.Remote;

import sn.innov.be.security.entities.Module;

@Remote
public interface ModuleService {

	public List<Module> listerModules();
	public void ajouterModule(Module module);
	public Module modifierModule(Module module);
	public Module supprimerModule(Module module);
	public Module getModule(String code);
}
