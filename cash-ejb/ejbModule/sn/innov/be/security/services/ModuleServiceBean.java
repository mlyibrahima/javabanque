package sn.innov.be.security.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import sn.innov.be.security.entities.Module;

@Stateless
public class ModuleServiceBean implements ModuleService {

	
	@PersistenceContext
	EntityManager em;
	public ModuleServiceBean() {
		
	}
	@Override
	public List<Module> listerModules() {
		List<Module> modules = new ArrayList<Module>();
		try {
			javax.persistence.Query query = em.createQuery("from Module m where m.supprime = false ");
			modules = query.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return modules;
	}

	@Override
	public void ajouterModule(Module module) {
		// TODO Auto-generated method stub
		em.persist(module);
	}

	@Override
	public Module modifierModule(Module module) {
		// TODO Auto-generated method stub
		return em.merge(module);
	}

	@Override
	public Module supprimerModule(Module module) {
		// TODO Auto-generated method stub
		module.setSupprime(true);
		return em.merge(module);
	}
	public Module getModule(String code) {
		Module m = em.find(Module.class, code);
		if(m != null ) return m ;
		else return null;
		//&& f.getStatut() != 0
	}
	
}
