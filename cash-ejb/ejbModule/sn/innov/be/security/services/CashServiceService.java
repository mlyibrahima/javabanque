package sn.innov.be.security.services;

import java.util.List;

import javax.ejb.Remote;

import sn.innov.be.parametrage.entities.CashService;
import sn.innov.be.parametrage.entities.UOCashService;
import sn.innov.be.uniteorganisationnelle.entities.UO;

@Remote
public interface CashServiceService {

	public List<CashService> listerServices();
	public void ajouterService(CashService fonctionnalite);
	public void supprimerService(CashService fonctionnalite);
 	public CashService getService(String code);
	public CashService save(CashService fonctionnalite);
	public void save(CashService service, List<UO> operateurs);
	List<UOCashService> listerUOCashServices(String service, String codeTypeUO);
 	
}
