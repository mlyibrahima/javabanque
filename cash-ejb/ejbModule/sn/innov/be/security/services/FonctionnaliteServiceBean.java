package sn.innov.be.security.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import sn.innov.be.security.entities.Fonctionnalite;

@Stateless
public class FonctionnaliteServiceBean implements FonctionnaliteService {

	
	@PersistenceContext
	EntityManager em;
	public FonctionnaliteServiceBean() {
		
	}
	@Override
	public List<Fonctionnalite> listerFonctionnalites() {
		List<Fonctionnalite> Fonctionnalites = new ArrayList<Fonctionnalite>();
		try {
			javax.persistence.Query query = em.createQuery("from Fonctionnalite f where f.supprime = false ");
			Fonctionnalites = query.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return Fonctionnalites;
	}

	@Override
	public void ajouterFonctionnalite(Fonctionnalite fonctionnalite) {
		// TODO Auto-generated method stub
		em.persist(fonctionnalite);
	}

	@Override
	public Fonctionnalite modifierFonctionnalite(Fonctionnalite fonctionnalite) {
		// TODO Auto-generated method stub
		return em.merge(fonctionnalite);
	}

	@Override
	public Fonctionnalite supprimerFonctionnalite(Fonctionnalite fonctionnalite) {
		// TODO Auto-generated method stub
		fonctionnalite.setSupprime(true);
		return em.merge(fonctionnalite);
	}
	@Override
	public Fonctionnalite getFonctionnalite(String code) {
		Fonctionnalite f = em.find(Fonctionnalite.class, code);
		if(f != null ) return f ;
		else return null;
		//&& f.getStatut() != 0
	}
	@Override
	public List<Fonctionnalite> listerFonctionnalites(String modCode) {
		List<Fonctionnalite> Fonctionnalites = new ArrayList<Fonctionnalite>();
		try {
			javax.persistence.Query query = em.createQuery("from Fonctionnalite f where f.module.code = :modCode and  f.supprime = false ");
			Fonctionnalites = query.setParameter("modCode", modCode).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return Fonctionnalites;
	}

}
