package sn.innov.be.security.services;

import java.util.List;

import javax.ejb.Remote;

import sn.innov.be.authentification.entities.Utilisateur;

@Remote
public interface UtilisateurService {
	
	public List<Utilisateur> listerUtilisateur();
	public void ajouterUtilisateur(Utilisateur action);
	public Utilisateur supprimerUtilisateur(Utilisateur action);
	public Utilisateur modifierUtilisateur(Utilisateur action);
	public Utilisateur find(Utilisateur u);
	public List<Utilisateur> listerUtilisateur(Long departementid);
   	public List<Utilisateur> findUserClient(Long idClient);
	
}
