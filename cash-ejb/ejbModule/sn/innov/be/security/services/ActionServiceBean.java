package sn.innov.be.security.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import sn.innov.be.security.entities.Action;
import sn.innov.be.security.entities.Fonctionnalite;

/**
 * Session Bean implementation class ActionServiceBean
 */
@Stateless
public class ActionServiceBean implements ActionService {

	@PersistenceContext
	EntityManager em;
    /**
     * Default constructor. 
     */
    public ActionServiceBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public Set<Action> listExcludedActionsForFonction(String fCode) {
		Set<Action> actions = new HashSet<Action>();
		try {
			Query query = em.createQuery("from Action a where a.supprime = false and a.code not in" +
					" (select acts.code from Fonction f inner join f.actions acts where f.code = :fCode)  order by a.fonctionnalite.code  ");
			query.setParameter("fCode", fCode);
			actions = new HashSet<Action>(query.getResultList());
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return actions;
	}
	
	@Override
	public Set<Action> listExcludedActionsForFonction(String fCode, String modCode) {
		Set<Action> actions = new HashSet<Action>();
		try {
			Query query = em.createQuery("from Action a where a.supprime = false and a.code not in" +
					" (select acts.code from Fonction f inner join f.actions acts where f.code = :fCode) and a.fonctionnalite.module.code = :modCode  order by a.fonctionnalite.code  ");
			query.setParameter("fCode", fCode);
			query.setParameter("modCode", modCode);
			actions = new HashSet<Action>(query.getResultList());
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return actions;
	}


	@Override
	public List<Action> listerAction() {
		// TODO Auto-generated method stub
		List<Action> actions = new ArrayList<Action>();
		try {
			javax.persistence.Query query = em.createQuery("from Action a where a.supprime = false order by a.fonctionnalite.code ");
			actions= query.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return actions;
	}

	@Override
	public void ajouterAction(Action action) {
		// TODO Auto-generated method stub
		em.persist(action);
	}

	@Override
	public Action supprimerAction(Action action) {
		// TODO Auto-generated method stub
		action.setSupprime(true);
		return em.merge(action);
	}

	@Override
	public Action modifierAction(Action action) {
		// TODO Auto-generated method stub
		return em.merge(action);
	}

	@Override
	public Set<Action> listActionsForFonction(String fCode, String modCode) {
		Set<Action> actions = new HashSet<Action>();
		try {
			Query query = em.createQuery("from Action a where a.supprime = false and a.code  in" +
					"  (select acts.code from Fonction f inner join f.actions acts where f.code = :fCode) and a.fonctionnalite.module.code = :modCode" +
					"  order by a.fonctionnalite.code ");
			query.setParameter("fCode", fCode);
			query.setParameter("modCode", modCode);
			actions = new HashSet<Action>(query.getResultList());
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return actions;
	}

	@Override
	public Set<Action> listActionsForFonction(String fCode) {
		Set<Action> actions = new HashSet<Action>();
		try {
			Query query = em.createQuery("from Action a where a.supprime = false and a.code  in" +
					"  (select acts.code from Fonction f inner join f.actions acts where f.code = :fCode) " +
					"  order by a.fonctionnalite.code ");
			query.setParameter("fCode", fCode);
			
			actions = new HashSet<Action>(query.getResultList());
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return actions;
	}

	@Override
	public Action findActionByCode(String actCode) {
		Action action = null ;
		try {
			action = em.find(Action.class, actCode) ;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return action ;
	}

	@Override
	public List<Action> listerAction(String fcntCode) {
		List<Action> actions = new ArrayList<Action>();
		try {
			javax.persistence.Query query = em.createQuery("from Action a where a.fonctionnalite.code = :fcntCode and a.supprime = false order by a.fonctionnalite.code ");
			actions= query.setParameter("fcntCode", fcntCode).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return actions;
	}

	@Override
	public void genereCrud(Fonctionnalite fonctionnalite) {
		// TODO Auto-generated method stub
		
		Action action=new Action();
		action.setCode("N_"+fonctionnalite.getCode());
		action.setDescription("CREER "+fonctionnalite.getDescription());
		action.setFonctionnalite(fonctionnalite);
		action.setSupprime(false);
		em.persist(action);
		
		action=new Action();
		action.setCode("U_"+fonctionnalite.getCode());
		action.setDescription("MODIFIER "+fonctionnalite.getDescription());
		action.setFonctionnalite(fonctionnalite);
		action.setSupprime(false);
		em.persist(action);
		
		action=new Action();
		action.setCode("D_"+fonctionnalite.getCode());
		action.setDescription("SUPPRIMER "+fonctionnalite.getDescription());
		action.setFonctionnalite(fonctionnalite);
		action.setSupprime(false);
		em.persist(action);
		
	}

	@Override
	public void creervoiraction(Fonctionnalite fonctionnalite) {
		// TODO Auto-generated method stub
		
		Action action=new Action();
		action.setCode("VOIR_"+fonctionnalite.getCode());
		action.setDescription("VOIR "+fonctionnalite.getDescription());
		action.setFonctionnalite(fonctionnalite);
		action.setSupprime(false);
		em.persist(action);
		
	}

}
