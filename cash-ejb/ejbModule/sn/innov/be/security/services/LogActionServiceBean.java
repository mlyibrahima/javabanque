package sn.innov.be.security.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.resteasy.util.DateUtil;

import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.security.entities.Action;
import sn.innov.be.security.entities.LogAction;

/**
 * Session Bean implementation class LogActionServiceBean
 */
@Stateless
public class LogActionServiceBean implements LogActionService {
	
	@PersistenceContext
	EntityManager em ;
	
	@EJB
	ActionService as ;

	@Override
	public List<Utilisateur> findUtilisateursControles(Long idUser) {
		
		String jpql = "select usr from Utilisateur u  join  u.utilisateursLogges usr where u.id = ? and u.supprime = false and usr.supprime = false ";
		Query query = em.createQuery(jpql);
		query.setParameter(1, idUser);
		
		return query.getResultList();
	}

	@Override
	public void log(String action, Utilisateur utilisateur) {
		LogAction log = new LogAction() ;
		Action act = as.findActionByCode(action);
		if(act == null ) { System.out.println("Erreur Recuperation action");return ; }
		log.setAction(act) ;
		log.setUtilisateur(utilisateur);
		em.merge(log);
		
	}

	@Override
	public List<LogAction> findLogAction(Long idUser, int debut , int nbParPage) {

		//String jpql = "from LogAction l where l.utilisateur.id = ? ";
		String jpql = "from LogAction l where l.utilisateur.id in ( select usr.id from Utilisateur u  join  u.utilisateursLogges usr where u.id = ? and u.supprime = false and usr.supprime = false) order by l.dateAction desc  ";
		Query query = em.createQuery(jpql);
		query.setParameter(1, idUser);
		if(debut >= 0)
			query.setFirstResult(debut);
		if(nbParPage > 0)
			query.setMaxResults(nbParPage);
		return query.getResultList();
	}

	@Override
	public List<Utilisateur> findUtilisateursNonControles(Long idUser) {
		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
		try {
			Query query = em.createQuery("from Utilisateur u where u.supprime = false and u.id not in" +
					" (select usr.id from Utilisateur u join u.utilisateursLogges usr where u.id = ? and u.supprime = false )  ");
			query.setParameter(1, idUser);
			utilisateurs = query.getResultList();
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return utilisateurs;
	}

	@Override
	public long compterLogAction(Long idUser) {
		String jpql = "select count(l) from LogAction l where l.utilisateur.id in ( select usr.id from Utilisateur u  join  u.utilisateursLogges usr where u.id = ? and u.supprime = false and usr.supprime = false) order by l.dateAction desc  ";
		Query query = em.createQuery(jpql);
		query.setParameter(1, idUser);
		
		return (Long) query.getSingleResult() ;
	}

	@Override
//	@Schedule(dayOfWeek="Mon-Fri", hour="22") 
	public void nettoyerLogsAnciens() {
		String jpql = " DELETE FROM LogAction l WHERE l.dateAction < :monthAgo " ;
		Calendar cal = Calendar.getInstance() ;
		cal.setTime(new Date()) ;
		cal.add(Calendar.MONTH, -1);
		Query query = em.createQuery(jpql).setParameter("monthAgo", cal.getTime());
		
		query.executeUpdate() ;
		
		
		
	}
	

		
	
	
	

}
