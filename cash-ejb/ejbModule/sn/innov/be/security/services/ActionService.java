package sn.innov.be.security.services;

import java.util.List;
import java.util.Set;

import javax.ejb.Remote;

import sn.innov.be.security.entities.Action;
import sn.innov.be.security.entities.Fonctionnalite;

@Remote
public interface ActionService {
	
	/**
	 * Retourner les actions qui n ont pas ete affectes a une fonction
	 * @param fCode
	 * @return
	 */
	public Set<Action>  listExcludedActionsForFonction(String fCode);
	public Set<Action>  listExcludedActionsForFonction(String fCode,String modCode);
	public List<Action> listerAction();
	public List<Action> listerAction(String fcntCode);
	public void ajouterAction(Action action);
	Action supprimerAction(Action action);
	Action modifierAction(Action action);
	public Set<Action> listActionsForFonction(String code);
	public Set<Action> listActionsForFonction(String code,String modCode);
	Action findActionByCode(String actCode) ;
	public void genereCrud(Fonctionnalite value);
	public void creervoiraction(Fonctionnalite value);

}
