package sn.innov.be.security.services;

import java.util.List;

import javax.ejb.Remote;

import sn.innov.be.authentification.entities.Fonction;

@Remote
/**+
 * 
 * @author moustapha
 *Service charg� de la gestion des fonctions
 */
public interface FonctionService {
	/**
	 * Retourne la liste des fonctions
	 * @return
	 */
	public List<Fonction> lister();
	
	/**
	 * Ajoute une nouvelle fonction 
	 * @param fonction
	 */
	public void ajouter(Fonction fonction ) throws Exception;
	
	/**
	 * Modifier une fonction 
	 * @param fonction
	 */
	public Fonction modifier(Fonction fonction) throws Exception;
	
	/**
	 * Retourne une fonction
	 * @param fonction
	 */
	public Fonction findByCode(String fCode);
	
	/**
	 * Supprime une fonction (suppression logique)
	 * @param fonction
	 */
	public Fonction supprimer(Fonction fonction);


}
