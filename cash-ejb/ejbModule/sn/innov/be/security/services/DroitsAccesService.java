package sn.innov.be.security.services;

import java.util.List;

import javax.ejb.Remote;

import sn.innov.be.security.entities.Action;
import sn.innov.be.security.entities.Fonctionnalite;
import sn.innov.be.security.entities.Module;

@Remote
/**+
 * 
 * @author sony
 *Service charg� de la gestion des droits des utilisateurs
 */
public interface DroitsAccesService {

	/**
	 * Retourne les modules auxquels une fonction a acc�s
	 * @param fCode
	 * @return
	 */
	List<Module> findFonctionModules(String fCode);

	/**
	 * Retourne les fonctionnalites d'un module auxquelless une fonction a acc�s
	 * @param fCode
	 * @return
	 */
	List<Fonctionnalite> findFonctionFonctionnalites(String fCode,String moduleCode);

	/**
	 * Retourne les actions d'une fonctionnalit� auxquelles une fonction a acc�s
	 * @param fCode
	 * @param fonctionnaliteCode
	 * @return
	 */
	List<Action> findFonctionActions(String fCode, String fonctionnaliteCode);

	boolean hasAccess(String code, String action);
	
	
	Module findModuleByBookmark(String bookmark);

}
