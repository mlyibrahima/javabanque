package sn.innov.be.security.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import sn.innov.be.authentification.entities.Fonction;

/**
 * Session Bean implementation class FonctionServiceBean
 */
@Stateless
public class FonctionServiceBean implements FonctionService {

	@PersistenceContext
	EntityManager em;
    /**
     * Default constructor. 
     */
    public FonctionServiceBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public List<Fonction> lister() {
		List<Fonction> fonctions = new ArrayList<Fonction>();
		try {
			Query query = em.createQuery("from Fonction f where f.supprime = false ");
			fonctions = query.getResultList();
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		
		return fonctions;
	}

	@Override
	public void ajouter(Fonction fonction)  {
		em.persist(fonction);
	}
	
	@Override
	public Fonction modifier(Fonction fonction) {
		return em.merge(fonction);
	}

	@Override
	public Fonction findByCode(String fCode) {
		Fonction f = em.find(Fonction.class, fCode);
		if(f != null) return f ;
		else return null;
	}

	@Override
	public Fonction supprimer(Fonction fonction) {
		if(lister().size() <= 1 ) return null ;
		fonction.setSupprime(true);
		return em.merge(fonction);
	}

}
