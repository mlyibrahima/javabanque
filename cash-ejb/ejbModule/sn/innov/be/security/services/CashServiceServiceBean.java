package sn.innov.be.security.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import sn.innov.be.parametrage.entities.CashService;
import sn.innov.be.parametrage.entities.UOCashService;
import sn.innov.be.uniteorganisationnelle.entities.UO;

@Stateless
public class CashServiceServiceBean implements CashServiceService {

	@PersistenceContext
	EntityManager em;

	public CashServiceServiceBean() {

	}

	@Override
	public List<CashService> listerServices() {
		List<CashService> Services = new ArrayList<CashService>();
		try {
			javax.persistence.Query query = em.createQuery("from CashService f ");
			Services = query.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return Services;
	}

	@Override
	public void ajouterService(CashService fonctionnalite) {
		// TODO Auto-generated method stub
		em.persist(fonctionnalite);
	}

	@Override
	public CashService save(CashService fonctionnalite) {
		// TODO Auto-generated method stub
		return em.merge(fonctionnalite);
	}

	@Override
	public void supprimerService(CashService service) {
		service = em.find(CashService.class, service.getCode());
		em.detach(service);
	}

	@Override
	public CashService getService(String code) {
		CashService f = em.find(CashService.class, code);
		if (f != null)
			return f;
		else
			return null;
		// && f.getStatut() != 0
	}

	@Override
	public void save(CashService service, List<UO> operateurs) {
		// TODO Auto-generated method stub

		String sql = "Delete from UOCashService uos where uos.service.code=:CODE ";
		Query query = em.createQuery(sql);
		query.setParameter("CODE", service.getCode());
		query.executeUpdate();

		service = em.merge(service);

		for (UO uo : operateurs) {

			UOCashService uos = new UOCashService();
			uos.setUo(uo);
			uos.setService(service);

			em.persist(uos);

		}

	}

	/**
	 * 
	 * M�thode permettant de lister les uo par services (ex les operateurs li�s
	 * au services)
	 * 
	 * @param service
	 * @param codeTypeUO
	 * 
	 * @return
	 * 
	 */

	@Override
	public List<UOCashService> listerUOCashServices(String service, String codeTypeUO) {
		
		List<UOCashService> uoServices = new ArrayList<UOCashService>();
		
		try {
			String sql = "Select u from UOCashService u left join u.uo uorg left join uorg.typeUO typ where 1=1 ";
		
			if (service != null && !service.isEmpty())
				sql += " and  u.service.code=:CODE  ";
			
			if (codeTypeUO != null && !codeTypeUO.isEmpty())
				sql += " and  typ.code=:CODETYPE  ";

			javax.persistence.Query query = em.createQuery(sql);
			
			if (service != null && !service.isEmpty())
				query.setParameter("CODE", service);
			
			if (codeTypeUO != null && !codeTypeUO.isEmpty())
				query.setParameter("CODETYPE", codeTypeUO);

			uoServices = query.getResultList();
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
		return uoServices;
	}

}
