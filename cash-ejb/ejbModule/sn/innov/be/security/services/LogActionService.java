package sn.innov.be.security.services;

import java.util.List;

import javax.ejb.Remote;

import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.security.entities.Action;
import sn.innov.be.security.entities.LogAction;

@Remote
public interface LogActionService {
	
	List<Utilisateur> findUtilisateursControles(Long idUser);
	
	List<Utilisateur> findUtilisateursNonControles(Long idUser);
	
	List<LogAction> findLogAction(Long idUser, int debut, int nbParPage) ;
	
	long  compterLogAction(Long idUser) ;
	
	void log(String action, Utilisateur utilisateur) ;
	
	void nettoyerLogsAnciens() ;

}
