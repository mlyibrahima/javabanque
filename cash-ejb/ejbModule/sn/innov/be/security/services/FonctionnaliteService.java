package sn.innov.be.security.services;

import java.util.List;

import javax.ejb.Remote;

import sn.innov.be.security.entities.Fonctionnalite;

@Remote
public interface FonctionnaliteService {

	public List<Fonctionnalite> listerFonctionnalites();
	public List<Fonctionnalite> listerFonctionnalites(String modCode);
	public void ajouterFonctionnalite(Fonctionnalite fonctionnalite);
	public Fonctionnalite supprimerFonctionnalite(Fonctionnalite fonctionnalite);
	public Fonctionnalite modifierFonctionnalite(Fonctionnalite fonctionnalite);
	public Fonctionnalite getFonctionnalite(String code);
}
