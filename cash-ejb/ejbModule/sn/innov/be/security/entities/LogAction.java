package sn.innov.be.security.entities;

import java.io.Serializable;
import java.lang.Long;
import java.util.Date;

import javax.persistence.*;

import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.security.entities.Action;

/**
 * Entity implementation class for Entity: LogAction
 *
 */
@Entity
@Table(name="logs_actions")
public class LogAction implements Serializable {

	private Action action;
	private Utilisateur utilisateur;
	private Date dateAction = new Date();   
	private Long id;
	private static final long serialVersionUID = 1L;

	public LogAction() {
		super();
	}   
	
	@ManyToOne
	@JoinColumn(name="log_act_id")
	public Action getAction() {
		return this.action;
	}

	public void setAction(Action action) {
		this.action = action;
	}   
	

	@ManyToOne
	@JoinColumn(name="log_usr_id")
	public Utilisateur getUtilisateur() {
		return this.utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	@Column(name="log_date")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDateAction() {
		return this.dateAction;
	}

	public void setDateAction(Date dateAction) {
		this.dateAction = dateAction;
	}  
	
	@Id
	@Column(name="log_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
   
}
