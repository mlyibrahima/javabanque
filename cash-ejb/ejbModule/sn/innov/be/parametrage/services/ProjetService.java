package sn.innov.be.parametrage.services;

import java.util.List;

import javax.ejb.Remote;

import sn.innov.be.parametrage.entities.Projet;

@Remote
public interface ProjetService {

	public List<Projet> listerFonctionnalites();
	public List<Projet> listerFonctionnalites(String modCode);
	public void ajouterFonctionnalite(Projet fonctionnalite);
	public Projet supprimerProjet(Projet fonctionnalite);
	public Projet modifierFonctionnalite(Projet fonctionnalite);
	public Projet getFonctionnalite(String code);
}