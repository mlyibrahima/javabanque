package sn.innov.be.parametrage.services;

import java.util.List;

import javax.ejb.Remote;

import sn.innov.be.parametrage.entities.Commission;
import sn.innov.be.parametrage.entities.Grille;
import sn.innov.be.parametrage.entities.Palier;
import sn.innov.be.parametrage.entities.Parameter;

@Remote
public interface ParametrageService {
	
public Grille findGrille(String code);	
public void creerGrille(Grille grille);
public List<Grille> findGrilles(int start, int step);
public int countGrilles();
public Long ajouterPalier(String codeGrille, Palier palier);
public Palier findPalier(Long id);
public List<Palier> findPaliers(String grilleCode);

public List<Commission> findCommissions(int start, int stop);
public int countCommission();

public Parameter getParameterByCode(String code);
public void addCommission(Commission commission);
public Commission findCommission(String code);
public void deletePalier(Long palier);
public void deleteGrille(String grille);
public void deleteCommission(String code);
List<Commission> findCommissions();

}
