package sn.innov.be.parametrage.services;

import java.util.List;

import javax.ejb.Remote;

import sn.innov.be.parametrage.entities.Grille;
import sn.innov.be.parametrage.entities.Palier;

@Remote
public interface GrilleService {
	
	/**
	 * Retourner les actions qui n ont pas ete affectes a une fonction
	 * @param fCode
	 * @return
	 */
 	public List<Grille> listerGrille();
  	public void ajouterGrille(Grille grille);
	public void supprimerGrille(Grille grille);
	Grille modifierGrille(Grille grille);
	Grille findGrilleByCode(String actCode) ;
	public void save(Grille grille);
	public void save(Palier tarif);
 	public List<Palier> listerPaliers(String codeGrille);
  	public void ajouterPalier(Palier palier);
	public void supprimerPalier(Palier action);
	Palier modifierPalier(Palier palier);
	
 
}
