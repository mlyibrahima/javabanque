package sn.innov.be.parametrage.services;


import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import sn.innov.be.parametrage.entities.Projet;

@Stateless
public class ProjetServiceBean implements ProjetService {

	
	@PersistenceContext
	EntityManager em;
	public ProjetServiceBean() {
		
	}
	
	@Override
	public List<Projet> listerFonctionnalites() {
		List<Projet> Fonctionnalites = new ArrayList<Projet>();
		try {
			javax.persistence.Query query = em.createQuery(" from Projet f where f.supprime = false ");
			Fonctionnalites = query.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return Fonctionnalites;
	}

	@Override
	public void ajouterFonctionnalite(Projet fonctionnalite) {
		// TODO Auto-generated method stub
		em.persist(fonctionnalite);
	}

	@Override
	public Projet modifierFonctionnalite(Projet fonctionnalite) {
		// TODO Auto-generated method stub
		return em.merge(fonctionnalite);
	}

	@Override
	public Projet supprimerProjet(Projet fonctionnalite) {
		// TODO Auto-generated method stub
		fonctionnalite.setSupprime(true);
		return em.merge(fonctionnalite);
	} 
	
	@Override
	public Projet getFonctionnalite(String code) {
		Projet f = em.find(Projet.class, code);
		if(f != null ) return f ;
		else return null;
		//&& f.getStatut() != 0
	}
	@Override
	public List<Projet> listerFonctionnalites(String modCode) {
		List<Projet> Fonctionnalites = new ArrayList<Projet>();
		try {
			javax.persistence.Query query = em.createQuery("from Projet f where f.code = :modCode and  f.supprime = false ");
			Fonctionnalites = query.setParameter("modCode", modCode).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return Fonctionnalites;
	}

}

