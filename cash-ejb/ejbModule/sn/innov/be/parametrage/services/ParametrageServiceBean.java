package sn.innov.be.parametrage.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import sn.innov.be.parametrage.entities.Commission;
import sn.innov.be.parametrage.entities.Grille;
import sn.innov.be.parametrage.entities.Palier;
import sn.innov.be.parametrage.entities.Parameter;

/**
 * Session Bean implementation class  
 */
@Stateless
public class ParametrageServiceBean implements ParametrageService {
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public void creerGrille(Grille grille) {
		entityManager.merge(grille);
		
	}

	@Override
	public Long ajouterPalier(String codeGrille, Palier palier) {
		Grille grille = entityManager.find(Grille.class, codeGrille);
		palier.setGrille(grille);
		grille.getPaliers().add(palier);
		entityManager.merge(palier);
		return palier.getId();
	}

	@Override
	public List<Grille> findGrilles(int start, int step) {
		String jpql = "from Grille order by code asc";
		Query query = entityManager.createQuery(jpql);
		
		if(start!=-1)
		query.setFirstResult(start);
		
		if(step!=-1)
		query.setMaxResults(step);
		
		return query.getResultList();
	}

	@Override
	public int countGrilles() {
		String jpql = "select count(g) from Grille g ";
		Query query = entityManager.createQuery(jpql);
		return ((Long) query.getSingleResult()).intValue();
	}

	@Override
	public List<Palier> findPaliers(String grilleCode) {
		String jpql = "select p from Palier p where p.grille.code = ? order by p.borneInferieure asc";
		Query query = entityManager.createQuery(jpql);
		query.setParameter(1, grilleCode);
		return query.getResultList();
	}

	@Override
	public Parameter getParameterByCode(String code) {
		// TODO Auto-generated method stub
		return entityManager.find(Parameter.class, code);
	}
	
	@Override
	public List<Commission> findCommissions() {
		// TODO Auto-generated method stub
		
		String jpql = "from Commission order by code asc";
		Query query = entityManager.createQuery(jpql);
 		
 		return query.getResultList();
	}

	@Override
	public List<Commission> findCommissions(int start, int stop) {
		// TODO Auto-generated method stub
		
		String jpql = "from Commission order by code asc";
		Query query = entityManager.createQuery(jpql);
		if(start>0)
		query.setFirstResult(start);
		
		if(stop>0)
			query.setMaxResults(stop);
		return query.getResultList();
	}

	@Override
	public int countCommission() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void addCommission(Commission commission) {
		// TODO Auto-generated method stub
		
		entityManager.merge(commission);
	}

	@Override
	public Grille findGrille(String code) {
		// TODO Auto-generated method stub
		return entityManager.find(Grille.class, code);

	}

	@Override
	public Commission findCommission(String code) {
		// TODO Auto-generated method stub
		return entityManager.find(Commission.class, code);
	}

	@Override
	public Palier findPalier(Long id) {
		// TODO Auto-generated method stub
		return entityManager.find(Palier.class, id);
	}

	@Override
	public void deletePalier(Long palier) {

		 String sql="delete from Palier gr where gr.id=:Code";
		 Query query=entityManager.createQuery(sql);
		query.setParameter("Code", palier);
		query.executeUpdate();
	
	}

	@Override
	public void deleteGrille(String grille) {
		// TODO Auto-generated method stub
		
		try{
		String sql="delete from Palier g where g.grille.code=:Code";
		Query  query=entityManager.createQuery(sql);
		 query.setParameter("Code", grille);
		query.executeUpdate();
		}catch(Exception e)
		{
			
		}
		
		String sql="delete from Grille gr where gr.code=:Code";
		Query query=entityManager.createQuery(sql);
		query.setParameter("Code", grille);
		query.executeUpdate();
	
 
	}

	@Override
	public void deleteCommission(String code) {
		// TODO Auto-generated method stub
		try{

		String sql="delete from Commission gr where gr.code=:Code";
		Query query=entityManager.createQuery(sql);
		query.setParameter("Code", code);
		query.executeUpdate();
		
		}
		catch(Exception e)
		{
			
		}
	}

	 
}
