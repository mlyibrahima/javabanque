package sn.innov.be.parametrage.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import sn.innov.be.parametrage.entities.Grille;
import sn.innov.be.parametrage.entities.Palier;

/**
 * Session Bean implementation class GrilleServiceBean
 */
@Stateless
public class GrilleServiceBean implements GrilleService {

	@PersistenceContext
	EntityManager em;
    /**
     * Default constructor. 
     */
    public GrilleServiceBean() {
        // TODO Auto-generated constructor stub
    }

  
	@Override
	public void ajouterGrille(Grille grille) {
		// TODO Auto-generated method stub
		em.persist(grille);
	}

	@Override
	public void supprimerGrille(Grille grille) {
		// TODO Auto-generated method stub
		try {
			
			javax.persistence.Query query = em.createQuery("DELETE from Palier v where v.grille.code=:ID");
			query.setParameter("ID", grille.getCode());
			query.executeUpdate();
			
			query = em.createQuery("DELETE from Grille v where v.code=:ID");
			query.setParameter("ID", grille.getCode());
			query.executeUpdate();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} 		 
	}

	@Override
	public Grille modifierGrille(Grille grille) {
		// TODO Auto-generated method stub
		return em.merge(grille);
	}

 

	@Override
	public Grille findGrilleByCode(String actCode) {
		Grille grille = null ;
		try {
			grille = em.find(Grille.class, actCode) ;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return grille ;
	}

	 

	@Override
	public List<Grille> listerGrille() {
		// TODO Auto-generated method stub
		
		List<Grille> grilles = new ArrayList<Grille>();
		try {
			javax.persistence.Query query = em.createQuery("from Grille a order by a.code desc ");
			grilles= query.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return grilles;
	}


  

	@Override
	public void save(Grille grille) {
		// TODO Auto-generated method stub
		
		  em.merge(grille);

	}


	@Override
	public void save(Palier tarif) {
		// TODO Auto-generated method stub
		  em.merge(tarif);

	}


	@Override
	public List<Palier> listerPaliers(String codeGrille) {
		// TODO Auto-generated method stub
		
		List<Palier> paliers = new ArrayList<Palier>();
		try {
			javax.persistence.Query query = em.createQuery("select t from Palier t left join t.grille g where g.code=:ID order by t.id desc ");
			query.setParameter("ID", codeGrille);
			paliers= query.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return paliers;	}


	@Override
	public void ajouterPalier(Palier palier) {
 		em.persist(palier);

	}


	@Override
	public void supprimerPalier(Palier palier) {
 		
		try {
			
			javax.persistence.Query query = em.createQuery("DELETE from Palier v where v.id=:ID");
			query.setParameter("ID", palier.getId());
			query.executeUpdate();
			
			
		} catch (Exception e) {
 			e.printStackTrace();
		} 
		
	}


	@Override
	public Palier modifierPalier(Palier palier) {
 		return em.merge(palier);
	}

 

}
