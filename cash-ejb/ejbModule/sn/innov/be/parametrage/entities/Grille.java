package sn.innov.be.parametrage.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="grilles")
public class Grille implements Serializable {
String code,description;
List<Palier> paliers = new ArrayList<Palier>();

@Id
@Column(name="GRI_CODE")
public String getCode() {
	return code;
}

public void setCode(String code) {
	this.code = code;
}
@Column(name="GRI_DESCRIPTION")
public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

@OneToMany(mappedBy="grille")
public List<Palier> getPaliers() {
	return paliers;
}
public void setPaliers(List<Palier> paliers) {
	this.paliers = paliers;
}
}
