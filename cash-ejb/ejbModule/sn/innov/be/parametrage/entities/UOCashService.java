package sn.innov.be.parametrage.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import sn.innov.be.uniteorganisationnelle.entities.UO;


@Entity
@Table(name = "uo_services")
public class UOCashService implements Serializable {
	
 	Long id;
 	UO uo;
 	CashService service;
 	
 	@Id
  	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne
	@JoinColumn(name="uos_uo_id")
	public UO getUo() {
		return uo;
	}
	public void setUo(UO uo) {
		this.uo = uo;
	}
	
	@ManyToOne
	@JoinColumn(name="uos_srv_code")
	public CashService getService() {
		return service;
	}
	public void setService(CashService service) {
		this.service = service;
	}
	
 
	 

}
