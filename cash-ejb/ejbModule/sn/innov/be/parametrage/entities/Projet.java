package sn.innov.be.parametrage.entities;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Projet
 *
 */
@Entity
@Table(name="projets")
public class Projet implements Serializable {
	@Id
	private String code;
	private String nom;
	private String domaine;
	private String responsable;
	Boolean supprime = false ;
	
	public Boolean getSupprime() {
		return supprime;
	}
	public void setSupprime(Boolean supprime) {
		this.supprime = supprime;
	}

	private static final long serialVersionUID = 1L;

	public Projet() {
		//super();
	}   
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}   
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}   
	public String getDomaine() {
		return this.domaine;
	}

	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}   
	public String getResponsable() {
		return this.responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	} 
	
   
}
