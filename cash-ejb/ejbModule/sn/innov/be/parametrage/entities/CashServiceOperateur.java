package sn.innov.be.parametrage.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import sn.innov.be.uniteorganisationnelle.entities.UO;


@Entity
@Table(name = "services_operateur")
public class CashServiceOperateur implements Serializable {
	
	Long id;
 	CashService service;
	UO operateur;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name="SRV_UO_ID")
	public CashService getService() {
		return service;
	}
	public void setService(CashService service) {
		this.service = service;
	}
	
	@ManyToOne
	@JoinColumn(name="UO_SRVID")
	public UO getOperateur() {
		return operateur;
	}
	public void setOperateur(UO operateur) {
		this.operateur = operateur;
	}
	
	
	

}
