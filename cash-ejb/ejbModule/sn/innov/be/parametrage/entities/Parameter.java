package sn.innov.be.parametrage.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
@Entity
@Table(name="parametres")
@Cacheable
@Cache(usage=CacheConcurrencyStrategy.TRANSACTIONAL)
public class Parameter implements Serializable {
	
String code, description,value;
 
@Id
@Column(name="PRM_CODE")
public String getCode() {
	return code;
}

public void setCode(String code) {
	this.code = code;
}
@Column(name="PRM_DESCRIPTION")
public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

@Column(name="PRM_VALUE")
public String getValue() {
	return value;
}

public void setValue(String value) {
	this.value = value;
}



 
}
