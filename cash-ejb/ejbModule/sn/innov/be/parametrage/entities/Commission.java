package sn.innov.be.parametrage.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="commissions")
public class Commission implements Serializable {
	
	
	String code;
	String description;
	TypeCommission type;
	BigDecimal montantFixe;
	float pourcentage;
	Grille grille;
	
	@Id
	@Column(name="TCOM_CODE")
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	@Column(name="TCOM_DESCRIPTION")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="TCOM_MONTANT_FIXE")
	public BigDecimal getMontantFixe() {
		return montantFixe;
	}
	public void setMontantFixe(BigDecimal montantFixe) {
		this.montantFixe = montantFixe;
	}
	
	@Column(name="TCOM_POURCENTAGE")
	public float getPourcentage() {
		return pourcentage;
	}
	public void setPourcentage(float pourcentage) {
		this.pourcentage = pourcentage;
	}
	
	@ManyToOne
	@JoinColumn(name="TCOM_GRI_CODE")
	public Grille getGrille() {
		return grille;
	}
	public void setGrille(Grille grille) {
		this.grille = grille;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="TCOM_TYPE_COM")
	public TypeCommission getType() {
		return type;
	}
	public void setType(TypeCommission type) {
		this.type = type;
	}
	
	
	
	

}
