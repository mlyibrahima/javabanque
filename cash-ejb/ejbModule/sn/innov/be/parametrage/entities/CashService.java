package sn.innov.be.parametrage.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import sn.innov.be.uniteorganisationnelle.entities.UO;


@Entity
@Table(name = "services")
public class CashService implements Serializable {
	
	String code;
	String nom;
	TypeService type;
	TypePaiement paiement;
	
	
	@Id
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@ManyToOne
	@JoinColumn(name="SER_TYP_CODE")
	public TypeService getType() {
		return type;
	}
	public void setType(TypeService type) {
		this.type = type;
	}
	
	@ManyToOne
	@JoinColumn(name="SER_PAY_CODE")
	public TypePaiement getPaiement() {
		return paiement;
	}
	public void setPaiement(TypePaiement paiement) {
		this.paiement = paiement;
	}
	 

}
