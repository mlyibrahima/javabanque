package sn.innov.be.parametrage.entities;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="paliers")
public class Palier implements Serializable {
Long id;
Float borneInferieure,borneSuperieure,valeur;
Grille grille;

@Id
@Column(name="PAL_ID")
@GeneratedValue(strategy=GenerationType.IDENTITY)
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
@Column(name="PAL_INF")
public Float getBorneInferieure() {
	return borneInferieure;
}
public void setBorneInferieure(Float borneInferieure) {
	this.borneInferieure = borneInferieure;
}
@Column(name="PAL_SUP")
public Float getBorneSuperieure() {
	return borneSuperieure;
}
public void setBorneSuperieure(Float borneSuperieure) {
	this.borneSuperieure = borneSuperieure;
}
@Column(name="PAL_VAL")
public Float getValeur() {
	return valeur;
}
public void setValeur(Float valeur) {
	this.valeur = valeur;
}
 
@ManyToOne(fetch=FetchType.LAZY)
@JoinColumn(name="PAL_GRI_CODE")
public Grille getGrille() {
	return grille;
}
public void setGrille(Grille grille) {
	this.grille = grille;
}


}
