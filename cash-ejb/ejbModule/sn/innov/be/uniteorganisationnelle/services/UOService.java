package sn.innov.be.uniteorganisationnelle.services;

import java.util.List;
import java.util.Set;

import javax.ejb.Remote;

import sn.innov.be.uniteorganisationnelle.entities.TypeUO;
import sn.innov.be.uniteorganisationnelle.entities.UO;

@Remote
public interface UOService {

	public UO saveUO(UO unite);
	
	public TypeUO saveTUO(TypeUO unite);
	
	public boolean supprimerUO(UO unite);
	
	public boolean supprimerTUO(TypeUO typeUO);
	
	public UO rechercherUO(String nomUO);
	
	public List<UO> listerUO();

	public List<TypeUO> listerTypeUO();
	
	public TypeUO getTypeUO(String code);
	
	public UO getUO(Long id);
	
	public List<UO> getListUOByType(String code);

	public List<UO> getListUOByTypes(List<String> listcode);

	public List<UO> searchListUOByType(String code,String search);

	public UO findUObyType(String code);

	public int saveImport(Set<UO> data);

	//Opublic void saveUO(UO uo, Contrat contrat);
	
 	
}
