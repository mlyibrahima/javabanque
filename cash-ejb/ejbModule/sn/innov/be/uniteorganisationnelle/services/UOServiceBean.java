package sn.innov.be.uniteorganisationnelle.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import sn.innov.be.uniteorganisationnelle.entities.TypeUO;
import sn.innov.be.uniteorganisationnelle.entities.UO;

/**
 * Session Bean implementation class UOServiceBean
 */
@Stateless
public class UOServiceBean implements UOService{

	@PersistenceContext
	EntityManager em;
	
    public UOServiceBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public UO saveUO(UO unite) {
		try {
			return em.merge(unite);
		} catch (Exception e) {
			e.printStackTrace();
 			return unite;
		}
	}

	@Override
	public TypeUO saveTUO(TypeUO unite) {
		try {
			return em.merge(unite);
		} catch (Exception e) {
			e.printStackTrace();
 			return unite;
		}
	}


	@Override
	public boolean supprimerTUO(TypeUO typeUO) {
		
		try {
			
			javax.persistence.Query query = em.createQuery("DELETE from TypeUO v where v.code=:ID");
			query.setParameter("ID", typeUO.getCode());
			query.executeUpdate();
			
			
		} catch (Exception e) {
 			//e.printStackTrace();
			return false;
		} 
		
		return true;
	}
	
	@Override
	public boolean supprimerUO(UO uo) {
		
		try {
			
			javax.persistence.Query query = em.createQuery("DELETE from UO v where v.id=:ID");
			query.setParameter("ID", uo.getId());
			query.executeUpdate();
			
			
		} catch (Exception e) {
 			//e.printStackTrace();
			return false;
		} 
		
		return true;
	}

	@Override
	public UO rechercherUO(String nomUO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UO> listerUO() {
		List<UO> UO = new ArrayList<UO>();
		try {
			javax.persistence.Query query = em.createQuery("select typ from UO typ");
			UO = query.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return UO;
	}

	@Override
	public List<TypeUO> listerTypeUO() {
		List<TypeUO> TypeUO = new ArrayList<TypeUO>();
		try {
			javax.persistence.Query query = em.createQuery("select typ from TypeUO typ");
			TypeUO = query.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return TypeUO;
	}

	@Override
	public TypeUO getTypeUO(String code) {
		TypeUO tuo = em.find(TypeUO.class, code);
		if(tuo != null ) return tuo ;
		else return null;
	}

	@Override
	public UO getUO(Long id) {
 		UO uo = em.find(UO.class, id);
		if(uo != null ) return uo ;
		else return null;
	}

	@Override
	public List<UO> getListUOByType(String code) {
		List<UO> uos =null;
		try {
			javax.persistence.Query query = em.createQuery("select uo from UO uo left join uo.typeUO typ where typ.code=:CODE");
			query.setParameter("CODE", code);			
			uos = (List<UO>)query.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return uos;
	}

	@Override
	public UO findUObyType(String code) {
		// TODO Auto-generated method stub
		
		UO uo =null;
		try {
			javax.persistence.Query query = em.createQuery("select uo from UO uo left join uo.typeUO typ where typ.code=:CODE");
			query.setParameter("CODE", code);	
			query.setMaxResults(1);
			uo = (UO)query.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return uo;
	}

	@Override
	public List<UO> searchListUOByType(String code, String search){
		List<UO> uos =null;
		try {
			
			String sql="select uo from UO uo left join uo.typeUO typ  where 1=1 ";
			
			if(code!=null && !code.equals(""))
			{
				sql+=" and typ.code=:CODE ";
			}
			
			if(search!=null && !search.equals(""))
			{
				sql+=" and uo.detail like :SEARCH ";
			}
			
			
			sql+=" order by uo.id desc ";
			
			javax.persistence.Query query = em.createQuery(sql);
			
			if(code!=null && !code.equals(""))
			{
				query.setParameter("CODE", code);	
			}
			
			if(search!=null && !search.equals(""))
			{
				query.setParameter("SEARCH", "%"+search+"%");	
			}

			
			uos = (List<UO>)query.getResultList();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return uos;
	}

	@Override
	public int saveImport(Set<UO> data) {
		// TODO Auto-generated method stub
		
		int nb=0;
	 	
		for (UO contact : data) {
			
			System.out.println("save "+nb);
			em.persist(contact);
			nb++;
		}
	
		return nb;
	}

	@Override
	public List<UO> getListUOByTypes(List<String> listcode) {
		// TODO Auto-generated method stub


		List<UO> uos =null;
		try {
			javax.persistence.Query query = em.createQuery("select uo from UO uo left join uo.typeUO typ where typ.code IN :CODE");
			query.setParameter("CODE", listcode);			
			uos = (List<UO>)query.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			
		}
		return uos;
	
	}

	/*@Override
	public void saveUO(UO uo, Contrat contrat) {
		// TODO Auto-generated method stub
		
		uo=em.merge(uo);
		
		ContratUO cuo=new ContratUO();
		cuo.setUo(uo);
		cuo.setContrat(contrat);
		em.persist(cuo);
		
	}*/

	 
	 
}
