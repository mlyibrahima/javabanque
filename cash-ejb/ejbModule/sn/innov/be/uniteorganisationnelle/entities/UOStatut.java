package sn.innov.be.uniteorganisationnelle.entities;

public enum UOStatut {

	VALIDE,SUSPENDED,CANCELED
}
