package sn.innov.be.uniteorganisationnelle.entities;


public class DetailBeneficiaire {

	String numero;
	String prenom;
	String nom;
	String cni;
	String dateNaissance;
 	String adresse;
 	String nomComplet;
 	
 	
 	
	public String getNomComplet() {
		return nomComplet=(prenom+ "  "+ nom).toUpperCase();
	}
	public void setNomComplet(String nomComplet) {
		this.nomComplet = nomComplet;
	}
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCni() {
		return cni;
	}
	public void setCni(String cni) {
		this.cni = cni;
	}
	public String getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
 	
 	
}
