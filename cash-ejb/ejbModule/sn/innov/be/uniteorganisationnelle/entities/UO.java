package sn.innov.be.uniteorganisationnelle.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="uos")
public class UO implements Serializable{
	
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	private Long id;
	private String nom;
	String reference;
	private UO parent;
	private TypeUO typeUO;
	String prenomContact;
	String nomContact;
	String nomComplet;
	String telephoneContact;
	String emailContact;
	String cniContact;
	String adresseContact;
 	UOStatut statut;

	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="UO_ID")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="UO_NOM")
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	
	@ManyToOne
	@JoinColumn(name="UO_UO_ID")
	public UO getParent() {
		return parent;
	}
	public void setParent(UO parent) {
		this.parent = parent;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="UO_TYP_CODE")	
	public TypeUO getTypeUO() {
		return typeUO;
	}
	public void setTypeUO(TypeUO typeUO) {
		this.typeUO = typeUO;
	}
	
	@Enumerated(EnumType.STRING)
	public UOStatut getStatut() {
		return statut;
	}
	public void setStatut(UOStatut statut) {
		this.statut = statut;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getPrenomContact() {
		return prenomContact;
	}
	public void setPrenomContact(String prenomContact) {
		this.prenomContact = prenomContact;
	}
	public String getNomContact() {
		return nomContact;
	}
	public void setNomContact(String nomContact) {
		this.nomContact = nomContact;
	}
	public String getNomComplet() {
		return prenomContact + " " + nomContact;
	}
	public void setNomComplet(String nomComplet) {
		this.nomComplet = nomComplet;
	}
	public String getTelephoneContact() {
		return telephoneContact;
	}
	public void setTelephoneContact(String telephoneContact) {
		this.telephoneContact = telephoneContact;
	}
	public String getEmailContact() {
		return emailContact;
	}
	public void setEmailContact(String emailContact) {
		this.emailContact = emailContact;
	}
	public String getCniContact() {
		return cniContact;
	}
	public void setCniContact(String cniContact) {
		this.cniContact = cniContact;
	}
	public String getAdresseContact() {
		return adresseContact;
	}
	public void setAdresseContact(String adresseContact) {
		this.adresseContact = adresseContact;
	}
	
	
	
}