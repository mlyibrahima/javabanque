package sn.innov.be.uniteorganisationnelle.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="typeuo")
public class TypeUO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String code;
	String libelle;
	TypeUO parent;
	
	@Id
	@Column(name="TUO_CODE")
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	@Column(name="TUO_LIBELLE")
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@ManyToOne
	@JoinColumn(name="TYPEUO_TYPEUO_ID")
	public TypeUO getParent() {
		return parent;
	}
	public void setParent(TypeUO parent) {
		this.parent = parent;
	}

	

}
