import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import sn.innov.web.banque.entities.Client;
import sn.innov.web.banque.entities.Compte;
import sn.innov.web.banque.services.ClientRemote;

public class CLientJavaRemote {

	public static void main(String[] args) {
		try {
			Context ctx = new InitialContext();
			String appName="";
			String moduleName="cash-ejb";
			String beanName="BK";
			String remoteInterface=ClientRemote.class.getName();
			String name="ejb:"+appName+"/"+moduleName+"/"+beanName+"!"+remoteInterface;
			
			ClientRemote proxy=(ClientRemote) ctx.lookup(name);
			
			proxy.addClient(new Client());
			proxy.addClient(new Client());
			proxy.addClient(new Client());
			
			/*proxy.addCompte(new Compte(), 1L);
			proxy.addCompte(new Compte(), 2L);
			proxy.addCompte(new Compte(), 3L);*/
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
