package sn.innov.web.banque.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.uniteorganisationnelle.entities.TypeUO;
import sn.innov.be.uniteorganisationnelle.services.UOService;
import sn.innov.web.banque.entities.Client;
import sn.innov.web.banque.entities.Compte;
import sn.innov.web.banque.entities.Operation;
import sn.innov.web.banque.entities.TypeCompte;
import sn.innov.web.banque.entities.TypeOperation;
import sn.innov.web.banque.services.ClientBean;
import sn.innov.web.banque.services.ClientRemote;
import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;

public class AddOperation extends WindowController{

	Operation operation = null;
	@Wire
	Textbox txtMontant;
	
	@Wire
	Bandbox bdCompte1, bdCompte2;
	@Wire				
	Listbox listComptes1, listComptes2, listOperations;
	
	public static final String COMPTE = "compte";
	public static final String COMPTE2 = "compte";
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		HashMap<String, Object> params = (HashMap<String, Object>) Executions.getCurrent().getArg();		
		operation = (Operation) params.get("operation");
		/*
		 * if(compte.getType()!= null && compte.getType().equals(TypeCompte.COURANT))
		 * listClients.getSelectedItem().setValue("COURANT"); else if(compte.getType()!=
		 * null && compte.getType().equals(TypeCompte.EPARGNE)){
		 * listClients.getSelectedItem().setValue("EPARGNE"); }
		 */
		
		txtMontant.setValue(operation.getMontant()+"");
		
		
		if(operation.getCompte() != null) {
			bdCompte1.setValue(operation.getCompte().getNumCompte());
			bdCompte1.setAttribute(COMPTE, operation.getCompte());
			bdCompte2.setValue(operation.getCompte().getNumCompte());
			bdCompte2.setAttribute(COMPTE, operation.getCompte());
		}
	}
	
	
	//@Listen("onOpen = #bdCompte1, #bdCompte2")
	@Listen("onOpen =#bdCompte1")
	public void chargerBookmark()
	{
		ClientRemote moduleService = (ClientRemote) JNDIUtils.lookUpEJB(EJBRegistry.ClientBean);
		List<Compte> compte = moduleService.consulterComptes();
		ListModelList<Compte> modulesModel = new ListModelList<Compte>(compte);
		listComptes1.setModel(modulesModel);
	}
	
	@Listen("onOpen =#bdCompte2")
	public void chargerBookmark2()
	{
		ClientRemote moduleService = (ClientRemote) JNDIUtils.lookUpEJB(EJBRegistry.ClientBean);
		List<Compte> compte = moduleService.consulterComptes();
		ListModelList<Compte> modulesModel2 = new ListModelList<Compte>(compte);
		listComptes2.setModel(modulesModel2);
	}
	
	@Listen(" onSelect = #listComptes1")
	public void onSelectModule(){
		if(listComptes1.getSelectedItem() != null ) {
			Compte mod = listComptes1.getSelectedItem().getValue();
			bdCompte1.setValue(mod.getNumCompte());
			bdCompte1.setAttribute(COMPTE, mod);
		}
		
		bdCompte1.close();
		
	}
	
	@Listen(" onSelect =#listComptes2")
	public void onSelectModule2(){
		if(listComptes2.getSelectedItem() != null ) {
			Compte mod = listComptes2.getSelectedItem().getValue();
			bdCompte2.setValue(mod.getNumCompte());
			bdCompte2.setAttribute(COMPTE2, mod);
		}
		
		bdCompte2.close();
		
	}
	
	@Listen("onOK = window")
	public void onOK(Event event)
	{
		if(bdCompte1.getValue().isEmpty())
		{alert("Attention, champs manquants");return;}
		System.out.println("onOK function");
		ClientRemote clientRemote = (ClientRemote) JNDIUtils.lookUpEJB(EJBRegistry.ClientBean);
		double montant = Double.parseDouble(txtMontant.getValue());
		if(listComptes1.getSelectedItem() != null && listComptes2.getSelectedItem() != null) {
			System.out.println("Ok");
			Compte c1 = (Compte) listComptes1.getSelectedItem().getValue();
			operation.setCompte(c1);
			Compte c2 = (Compte) listComptes2.getSelectedItem().getValue();
			//operation.setCompte2(c2);
			if(listOperations.getSelectedItem() != null && listOperations.getSelectedItem().getValue().toString().equals("VIREMENT")) {
				System.out.println("VIREMENT");
				//operation.setType(TypeOperation.VIREMENT);
				clientRemote.virement(c1, c2, montant);
			}
			else if(listOperations.getSelectedItem() != null && listOperations.getSelectedItem().getValue().toString().equals("PRELEVEMENT")) {
				//operation.setType(TypeOperation.PRELEVEMENT);
				System.out.println("PRELEV");
				clientRemote.virement(c2, c1, montant);
			}
			/*
			 * if(listOperations.getSelectedItem() != null &&
			 * listOperations.getSelectedItem().getValue().toString().equals("DEPOT")) {
			 * System.out.println("DEPOT"); //operation.setType(TypeOperation.VIREMENT);
			 * clientRemote.verser(c1.getidCompte(), montant); } else
			 * if(listOperations.getSelectedItem() != null &&
			 * listOperations.getSelectedItem().getValue().toString().equals("RETRAIT")) {
			 * //operation.setType(TypeOperation.PRELEVEMENT); System.out.println("PRELEV");
			 * clientRemote.retirer(c1.getidCompte(), montant); }
			 */
		}		
		
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getParent().getSpaceOwner()), null);
		getSelf().detach();		
	}
	
	@Listen("onAnnuler = window")
	public void onAnnuler(){
		getSelf().detach();
	}
}
