package sn.innov.web.banque.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.uniteorganisationnelle.entities.TypeUO;
import sn.innov.be.uniteorganisationnelle.services.UOService;
import sn.innov.web.banque.entities.Client;
import sn.innov.web.banque.entities.Compte;
import sn.innov.web.banque.entities.TypeCompte;
import sn.innov.web.banque.services.ClientBean;
import sn.innov.web.banque.services.ClientRemote;
import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;

public class AddCompte extends WindowController{

	Compte compte = null;
	@Wire
	Textbox txtNumCompte, txtSolde;
	
	@Wire
	Bandbox bdClient;
	@Wire
	Listbox listClients, listComptes;
	
	public static final String CLIENT = "client";
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		HashMap<String, Object> params = (HashMap<String, Object>) Executions.getCurrent().getArg();		
		compte = (Compte) params.get("compte");
		/*
		 * if(compte.getType()!= null && compte.getType().equals(TypeCompte.COURANT))
		 * listClients.getSelectedItem().setValue("COURANT"); else if(compte.getType()!=
		 * null && compte.getType().equals(TypeCompte.EPARGNE)){
		 * listClients.getSelectedItem().setValue("EPARGNE"); }
		 */
		
		txtSolde.setValue(compte.getSolde()+"");
		txtNumCompte.setValue(compte.getNumCompte());
		
		
		if(compte.getClient() != null) {
			bdClient.setValue(compte.getClient().getPrenom());
			bdClient.setAttribute(CLIENT, compte.getClient());
		}
	}
	
	
	@Listen("onOpen = #bdClient")
	public void chargerBookmark()
	{
		ClientRemote moduleService = (ClientRemote) JNDIUtils.lookUpEJB(EJBRegistry.ClientBean);
		List<Client> client = moduleService.consulterClients();
		ListModelList<Client> modulesModel = new ListModelList<Client>(client);
		listClients.setModel(modulesModel);
	}
	
	@Listen(" onSelect = #listClients ")
	public void onSelectModule(){
		if(listClients.getSelectedItem() != null ) {
			Client mod = listClients.getSelectedItem().getValue() ;
			bdClient.setValue(mod.getPrenom());
			bdClient.setAttribute(CLIENT, compte.getClient());
		}
		
		bdClient.close();
		
	}
	
	@Listen("onOK = window")
	public void onOK(Event event)
	{
		if(bdClient.getValue().isEmpty())
		{alert("Attention, champs manquants");return;}
		
		if(listComptes.getSelectedItem()!=null && listComptes.getSelectedItem().getValue().toString().equals("COURANT")) {
			compte.setType(TypeCompte.COURANT);
		}
		else if(listComptes.getSelectedItem()!=null && listComptes.getSelectedItem().getValue().toString().equals("EPARGNE")) {
			compte.setType(TypeCompte.EPARGNE);	
		}
		
		if(listClients.getSelectedItem() !=null) {
			Client cli = (Client) listClients.getSelectedItem().getValue();
			compte.setClient(cli);
		}
		
		compte.setNumCompte(txtNumCompte.getValue());
		compte.setSolde(Double.parseDouble(txtSolde.getValue()));
		
		
		
		
		ClientRemote clientRemote = (ClientRemote) JNDIUtils.lookUpEJB(EJBRegistry.ClientBean);
				
		
		try {
			Long id = Long.MAX_VALUE;
			if(compte.getidCompte()!=null)
				id = compte.getidCompte();
			if(clientRemote.consulterCompte(id)==null) {
				clientRemote.addCompte(compte);
			}else {
				clientRemote.modifierCompte(compte);
			}
			Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getParent().getSpaceOwner()), null);
			getSelf().detach();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@Listen("onAnnuler = window")
	public void onAnnuler(){
		getSelf().detach();
	}
}
