package sn.innov.web.banque.controllers;

import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Window;

import sn.innov.web.banque.entities.Client;
import sn.innov.web.banque.services.ClientRemote;
import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Module;
import sn.innov.be.security.services.ModuleService;

public class ListClientController extends WindowController {
	
	@Wire
	Listbox listClients;
	
	/**
	 * Liste des actions de cet �cran
	 */
	static final String CREER_CLIENT = "CREER_CLIENT", 
						MODIFIER_CLIENT = "MODIFIER_CLIENT",
						SUPPRIMER_CLIENT = "SUPPRIMER_CLIENT";
						//VOIR_MODULE = "VOIR_MODULE";
	
	/**
	 * Ressources appel�es par cet ecran
	 */
	static final String	URI_FORM="/WEB-INF/banque/client.zul";
						

	ClientRemote clientService = (ClientRemote) JNDIUtils.lookUpEJB(EJBRegistry.ClientBean);

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}
	
	/**
	 * Ecouteur de l'evenement onListChange pour recharger la liste des fonctions
	 */
	@Listen(EventConstants.ON_LIST_CHANGE+" = window")
	public void afficherListeModules()
	{
		
		List<Client> clients = clientService.consulterClients();
		ListModelList<Client> modelClient = new ListModelList<Client>(clients);
		listClients.setModel(modelClient);
	}
	
	/**
	 * Switch des �venements lanc�s par un action button
	 * @param event
	 */
	@Listen(ON_CLICK_ACTION_BUTTON+" = window")
	public void onClickActionButton(Event event)
	{
		String action = (String) event.getData();

		if(action.equalsIgnoreCase(CREER_CLIENT)){
			HashMap<String, Object> params = new HashMap<String, Object>();
			
			params.put("client", new Client());
			loadPopUp(URI_FORM, params );
			
		}else if(action.equalsIgnoreCase(MODIFIER_CLIENT)){
			if(listClients.getSelectedItem() != null){
				HashMap<String, Object> params = new HashMap<String, Object>();
				
				params.put("client", (Client)listClients.getSelectedItem().getValue());
				loadPopUp(URI_FORM, params );
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
			
		}else if(action.equalsIgnoreCase(SUPPRIMER_CLIENT)){
			if(listClients.getSelectedItem() != null){
				
				confirm("Etes vous sur de bien vouloire supprimer l element?", "Suppression client",
					new EventListener<Messagebox.ClickEvent>() {
					
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if(event.getButton() == Button.YES){
							Events.postEvent(EventConstants.ON_DELETE,((Window)getSelf().getSpaceOwner()), null);	
						}else if(event.getButton() == Button.NO) {
		
						}
					}	
				});
				
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
			
		}
	}
	
	/**
	 * Ecouteur de l'evenment: suppression confirm�e
	 */
	@Listen(EventConstants.ON_DELETE+" = window")
	public void supprimerModules()
	{
		Client client = listClients.getSelectedItem().getValue();
		clientService.supprimerClient(client.getidClient());
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}
	
}
