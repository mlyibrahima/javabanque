package sn.innov.web.banque.controllers;

import java.util.HashMap;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.web.banque.entities.Client;
import sn.innov.web.banque.services.ClientBean;
import sn.innov.web.banque.services.ClientRemote;
import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;

public class AddClient extends WindowController{

	Client client = null;
	@Wire
	Textbox txtNom, txtPrenom, txtNumTelephone, txtAdresse;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		HashMap<String, Object> params = (HashMap<String, Object>) Executions.getCurrent().getArg();		
		client = (Client) params.get("client");
		txtNom.setValue(client.getNom());
		txtPrenom.setValue(client.getPrenom());
		txtNumTelephone.setValue(client.getNumTelephone());
		txtAdresse.setValue(client.getAdresse());	
	}
	
	@Listen("onOK = window")
	public void onOK(Event event)
	{
		if(txtNom.getValue().isEmpty())
		{alert("Attention, champs manquants");return;}
		
		client.setNom(txtNom.getValue());
		client.setPrenom(txtPrenom.getValue());
		client.setNumTelephone(txtNumTelephone.getValue());
		client.setAdresse(txtAdresse.getValue());
		ClientRemote clientBean = (ClientRemote) JNDIUtils.lookUpEJB(EJBRegistry.ClientBean);
		
		try {
			
			Long id = Long.MAX_VALUE;
			if(client.getidClient()!=null)
				id = client.getidClient();
			if(clientBean.consulterClient(id)== null)
			 clientBean.addClient(client);
			else {
				clientBean.modifierClient(client);
			}
			Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getParent().getSpaceOwner()), null);
			getSelf().detach();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@Listen("onAnnuler = window")
	public void onAnnuler(){
		getSelf().detach();
	}
}
