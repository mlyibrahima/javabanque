package sn.innov.web.reporting.controllers;


import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.ngi.zhighcharts.SimpleExtXYModel;
import org.ngi.zhighcharts.ZHighCharts;
import org.zkoss.util.logging.Log;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.SimplePieModel;
import org.zkoss.zul.Window;

public class ReportingController extends Window implements EventListener {
	protected Log logger = Log.lookup(this.getClass());
	
	//Basic Line	
	private ZHighCharts chartComp;
	private SimpleExtXYModel dataChartModel = new SimpleExtXYModel();
	
	//Pie with legend
	private ZHighCharts chartComp20, chartComp30;
	private SimplePieModel pieModel20 = new SimplePieModel();
	private SimplePieModel pieModel30 = new SimplePieModel();


	// date format used to capture date time
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
							
	public void onCreate() throws Exception {

		displayPie20();
		displayPie30();
		displayCourbe();
		
			
	}
	
	private void displayPie30() {

		//================================================================================
	    // Pie chart
	    //================================================================================

		chartComp30 = (ZHighCharts) getFellow("chartComp30");
		chartComp30.setTitle("Résultats plaintes");
		chartComp30.setSubTitle("2014");
		chartComp30.setType("pie");
		chartComp30.setTooltipFormatter("function formatTooltip(obj){" +
					"return obj.key + '<br /> requetes: <b>'+obj.y+'%</b>'" +
				"}");
		chartComp30.setPlotOptions("{" +
				"pie:{" +
					"allowPointSelect: true," +
					"cursor: 'pointer'," +
					"dataLabels: {" +
						"enabled: true, " +
						"color: '#000000'," +
						"connectorColor: '#000000'," +
						"formatter: function() {"+
                            "return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';"+
                        "}"+
					"}" +
				"}" +
			"}");							
		chartComp30.setModel(pieModel30);	
		
		//Adding some data to the model
		pieModel30.setValue("Fautif", 15);
		pieModel30.setValue("Non fautif", 55);
		pieModel30.setValue("Contentieux", 30);
		
	
		
	}
	
	private void displayPie20() {

		//================================================================================
	    // Pie chart
	    //================================================================================

		chartComp20 = (ZHighCharts) getFellow("chartComp20");
		chartComp20.setTitle("Répartition des requêtes de clients en 2014");
		chartComp20.setSubTitle("2014");
		chartComp20.setType("pie");
		chartComp20.setTooltipFormatter("function formatTooltip(obj){" +
				"return obj.key + '<br />Browser Share: <b>'+obj.y+'%</b>'" +
			"}");
		chartComp20.setPlotOptions("{" +
				"pie:{" +
				"allowPointSelect: true," +
				"cursor: 'pointer'," +
				"dataLabels: {" +
					"enabled: true, " +
					"color: '#000000'," +
					"connectorColor: '#000000'," +
					"formatter: function() {"+
                        "return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';"+
                    "}"+
				"}" +
			"}" +
		"}");							
		chartComp20.setModel(pieModel20);	
		
		//Adding some data to the model
		pieModel20.setValue("Demandes infos", 45.0);
		pieModel20.setValue("Rendezvous", 24);
		pieModel20.setValue("Plaintes", 31);
		
	
		
	}

	private void displayCourbe() {
		//================================================================================
	    // Basic Line
	    //================================================================================

		chartComp = (ZHighCharts) getFellow("chartComp");
		chartComp.setTitle("Délais moyens de traitement des requêtes de clients");
		chartComp.setSubTitle("Source: innov");
		chartComp.setType("line");
		chartComp.setXAxisTitle("Mois");
		chartComp.setxAxisOptions("{" +
					"categories: [" +
						"'Jan', " +
						"'Fev', " +
						"'Mar', " +
						"'Avr', " +
						"'Mai', " +
						"'Jui', " +
						"'Jul', " +
						"'Aou', " +
						"'Sep', " +
						"'Oct', " +
						"'Nov', " +
						"'Dec'" +
					"]" +
				"}");
		chartComp.setYAxisTitle("Délais de traitements (Jours)");
		chartComp.setyAxisOptions("{" +
					"plotLines: [" +
					"{" +
						"value: 0, " +
						"width: 1," +
						"color: '#808080' " +
					"}]" +
				"}");
		chartComp.setLegend("{" +
					"layout: 'vertical'," +
					"align: 'right'," +
					"verticalAlign: 'top'," +
					"x: -10," +
					"y: 100," +
					"borderWidth: 0" +
				"}");
		chartComp.setPlotOptions("{" +
				"series:{" +
					"dataLabels:{" +
						"formatter: function (){return this.y +'J';}," + 
						"enabled: true" +
					"}" +
				"}" +
			"}");
		chartComp.setTooltipFormatter("function formatTooltip(obj){ " +
					"return '<b>'+ obj.series.name +'</b>" +
					"<br/>'+ obj.x +': '+ obj.y +'Jours';" +
				"}");
		
		chartComp.setModel(dataChartModel);
		
		//Adding some data to the model
		dataChartModel.addValue("Plaintes", 0, 7);
		dataChartModel.addValue("Plaintes", 1, 16);
		dataChartModel.addValue("Plaintes", 2, 19);
		dataChartModel.addValue("Plaintes", 3, 20);
		dataChartModel.addValue("Plaintes", 4, 25);
		dataChartModel.addValue("Plaintes", 5, 28);
		dataChartModel.addValue("Plaintes", 6, 29);
		dataChartModel.addValue("Plaintes", 7, 33);
		dataChartModel.addValue("Plaintes", 8, 35);
		dataChartModel.addValue("Plaintes", 9, 37);
		dataChartModel.addValue("Plaintes", 10, 21);
		dataChartModel.addValue("Plaintes", 11, 23);

		dataChartModel.addValue("Rendez-vous", 0, 2);
		dataChartModel.addValue("Rendez-vous", 1, 8);
		dataChartModel.addValue("Rendez-vous", 2, 5);
		dataChartModel.addValue("Rendez-vous", 3, 11);
		dataChartModel.addValue("Rendez-vous", 4, 17);
		dataChartModel.addValue("Rendez-vous", 5, 22);
		dataChartModel.addValue("Rendez-vous", 6, 24);
		dataChartModel.addValue("Rendez-vous", 7, 24);
		dataChartModel.addValue("Rendez-vous", 8, 20);
		dataChartModel.addValue("Rendez-vous", 9, 14);
		dataChartModel.addValue("Rendez-vous", 10, 8);
		dataChartModel.addValue("Rendez-vous", 11, 6);
		
		dataChartModel.addValue("Demandes infos", 0, 9);
		dataChartModel.addValue("Demandes infos", 1, 6);
		dataChartModel.addValue("Demandes infos", 2, 3);
		dataChartModel.addValue("Demandes infos", 3, 8);
		dataChartModel.addValue("Demandes infos", 4, 13);
		dataChartModel.addValue("Demandes infos", 5, 17);
		dataChartModel.addValue("Demandes infos", 6, 18);
		dataChartModel.addValue("Demandes infos", 7, 17);
		dataChartModel.addValue("Demandes infos", 8, 14);
		dataChartModel.addValue("Demandes infos", 9, 9.0);
		dataChartModel.addValue("Demandes infos", 10, 3.9);
		dataChartModel.addValue("Demandes infos", 11, 1.0);
	}
	
	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * internal method to convert date&amp;time from string to epoch milliseconds
	 * 
	 * @param date
	 * @return
	 * @throws Exception
	 */
	private long getDateTime(String date) throws Exception {
		return sdf.parse(date).getTime();
	}

}
 

