package sn.innov.web.securite.controllers;

import java.util.HashMap;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Module;
import sn.innov.be.security.services.ModuleService;
public class FormModuleController extends WindowController {
	
	Module module = null;
	@Wire 
	Textbox txtCode,txtDescription, txtImage;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		HashMap<String, Object> params = (HashMap<String, Object>) Executions.getCurrent().getArg();		
		module = (Module) params.get("module");
		txtCode.setValue(module.getCode());
		txtImage.setValue(module.getImage());
		txtDescription.setValue(module.getDescription());
		
	}
	
	@Listen("onOK = window")
	public void onOK(Event event)
	{
		if(txtCode.getValue().isEmpty())
		{alert("Attention, champs manquants");return;}
		
		module.setCode(txtCode.getValue());
		module.setDescription(txtDescription.getValue());
		module.setImage(txtImage.getValue());
		ModuleService moduleService = (ModuleService) JNDIUtils.lookUpEJB(EJBRegistry.ModuleServiceBean);
		
		try {
			moduleService.modifierModule(module);
			Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getParent().getSpaceOwner()), null);
			getSelf().detach();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@Listen("onAnnuler = window")
	public void onAnnuler(){
		getSelf().detach();
	}
}
