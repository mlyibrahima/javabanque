package sn.innov.web.securite.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Action;
import sn.innov.be.security.entities.Fonctionnalite;
import sn.innov.be.security.services.ActionService;
import sn.innov.be.security.services.FonctionnaliteService;

public class FormActionController extends WindowController {
	
	private static final String CODE_FONC = "codeFonc";
	Action  action = null;
	@Wire
	Bandbox bdFonctionnalite;
	@Wire
	Listbox listFonctionnalite;
	@Wire 
	Textbox txtCode,txtDescription;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		HashMap<String, Object> params = (HashMap<String, Object>)Executions.getCurrent().getArg();		
		action = (Action) params.get("action");
		txtCode.setValue(action.getCode());
		txtDescription.setValue(action.getDescription());
		Fonctionnalite fonct = action.getFonctionnalite() ;
		if(fonct != null){
			bdFonctionnalite.setValue(fonct.getDescription());
			bdFonctionnalite.setAttribute(CODE_FONC, fonct.getCode());
		}
		
	}
	
	
	@Listen("onOpen = #bdFonctionnalite")
	public void chargerFonctionnalite()
	{

		FonctionnaliteService fonctionnaliteService = (FonctionnaliteService) JNDIUtils.lookUpEJB(EJBRegistry.FonctionnaliteServiceBean);
		List<Fonctionnalite> fonctionnalites = fonctionnaliteService.listerFonctionnalites();
		ListModelList<Fonctionnalite> fonctionnaliteModel = new ListModelList<Fonctionnalite>(fonctionnalites);
		listFonctionnalite.setModel(fonctionnaliteModel);
	}
	
	@Listen(" onSelect = #listFonctionnalite ")
	public void onChargerFonctionnalites(){
		if(listFonctionnalite.getSelectedItem() != null) {
			Fonctionnalite fonct = listFonctionnalite.getSelectedItem().getValue() ;
			bdFonctionnalite.setValue(fonct.getDescription());
			bdFonctionnalite.setAttribute(CODE_FONC, fonct.getCode()) ;
			bdFonctionnalite.close();
		}
		
	}
	
	@Listen("onOK = window")
	public void onOK(Event event)
	{
		
		if(txtCode.getValue().isEmpty() )
		{alert("Attention, champs manquants");return;}
		FonctionnaliteService fonctionnaliteService = (FonctionnaliteService) JNDIUtils.lookUpEJB(EJBRegistry.FonctionnaliteServiceBean);
		String codeFonct = (String) bdFonctionnalite.getAttribute(CODE_FONC);
		Fonctionnalite fonctionnalite =  codeFonct != null ?fonctionnaliteService.getFonctionnalite(codeFonct) : null ;
		action.setCode(txtCode.getValue());
		action.setFonctionnalite(fonctionnalite);
		action.setDescription(txtDescription.getValue());
		ActionService actionService = (ActionService) JNDIUtils.lookUpEJB(EJBRegistry.ActionServiceBean);
		
		try {
			actionService.modifierAction(action);
			Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getParent().getSpaceOwner()), null);
			getSelf().detach();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@Listen("onAnnuler = window")
	public void onAnnuler(){
		getSelf().detach();
	}
}
