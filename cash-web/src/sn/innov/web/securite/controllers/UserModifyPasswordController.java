package sn.innov.web.securite.controllers;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.web.authentification.controllers.ChangementPasswordController;
import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.common.utils.SessionAttributes;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.core.utils.Utils;
import sn.innov.be.security.services.UtilisateurService;

public class UserModifyPasswordController extends WindowController {


	private static final long serialVersionUID = 1L;

	@Wire
	Textbox txtPassword1, txtPassword2, txtPassword;
	
	UtilisateurService userService = (UtilisateurService) JNDIUtils.lookUpEJB(EJBRegistry.UtilisateurServiceBean);
	Utilisateur user = (Utilisateur) Sessions.getCurrent().getAttribute(SessionAttributes.CONNECTED_USER);
	
@Override
	public void doAfterCompose(Component comp){
		try {
			super.doAfterCompose(comp);

		}catch (Exception e) { e.printStackTrace(); if(!e.getMessage().isEmpty()){alert("Attention: "+e.getMessage());} }		
	}

@Listen("onAnnuler = window")
public void onAnnuler()
{
	getSelf().detach();

}

	@Listen("onModifyPasswordUser = window")
	public void onModifyPasswordUser(Event event)
	{
		try{ 
				if(!Utils.getEncodedPassword(txtPassword.getValue()).equalsIgnoreCase(user.getPassword())){
					Messagebox.show("Ancien mot de passe erronne", "Erreur au niveau des mots de passe", Messagebox.OK, Messagebox.ERROR);
					return;
				}else if(!txtPassword1.getValue().equalsIgnoreCase(txtPassword2.getValue())){
					Messagebox.show("Attention, les deux mots de passe ne correspondent pas", "Erreur au niveau des mots de passe", Messagebox.OK, Messagebox.ERROR);
					return;
				}else {
					user.setPassword(Utils.getEncodedPassword(txtPassword1.getValue()));
					
					userService.modifierUtilisateur(user);
					Events.postEvent(EventConstants.ON_LIST_CHANGE,getSelf().getParent(), null);
					getSelf().detach();
					Messagebox.show("Votre mot de passe a ete modifie avec succes", "Changement de mot de passe effectue", Messagebox.OK, Messagebox.INFORMATION);

				}
	} catch (Exception e) { e.printStackTrace(); if(!e.getMessage().isEmpty()){alert("Attention: "+e.getMessage());} }
	}
}