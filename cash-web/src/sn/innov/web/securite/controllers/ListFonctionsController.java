package sn.innov.web.securite.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.authentification.entities.Fonction;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.services.FonctionService;

public class ListFonctionsController extends WindowController {
	
	@Wire
	Listbox listFonctions;
	
	/**
	 * Liste des actions de cet �cran
	 */
	static final String CREER_FONCTION = "CREER_FONCTION", 
						MODIFIER_FONCTION = "MODIFIER_FONCTION",
						SUPPRIMER_FONCTION = "SUPPRIMER_FONCTION",
						VOIR_FONCTION = "VOIR_FONCTION",
						ACTION_FONCTION = "ACTION_FONCTION";
	
	/**
	 * Ressources appel�es par cet ecran
	 */
	static final String	URI_FORM="/WEB-INF/securite/referentiel/formFonction.zul",
						URI_ACTION="/WEB-INF/securite/referentiel/actionsFonction.zul",
						URI_FICHE="/WEB-INF/securite/referentiel/ficheFonction.zul";

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}
	
	/**
	 * Ecouteur de l'evenement onListChange pour recharger la liste des fonctions
	 */
	@Listen(EventConstants.ON_LIST_CHANGE+" = window")
	public void afficherListeFonction()
	{
		FonctionService fonctionService = (FonctionService) JNDIUtils.lookUpEJB(EJBRegistry.FonctionServiceBean);
		List<Fonction> fonctions = fonctionService.lister();
		ListModelList<Fonction> modelFonction = new ListModelList<Fonction>(fonctions);
		listFonctions.setModel(modelFonction);
	}
	
	/**
	 * Switch des �venements lanc�s par un action button
	 * @param event
	 */
	@Listen(ON_CLICK_ACTION_BUTTON+" = window")
	public void onClickActionButton(Event event)
	{
		String action = (String) event.getData();

		if(action.equalsIgnoreCase(CREER_FONCTION)){
			HashMap<String, Object> params = new HashMap<String, Object>();
			
			params.put("fonction", new Fonction());
			loadPopUp(URI_FORM, params );
			
		}else if(action.equalsIgnoreCase(MODIFIER_FONCTION)){
			if(listFonctions.getSelectedItem() != null){
				HashMap<String, Object> params = new HashMap<String, Object>();
				
				params.put("fonction", (Fonction)listFonctions.getSelectedItem().getValue());
				loadPopUp(URI_FORM, params );
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
			
		}else if(action.equalsIgnoreCase(SUPPRIMER_FONCTION)){
			if(listFonctions.getSelectedItem() != null){
				
				confirm("Etes vous sur de bien vouloire supprimer l element?", "Suppression fonction",
					new EventListener<Messagebox.ClickEvent>() {
					
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if(event.getButton() == Button.YES){
							Events.postEvent(EventConstants.ON_DELETE,((Window)getSelf().getSpaceOwner()), null);	
						}else if(event.getButton() == Button.NO) {
		
						}
					}	
				});
				
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
			
		}else if(action.equalsIgnoreCase(VOIR_FONCTION)){
			if(listFonctions.getSelectedItem() != null){
				HashMap<String, Object> params = new HashMap<String, Object>();
				
				params.put("fonction", (Fonction)listFonctions.getSelectedItem().getValue());
				loadPopUp(URI_FICHE, params );
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
		}else if(action.equalsIgnoreCase(ACTION_FONCTION)){
			if(listFonctions.getSelectedItem() != null){
				HashMap<String, Object> params = new HashMap<String, Object>();
				
				params.put("fonction", (Fonction)listFonctions.getSelectedItem().getValue());
				loadPopUp(URI_ACTION, params );
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
		}
	}
	
	/**
	 * Ecouteur de l'evenment: suppression confirm�e
	 */
	@Listen(EventConstants.ON_DELETE+" = window")
	public void supprimerFonction()
	{
		FonctionService fonctionService = (FonctionService) JNDIUtils.lookUpEJB(EJBRegistry.FonctionServiceBean);
		Fonction fonction = listFonctions.getSelectedItem().getValue();
		if(fonctionService.supprimer(fonction) == null) 
		{alert("Suppression impossible");return;}
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}
	
}
