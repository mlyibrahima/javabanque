package sn.innov.web.securite.controllers;

import java.util.HashMap;

import javax.servlet.ServletContext;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.ContextAttributes;
import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.core.bookmarks.State;
import sn.innov.web.core.bookmarks.States;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.authentification.entities.Fonction;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.services.FonctionService;

public class FormFonctionController extends WindowController {
	
	Fonction  fonction = null;
	@Wire
	Bandbox bdWelcomeBookmark;
	@Wire
	Listbox listWelcomeBookmark;
	@Wire 
	Textbox txtCode,txtDescription;
	
	
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		HashMap<String, Object> params = (HashMap<String, Object>) Executions.getCurrent().getArg();		
		fonction = (Fonction) params.get("fonction");
		
		txtCode.setValue(fonction.getCode());
		txtDescription.setValue(fonction.getDescription());
		bdWelcomeBookmark.setValue(fonction.getWelcomeBookmark());
	
	}
	
	

	
	@Listen("onOpen = #bdWelcomeBookmark")
	public void chargerBookmark()
	{
		ServletContext context = (ServletContext) Executions.getCurrent()
				.getDesktop().getWebApp().getNativeContext();
		States states = (States) context.getAttribute(ContextAttributes.STATES);
		ListModelList<State> statesModel = new ListModelList<State>(states.getStates());
		listWelcomeBookmark.setModel(statesModel);
	}
	
	@Listen("onOK = window")
	public void onOK(Event event)
	{
		if(txtCode.getValue().isEmpty() || bdWelcomeBookmark.getValue().isEmpty())
		{alert("Attention, champs manquants");return;}
		
		fonction.setCode(txtCode.getValue());
		fonction.setWelcomeBookmark(bdWelcomeBookmark.getValue());
		fonction.setDescription(txtDescription.getValue());
		
	
		FonctionService fonctionService = (FonctionService) JNDIUtils.lookUpEJB(EJBRegistry.FonctionServiceBean);
		
		try {
			fonctionService.modifier(fonction);
			Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getParent().getSpaceOwner()), null);
			getSelf().detach();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@Listen("onAnnuler = window")
	public void onAnnuler(){
		getSelf().detach();
	}
	
}
