package sn.innov.web.securite.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Action;
import sn.innov.be.security.entities.Fonctionnalite;
import sn.innov.be.security.entities.Module;
import sn.innov.be.security.services.FonctionnaliteService;
import sn.innov.be.security.services.ModuleService;

public class ListFonctionnaliteController extends WindowController {

	@Wire
	Listbox listFonctionnalites;
	
	@Wire
	Bandbox bdModule;
	@Wire
	Listbox listModule;

	
	/**
	 * Liste des actions de cet �cran
	 */
	static final String CREER_FONCTIONNALITE = "CREER_FONCTIONNALITE", 
						MODIFIER_FONCTIONNALITE = "MODIFIER_FONCTIONNALITE",
						SUPPRIMER_FONCTIONNALITE = "SUPPRIMER_FONCTIONNALITE";
	
	/**
	 * Ressources appel�es par cet ecran
	 */
	static final String	URI_FORM="/WEB-INF/fonctionnalite/form.zul";
						
	ModuleService moduleService = (ModuleService) JNDIUtils.lookUpEJB(EJBRegistry.ModuleServiceBean);
	FonctionnaliteService fonctionnaliteService = (FonctionnaliteService) JNDIUtils.lookUpEJB(EJBRegistry.FonctionnaliteServiceBean);


	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}
	
	@Listen("onOpen = #bdModule")
	public void chargerModule()
	{

		List<Module> fonctionnalites = moduleService.listerModules();
		ListModelList<Module> fonctionnaliteModel = new ListModelList<Module>(fonctionnalites);
		listModule.setModel(fonctionnaliteModel);
	}
	
	@Listen(" onSelect = #listModule ")
	public void onChargerModules(){
		if(listModule.getSelectedItem() != null) {
			Module module= listModule.getSelectedItem().getValue() ;
			try {
				
		 		List<Fonctionnalite> fonctionnalites = fonctionnaliteService.listerFonctionnalites(module.getCode());
				ListModelList<Fonctionnalite> modelFonctionnalite = new ListModelList<Fonctionnalite>(fonctionnalites);
				listFonctionnalites.setModel(modelFonctionnalite);
	 			
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			bdModule.close();

		}
		
	}

	
	/**
	 * Ecouteur de l'evenement onListChange pour recharger la liste des fonctions
	 */
	@Listen(EventConstants.ON_LIST_CHANGE+" = window")
	public void afficherListeFonctionnalites()
	{
 		List<Fonctionnalite> fonctionnalites = fonctionnaliteService.listerFonctionnalites();
		ListModelList<Fonctionnalite> modelFonctionnalite = new ListModelList<Fonctionnalite>(fonctionnalites);
		listFonctionnalites.setModel(modelFonctionnalite);
	}
	
	/**
	 * Switch des �venements lanc�s par un action button
	 * @param event
	 */
	@Listen(ON_CLICK_ACTION_BUTTON+" = window")
	public void onClickActionButton(Event event)
	{
		String action = (String) event.getData();

		if(action.equalsIgnoreCase(CREER_FONCTIONNALITE)){
			HashMap<String, Object> params = new HashMap<String, Object>();
			
			params.put("fonctionnalite", new Fonctionnalite());
			loadPopUp(URI_FORM, params );
			
		}else if(action.equalsIgnoreCase(MODIFIER_FONCTIONNALITE)){
			if(listFonctionnalites.getSelectedItem() != null){
				HashMap<String, Object> params = new HashMap<String, Object>();
				
				params.put("fonctionnalite", (Fonctionnalite)listFonctionnalites.getSelectedItem().getValue());
				loadPopUp(URI_FORM, params );
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
			
		}else if(action.equalsIgnoreCase(SUPPRIMER_FONCTIONNALITE)){
			if(listFonctionnalites.getSelectedItem() != null){
				
				confirm("Etes vous sur de bien vouloire supprimer l element?", "Suppression fonctionnalite",
					new EventListener<Messagebox.ClickEvent>() {
					
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if(event.getButton() == Button.YES){
							Events.postEvent(EventConstants.ON_DELETE,((Window)getSelf().getSpaceOwner()), null);	
						}else if(event.getButton() == Button.NO) {
		
						}
					}	
				});
				
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
			
		}
	}
	
	/**
	 * Ecouteur de l'evenment: suppression confirm�e
	 */
	@Listen(EventConstants.ON_DELETE+" = window")
	public void supprimerFonctionnalites()
	{
		FonctionnaliteService fonctionnaliteService = (FonctionnaliteService) JNDIUtils.lookUpEJB(EJBRegistry.FonctionnaliteServiceBean);
		Fonctionnalite fonctionnalite = listFonctionnalites.getSelectedItem().getValue();
		if(fonctionnaliteService.supprimerFonctionnalite(fonctionnalite) == null) 
		{alert("Suppression impossible");return;}
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}

}
