package sn.innov.web.securite.controllers;

import java.util.HashMap;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;

import sn.innov.web.components.DualListboxReadOnly;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.authentification.entities.Fonction;
import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Action;
import sn.innov.be.security.services.ActionService;

public class FicheUtilisateurController extends WindowController {
	
	Utilisateur  utilisateur = null;
	@Wire
	Label lblProfil, lblPrenom, lblNom, lblFonction;
	@Wire
	Listbox listAction;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		HashMap<String, Object> params = (HashMap<String, Object>) Executions.getCurrent().getArg();		
		utilisateur = (Utilisateur) params.get("utilisateur");
		if(utilisateur != null)
		{
			lblProfil.setValue(utilisateur.getFonction().getDescription()) ;
			lblPrenom.setValue(utilisateur.getPrenom()) ;
			lblNom.setValue(utilisateur.getNom()) ;
			lblFonction.setValue(utilisateur.getPoste()) ;
		
			ActionService actionService = (ActionService) JNDIUtils.lookUpEJB(EJBRegistry.ActionServiceBean);
			listAction.setModel( new ListModelList<Action>(actionService.listActionsForFonction(utilisateur.getFonction().getCode()))  ) ;
			
			
		}
		
	}
	
}
