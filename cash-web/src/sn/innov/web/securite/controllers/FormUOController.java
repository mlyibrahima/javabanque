package sn.innov.web.securite.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.uniteorganisationnelle.entities.TypeUO;
import sn.innov.be.uniteorganisationnelle.entities.UO;
import sn.innov.be.uniteorganisationnelle.services.UOService;

public class FormUOController extends WindowController {


	private static final String ID_TYPEUO = "code";
	TypeUO typeUo = null;
	
	UO uo = null;
	UO uoParent=null;
	
	@Wire
	Bandbox bdTypeUO,bdUOParent;
 	@Wire
	Listbox listTypeUO,listUOParent;

 	@Wire 
	Textbox txtDescription;
	
	UOService uOService = (UOService) JNDIUtils.lookUpEJB(EJBRegistry.UOServiceBean);		
 
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		HashMap<String, Object> params = (HashMap<String, Object>) Executions.getCurrent().getArg();		
		uo = (UO) params.get("uos");
		txtDescription.setValue(uo.getNom());
		
		if(uo.getTypeUO()!=null)
			for (int i = 0; i < listTypeUO.getItemCount(); i++) {
				if(((TypeUO)listTypeUO.getItemAtIndex(i).getValue()).getCode().equals(uo.getTypeUO().getCode())){
					listTypeUO.setSelectedIndex(i);
					bdTypeUO.setValue(uo.getTypeUO().getLibelle());
					bdTypeUO.close();


				}
			}
		
		if(uo.getParent()!=null)
			for (int i = 0; i < listUOParent.getItemCount(); i++) {
				if(((UO)listUOParent.getItemAtIndex(i).getValue()).getId()==uo.getParent().getId()){
					listUOParent.setSelectedIndex(i);
					bdUOParent.setValue(uo.getParent().getNom());
					bdUOParent.close();


				}
			}
		
	//	txtCode.setValue(uo.getCode());
		/*
		bdTypeUOparent.setValue(uo.getParent().getNom());
		bdTypeUOparent.setAttribute(ID_TYPEUO, uo.getId());
		bdTypeUO.setValue(uo.getTypeUO().getLibelle());
		bdTypeUO.setAttribute(ID_TYPEUO, uo.getTypeUO().getId());*/
	}
	

	@Listen("onOpen = #bdTypeUO")
	public void chargerBookmark()
	{
 		List<TypeUO> typeUO = uOService.listerTypeUO();
		ListModelList<TypeUO> modulesModel = new ListModelList<TypeUO>(typeUO);
		listTypeUO.setModel(modulesModel);
	}
	
	@Listen(" onSelect = #listTypeUO ")
	public void onSelectModule(){
		if(listTypeUO.getSelectedItem() != null ) {
			typeUo = listTypeUO.getSelectedItem().getValue() ;
			bdTypeUO.setValue(typeUo.getLibelle());
			bdTypeUO.setAttribute(ID_TYPEUO, typeUo);
		}
		
		bdTypeUO.close();
		chargerBookmark2();
	}

	@Listen("onOpen = #bdUOParent")
	public void chargerBookmark2()
	{
		 
	 		List<UO> UO = uOService.listerUO();
			ListModelList<UO> modulesModel = new ListModelList<UO>(UO);
			listUOParent.setModel(modulesModel);

	}
	@Listen(" onSelect = #listUOParent ")
	public void onSelectModule2(){
		if(listUOParent.getSelectedItem() != null ) {
			UO mod = listUOParent.getSelectedItem().getValue() ;
			bdUOParent.setValue(mod.getNom());
			bdUOParent.setAttribute(ID_TYPEUO, mod);
		}
		
		bdUOParent.close();
		
	}	

	
	@Listen("onOK = window")
	public void onOK(Event event)
	{
		if(bdUOParent.getValue().isEmpty() || bdTypeUO.getValue().isEmpty()){
			alert("Attention, Champs manquants ! ");return;}
		
		String codeMod = ""+bdTypeUO.getAttribute(ID_TYPEUO);
		TypeUO typeUO = codeMod != null ? uOService.getTypeUO(codeMod) : null;
		uo.setTypeUO(typeUO);
		
		uo.setTypeUO((TypeUO)bdTypeUO.getAttribute(ID_TYPEUO));
		uo.setParent((UO)bdUOParent.getAttribute(ID_TYPEUO));
		
 		uo.setNom(txtDescription.getValue());
 		
 		try {
			uOService.saveUO(uo);
			Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getParent().getSpaceOwner()), null);
			getSelf().detach();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Listen("onAnnuler = window")
	public void onAnnuler(){
		getSelf().detach();
	}
}
