package sn.innov.web.securite.controllers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Wire;

import sn.innov.web.components.DualListbox;
import sn.innov.web.components.DualListboxLog;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.authentification.entities.Fonction;
import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Action;
import sn.innov.be.security.services.ActionService;
import sn.innov.be.security.services.LogActionService;
import sn.innov.be.security.services.UtilisateurService;

public class LogsUtilisateursController extends WindowController {
	
	Utilisateur  utilisateur = null;
	@Wire
	DualListboxLog dualListActions;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		HashMap<String, Object> params = (HashMap<String, Object>) Executions.getCurrent().getArg();		
		utilisateur = (Utilisateur) params.get("utilisateur");
		if(utilisateur != null)
		{
			LogActionService utilisateurService = (LogActionService) JNDIUtils.lookUpEJB(EJBRegistry.LogActionServiceBean);
			List<Utilisateur> utilisateursControles = utilisateurService.findUtilisateursControles(utilisateur.getId()) ;
			List<Utilisateur> utilisateursRestants = utilisateurService.findUtilisateursNonControles(utilisateur.getId()) ;
			dualListActions.setUtilisateur(utilisateur);
			dualListActions.setModel(new HashSet<Utilisateur>(utilisateursControles), new HashSet<Utilisateur>(utilisateursRestants));
		}
		
	}
	
	
}
