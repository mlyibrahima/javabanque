package sn.innov.web.securite.controllers;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.common.utils.SessionAttributes;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.authentification.entities.Fonction;
import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.core.utils.Utils;
import sn.innov.be.referentiel.services.ReferentielCommonService;
import sn.innov.be.security.services.LogActionService;
import sn.innov.be.uniteorganisationnelle.entities.UO;

public class FormUtilisateursController extends WindowController {
	
	Utilisateur  utilisateur = null;
	
	String action = "";
	@Wire 
	Textbox txtLogin ,txtPrenom, txtNom, txtPassword;
	@Wire
	Combobox cbbFonction,cbbDepartement, cbbUO;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		
		HashMap<String, Object> params = (HashMap<String, Object>) Executions.getCurrent().getArg();		
		utilisateur = (Utilisateur) params.get("utilisateur");
		action = (String) params.get("action");
		cbbFonction.setItemRenderer(new CbbFonctionRenderer());
		
		cbbUO.setItemRenderer(new CbbUORenderer());
		if(utilisateur != null){
			txtLogin.setValue(utilisateur.getLogin()) ;
			txtPrenom.setValue(utilisateur.getPrenom());
			txtNom.setValue(utilisateur.getNom());
			if(utilisateur.getFonction() != null){
				cbbFonction.setValue(utilisateur.getFonction().getCode());
			}
			/*if(utilisateur.getDepartement() != null){
				cbbDepartement.setValue(utilisateur.getDepartement().getLibelle());
			}*/
		}
	}
	
	
	@Listen("onOpen = #cbbFonction")
	public void chargerUniteAutoComplete() {
		
		ReferentielCommonService ref = (ReferentielCommonService) JNDIUtils.lookUpEJB(EJBRegistry.ReferentielCommonServiceBean);
		List<Fonction> lss = ref.lister(Fonction.class);
		cbbFonction.setModel(new ListModelList<Fonction>(lss));
	
	}
	

	
	@Listen("onOpen = #cbbUO")
	public void chargerUniteComplete1() {/*
		
		UtilisateurService ref = (UtilisateurService) JNDIUtils.lookUpEJB(EJBRegistry.UtilisateurServiceBean);
		List<UO> lsss = ref.listerUO();
		cbbUO.setModel(new ListModelList<UO>(lsss));
	
	*/}
	
	class CbbFonctionRenderer implements ComboitemRenderer<Fonction>{

		@Override
		public void render(Comboitem item, Fonction fonction, int index)
				throws Exception {
			item.setLabel(fonction.getCode());
			item.setValue(fonction);
		}
	}

	
	class CbbUORenderer implements ComboitemRenderer<UO>{

		@Override
		public void render(Comboitem item, UO uo, int index)
				throws Exception {
			item.setLabel(uo.getNom());
			item.setValue(uo);
		}
	}
	
	@Listen("onOK = window")
	public void onOK(Event event)
	{
		
		if(txtLogin.getValue().isEmpty() )
		{alert("Attention, champs manquants");return;}
		
		utilisateur.setLogin(txtLogin.getValue());
		utilisateur.setPrenom(txtPrenom.getValue()) ;
		utilisateur.setNom(txtNom.getValue());
		
		if(cbbFonction.getSelectedItem() != null){
			utilisateur.setFonction((Fonction) cbbFonction.getSelectedItem().getValue());
		}
		
		if(cbbUO.getSelectedItem() != null){
		//	utilisateur.setUo((UO) cbbUO.getSelectedItem().getValue());
		}
		
		if(txtPassword.getValue() != null && !txtPassword.getValue().isEmpty()){
			try {
				utilisateur.setPassword(Utils.getEncodedPassword(txtPassword.getValue()));
			} catch (WrongValueException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		
		try {
			ReferentielCommonService ref =(ReferentielCommonService) JNDIUtils.lookUpEJB(EJBRegistry.ReferentielCommonServiceBean);
			ref.save(utilisateur);
			LogActionService las = (LogActionService) JNDIUtils.lookUpEJB(EJBRegistry.LogActionServiceBean) ;
			Utilisateur connected = (Utilisateur) Sessions.getCurrent().getAttribute(SessionAttributes.CONNECTED_USER);
			las.log(action, connected) ;
			Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getParent().getSpaceOwner()), null);
			getSelf().detach();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@Listen("onAnnuler = window")
	public void onAnnuler(){
		getSelf().detach();
	}
}
