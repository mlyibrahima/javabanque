package sn.innov.web.securite.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Fonctionnalite;
import sn.innov.be.security.entities.Module;
import sn.innov.be.security.services.FonctionnaliteService;
import sn.innov.be.security.services.ModuleService;
import sn.innov.be.parametrage.entities.Projet;
import sn.innov.be.parametrage.services.ProjetService;

public class FormProjetController extends WindowController {


	private static final String CODE_MODULE = "codeModule";
	Projet  fonctionnalite = null;
	
	@Wire 
	Textbox txtCode,txtNom,txtDomaine, txtResponsable;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		HashMap<String, Object> params = (HashMap<String, Object>) Executions.getCurrent().getArg();		
		fonctionnalite = (Projet) params.get("projet");
		txtCode.setValue(fonctionnalite.getCode());
		txtNom.setValue(fonctionnalite.getNom());
		txtDomaine.setValue(fonctionnalite.getDomaine());
		txtResponsable.setValue(fonctionnalite.getResponsable());
		/*Module module = fonctionnalite.getModule();
		if(module != null) {
			bdModule.setValue(module.getDescription());
			bdModule.setAttribute(CODE_MODULE, module.getCode());
		}*/
	
	
	}
	
	
	
	
	@Listen("onOK = window")
	public void onOK(Event event)
	{
		if(txtCode.getValue().isEmpty() || txtDomaine.getValue().isEmpty() || txtNom.getValue().isEmpty() || txtResponsable.getValue().isEmpty())
		{alert("Attention, champs manquants");return;}
		
		ProjetService moduleService = (ProjetService) JNDIUtils.lookUpEJB(EJBRegistry.ProjetServiceBean);
		//String codeMod = (String) bdModule.getAttribute(CODE_MODULE);
		//Module module = codeMod != null ? moduleService.getModule(codeMod) : null;
		
		fonctionnalite.setCode(txtCode.getValue());
		//fonctionnalite.setModule(module);
		fonctionnalite.setNom(txtNom.getValue());
		fonctionnalite.setDomaine(txtDomaine.getValue());
		fonctionnalite.setResponsable(txtResponsable.getValue());
		fonctionnalite.setSupprime(false);
		ProjetService fonctionnaliteService = (ProjetService) JNDIUtils.lookUpEJB(EJBRegistry.ProjetServiceBean);
		
		try {
			fonctionnaliteService.modifierFonctionnalite(fonctionnalite);
			Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getParent().getSpaceOwner()), null);
			getSelf().detach();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@Listen("onAnnuler = window")
	public void onAnnuler(){
		getSelf().detach();
	}
}
