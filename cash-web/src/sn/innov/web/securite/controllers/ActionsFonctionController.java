package sn.innov.web.securite.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;




import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tabpanels;
import org.zkoss.zul.Tabs;
import org.zkoss.zul.Vbox;

import sn.innov.web.components.DualListbox;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.authentification.entities.Fonction;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Action;
import sn.innov.be.security.entities.Fonctionnalite;
import sn.innov.be.security.entities.Module;
import sn.innov.be.security.services.ActionService;
import sn.innov.be.security.services.DroitsAccesService;
import sn.innov.be.security.services.FonctionService;
import sn.innov.be.security.services.FonctionnaliteService;
import sn.innov.be.security.services.ModuleService;

public class ActionsFonctionController extends WindowController {
	
	Fonction  fonction = null;
	DroitsAccesService droitsServices = (DroitsAccesService) JNDIUtils.lookUpEJB(EJBRegistry.DroitsAccesServiceBean) ;
	@Wire
	Tabbox tabbox ;
	
	private List<Checkbox> selectedCheckboxs = new ArrayList<Checkbox>();
	
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		HashMap<String, Object> params = (HashMap<String, Object>) Executions.getCurrent().getArg();		
		fonction = (Fonction) params.get("fonction");
		
		 
		ModuleService modServices = (ModuleService) JNDIUtils.lookUpEJB(EJBRegistry.ModuleServiceBean) ;
		FonctionnaliteService fcntServices = (FonctionnaliteService) JNDIUtils.lookUpEJB(EJBRegistry.FonctionnaliteServiceBean) ;
		ActionService actionServices = (ActionService) JNDIUtils.lookUpEJB(EJBRegistry.ActionServiceBean) ;
		
		List<Module> modules = modServices.listerModules() ;
		
		Tabs tabs = new Tabs();
		tabs.setParent(tabbox) ;
		
		Tabpanels tabpanels = new Tabpanels();
		tabpanels.setWidth("100%") ;
		tabpanels.setParent(tabbox);
		
		// Listing des modules
		for( Module module : modules ){
			// config des tabs
			Tab tab = new Tab(module.getDescription()) ;
			tab.setParent(tabs) ;
			
			//config des tabpanels
			Tabpanel tabpanel = new Tabpanel() ;
			tabpanel.setWidth("100%");
			tabpanel.setHeight("380px") ;
			tabpanel.setStyle("overflow:auto") ;
			tabpanel.setParent(tabpanels);
			
			// config des tabbox dans les tabpanels
			Tabbox tabboxFonctionnalites = new Tabbox() ;
			
			tabboxFonctionnalites.setMold("accordion");
			tabboxFonctionnalites.setWidth("100%") ;
	
			tabboxFonctionnalites.setParent(tabpanel) ;
			
			Tabs tabsFonctionnalites = new Tabs();
			tabsFonctionnalites.setParent(tabboxFonctionnalites) ;
			
			Tabpanels tabpanelsFonctionnalites = new Tabpanels();
			tabpanelsFonctionnalites.setWidth("100%") ;
			tabpanelsFonctionnalites.setParent(tabboxFonctionnalites);
			
			
			List<Fonctionnalite> fonctionnalites  = fcntServices.listerFonctionnalites(module.getCode()) ;
			for( Fonctionnalite fonctionnalite : fonctionnalites ){
				Tab tabFonctionnalite = new Tab(fonctionnalite.getDescription()) ;
				
				tabFonctionnalite.setParent(tabsFonctionnalites) ;
				
				Tabpanel tabpanelFonctionnalite = new Tabpanel() ;
				tabpanelFonctionnalite.setParent(tabpanelsFonctionnalites);
				tabpanelFonctionnalite.setWidth("100%") ;
				tabpanelFonctionnalite.setStyle("overflow:auto") ;
				Vbox vbox = new Vbox() ;
				vbox.setParent(tabpanelFonctionnalite);
				List<Action> actions = actionServices.listerAction(fonctionnalite.getCode()) ;
				
				for(Action action : actions) {
					Checkbox checkbox = new Checkbox(action.getDescription()) ;
					checkbox.setParent(vbox) ;
					checkbox.setValue(action) ;
					checkbox.setChecked(droitsServices.hasAccess(fonction.getCode(), action.getCode()));
					if(checkbox.isChecked()) 
						selectedCheckboxs.add(checkbox);
					checkbox.addEventListener(Events.ON_CHECK, new EventListener<Event>() {

						@Override
						public void onEvent(Event e) throws Exception {
							Checkbox check =  (Checkbox) e.getTarget() ;
							if (check.isChecked()) 
								selectedCheckboxs.add(check);
							else selectedCheckboxs.remove(check);
						}
					}) ;
				}
				
				
			/*	Listbox listActions = new Listbox() ;
				listActions.setCheckmark(true);
				listActions.setMultiple(true) ;
			
				Listhead lHead = new Listhead() ;
				Listheader lHeader = new Listheader("") ;
				lHeader.setParent(lHead);
				lHead.setParent(listActions) ;
				
				listActions.setItemRenderer(new ListitemRenderer<Action>() {

					@Override
					public void render(Listitem item, Action action, int index)
							throws Exception {
						new Listcell(action.getDescription()).setParent(item) ;
						item.setValue(action) ;
						item.setSelected(droitsServices.hasAccess(fonction.getCode(), action.getCode())) ;
						
					}
				}) ;
				
				ListModelList<Action> listModelAction = new ListModelList<Action>(actionServices.listerAction(fonctionnalite.getCode()));
				listModelAction.setMultiple(true);
				listActions.setModel(listModelAction) ;
				
				listActions.setParent(tabpanelFonctionnalite);*/
				
				
			}
			
			
		}
		
	
	}
	
	@Listen(" onClick = #btnValider ")
	public void onClickBoutonValider(){
		Set<Action> chosen = new HashSet<Action>() ;
		for(Checkbox c:selectedCheckboxs) {
			chosen.add((Action) c.getValue());
		}
		if(fonction != null)
		{
			fonction.setActions(chosen);
			FonctionService fs = (FonctionService) JNDIUtils.lookUpEJB(EJBRegistry.FonctionServiceBean);
			try {
				fs.modifier(fonction);
				getSelf().detach();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
		

	
	@Listen(" onClick = #btnAnnuler ")
	public void onClickBoutonAnnuler(){
		getSelf().detach();
	}
	
	
	

	
	
	
	
	
}
