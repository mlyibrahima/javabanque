package sn.innov.web.securite.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.jms.Session;

import org.zkoss.image.AImage;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Fileupload;
import org.zkoss.zul.Image;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;

import sn.innov.web.common.utils.SessionAttributes;
import sn.innov.web.common.utils.UIConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.services.UtilisateurService;

public class ProfilController extends WindowController {
	
	@Wire
	Textbox txtPrenom, txtNom, txtLogin,txtProfil ;
	
	@Wire
	Vlayout pics ;
	
	Utilisateur currentUtilisateur = null;
	String imgPath = "", imgName = "" ;
	UtilisateurService us = (UtilisateurService) JNDIUtils.lookUpEJB(EJBRegistry.UtilisateurServiceBean) ;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		
		currentUtilisateur = (Utilisateur) Executions.getCurrent().getSession().getAttribute(SessionAttributes.CONNECTED_USER) ;
		currentUtilisateur =  us.find(currentUtilisateur) ;
		txtPrenom.setValue(currentUtilisateur.getPrenom()) ;
		txtNom.setValue(currentUtilisateur.getNom()) ;
		txtLogin.setValue(currentUtilisateur.getLogin()) ;
 		txtProfil.setValue(currentUtilisateur.getFonction().getDescription()) ;
		if(currentUtilisateur.getImageProfil() != null && !"".equals(currentUtilisateur.getImageProfil())){
			imgPath = UIConstants.IMAGES_PROFIL_PATH+currentUtilisateur.getImageProfil() ;
			imgName = currentUtilisateur.getImageProfil() ;
			Image img = new Image();
	        try {
				img.setContent( new AImage( "t" , new FileInputStream(imgPath)));
			} catch (IOException e1) {

				e1.printStackTrace();
			}
	        Components.removeAllChildren(pics);
			img.setParent(pics) ;
			img.setHeight(UIConstants.IMAGES_PROFIL_SIZE) ;
		}
	}
	
	@Listen(" onUpload = button ")
	public void onUploadProfilPic(UploadEvent e) {
		Media media = e.getMedia() ;
		
		imgPath = UIConstants.IMAGES_PROFIL_PATH+media.getName() ;
		imgName = media.getName() ;
		File f=new File(imgPath);
        InputStream inputStream= media.getStreamData();
        try {
			OutputStream out=new FileOutputStream(f);
			byte buf[]=new byte[1024];
			int len;
			while((len=inputStream.read(buf))>0)
			out.write(buf,0,len);
			out.close();
			inputStream.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        
        Image img = new Image(imgPath);
        try {
			img.setContent( new AImage( "t" , new FileInputStream(imgPath)));
		} catch (IOException e1) {

			e1.printStackTrace();
		}
        Components.removeAllChildren(pics);
		img.setParent(pics) ;
		img.setHeight(UIConstants.IMAGES_PROFIL_SIZE) ;
	    
	}
	
	@Listen(" onOK = window ")
	public void onOK() {
		currentUtilisateur.setPrenom(txtPrenom.getValue()) ;
		currentUtilisateur.setNom(txtNom.getValue()) ;
 		currentUtilisateur.setImageProfil(imgName) ;
		UtilisateurService us = (UtilisateurService) JNDIUtils.lookUpEJB(EJBRegistry.UtilisateurServiceBean) ;
		us.modifierUtilisateur(currentUtilisateur);
		Executions.getCurrent().getSession().setAttribute(SessionAttributes.CONNECTED_USER, currentUtilisateur) ;
		alert("Enregistrement effectu�");
		
	}
	
	@Listen(" onAnnuler = window ")
	public void onAnnuler() {
		
	}

}
