package sn.innov.web.securite.controllers;

import java.util.HashMap;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;

import sn.innov.web.components.DualListboxReadOnly;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.authentification.entities.Fonction;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Action;
import sn.innov.be.security.services.ActionService;

public class FicheFonctionController extends WindowController {
	
	Fonction  fonction = null;
	@Wire
	Label lblCode, lblDescription;
	@Wire
	Listbox listAction;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		HashMap<String, Object> params = (HashMap<String, Object>) Executions.getCurrent().getArg();		
		fonction = (Fonction) params.get("fonction");
		if(fonction != null)
		{
			lblCode.setValue(fonction.getCode());
			
			lblDescription.setValue(fonction.getDescription());
		
			ActionService actionService = (ActionService) JNDIUtils.lookUpEJB(EJBRegistry.ActionServiceBean);
			listAction.setModel( new ListModelList<Action>(actionService.listActionsForFonction(fonction.getCode()))  ) ;
			
			
		}
		
	}
	
}
