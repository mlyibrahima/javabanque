package sn.innov.web.securite.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Action;
import sn.innov.be.security.entities.Fonctionnalite;
import sn.innov.be.security.services.ActionService;
import sn.innov.be.security.services.FonctionnaliteService;

public class ListActionController extends WindowController {

	@Wire
	Listbox listActions;
	
	@Wire
	Bandbox bdFonctionnalite;
	@Wire
	Listbox listFonctionnalite;
	
	private static final String CODE_FONC = "codeFonc";

	
	/**
	 * Liste des actions de cet �cran
	 */
	static final String CREER_ACTION = "CREER_ACTION", 
						MODIFIER_ACTION = "MODIFIER_ACTION",
						SUPPRIMER_ACTION = "SUPPRIMER_ACTION",
						GENERER_CRUD_ACTION = "GENERER_CRUD_ACTION",
								CREER_VOIR_ACTION = "CREER_VOIR_ACTION";
	
	/**
	 * Ressources appel�es par cet ecran
	 */
	static final String	URI_FORM="/WEB-INF/action/form.zul";

	ActionService actionService = (ActionService) JNDIUtils.lookUpEJB(EJBRegistry.ActionServiceBean);
	FonctionnaliteService fonctionnaliteService = (FonctionnaliteService) JNDIUtils.lookUpEJB(EJBRegistry.FonctionnaliteServiceBean);

						

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
 		
	}
	
	@Listen("onOpen = #bdFonctionnalite")
	public void chargerFonctionnalite()
	{

		List<Fonctionnalite> fonctionnalites = fonctionnaliteService.listerFonctionnalites();
		ListModelList<Fonctionnalite> fonctionnaliteModel = new ListModelList<Fonctionnalite>(fonctionnalites);
		listFonctionnalite.setModel(fonctionnaliteModel);
	}
	
	@Listen(" onSelect = #listFonctionnalite ")
	public void onChargerFonctionnalites(){
		if(listFonctionnalite.getSelectedItem() != null) {
			Fonctionnalite fonctionnalite= listFonctionnalite.getSelectedItem().getValue() ;
			try {
				List<Action> actions = actionService.listerAction(fonctionnalite.getCode());
				ListModelList<Action> modelAction = new ListModelList<Action>(actions);
				listActions.setModel(modelAction);
	 			
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			bdFonctionnalite.close();

		}
		
	}
	
	 
	
	/**
	 * Ecouteur de l'evenement onListChange pour recharger la liste des fonctions
	 */
	@Listen(EventConstants.ON_LIST_CHANGE+" = window")
	public void afficherListeActions()
	{
 		List<Action> actions = actionService.listerAction();
		ListModelList<Action> modelAction = new ListModelList<Action>(actions);
		listActions.setModel(modelAction);
	}
	
	/**
	 * Switch des �venements lanc�s par un action button
	 * @param event
	 */
	@Listen(ON_CLICK_ACTION_BUTTON+" = window")
	public void onClickActionButton(Event event)
	{
		String action = (String) event.getData();

		if(action.equalsIgnoreCase(CREER_ACTION)){
			HashMap<String, Object> params = new HashMap<String, Object>();
			
			params.put("action", new Action());
			loadPopUp(URI_FORM, params );
			
		}else if(action.equalsIgnoreCase(MODIFIER_ACTION)){
			if(listActions.getSelectedItem() != null){
				HashMap<String, Object> params = new HashMap<String, Object>();
				
				params.put("action", (Action)listActions.getSelectedItem().getValue());
				loadPopUp(URI_FORM, params );
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
			
		}else if(action.equalsIgnoreCase(SUPPRIMER_ACTION)){
			if(listActions.getSelectedItem() != null){
				
				confirm("Etes vous sur de bien vouloire supprimer l element?", "Suppression action",
					new EventListener<Messagebox.ClickEvent>() {
					
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if(event.getButton() == Button.YES){
							Events.postEvent(EventConstants.ON_DELETE,((Window)getSelf().getSpaceOwner()), null);	
						}else if(event.getButton() == Button.NO) {
		
						}
					}	
				});
				
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
			
		}else if(action.equalsIgnoreCase(GENERER_CRUD_ACTION)){
			if(listFonctionnalite.getSelectedItem() != null){
				
				confirm("Etes vous sur de bien vouloire generer le crud ?", "Generer les actions",
					new EventListener<Messagebox.ClickEvent>() {
					
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if(event.getButton() == Button.YES){
							
							actionService.genereCrud((Fonctionnalite)listFonctionnalite.getSelectedItem().getValue());
							
							onChargerFonctionnalites();
						
						}else if(event.getButton() == Button.NO) {
		
						}
					}	
				});
				
			}
		}else if(action.equalsIgnoreCase(CREER_VOIR_ACTION)){
			if(listFonctionnalite.getSelectedItem() != null){
				
				confirm("Etes vous sur de bien vouloire creer l'action ?", "Generer les actions",
					new EventListener<Messagebox.ClickEvent>() {
					
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if(event.getButton() == Button.YES){
							
							actionService.creervoiraction((Fonctionnalite)listFonctionnalite.getSelectedItem().getValue());
							
							onChargerFonctionnalites();
						
						}else if(event.getButton() == Button.NO) {
		
						}
					}	
				});
				
			}
		}
	}
	
	/**
	 * Ecouteur de l'evenment: suppression confirm�e
	 */
	@Listen(EventConstants.ON_DELETE+" = window")
	public void supprimerActions()
	{
 		Action action = listActions.getSelectedItem().getValue();
		if(actionService.supprimerAction(action) == null) 
		{alert("Suppression impossible");return;}
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}}
