package sn.innov.web.securite.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.uniteorganisationnelle.entities.TypeUO;
import sn.innov.be.uniteorganisationnelle.services.UOService;

public class FormTypeUOController extends WindowController {


	private static final String ID_TYPEUO = "id";
	TypeUO typeUO = null;
	@Wire
	Bandbox bdTypeUOparent;
	@Wire
	Listbox listModule;
	@Wire 
	Textbox txtDescription,txtCode;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		HashMap<String, Object> params = (HashMap<String, Object>) Executions.getCurrent().getArg();		
		typeUO = (TypeUO) params.get("typeuo");
		txtDescription.setValue(typeUO.getLibelle());	
		txtCode.setValue(typeUO.getCode());	
		
		TypeUO parent = typeUO.getParent();
		if(parent != null) {
			bdTypeUOparent.setValue(parent.getLibelle());
			bdTypeUOparent.setAttribute(ID_TYPEUO, parent.getCode());
		}
	}
	
	
	@Listen("onOpen = #bdTypeUOparent")
	public void chargerBookmark()
	{
		UOService moduleService = (UOService) JNDIUtils.lookUpEJB(EJBRegistry.UOServiceBean);
		List<TypeUO> typeUO = moduleService.listerTypeUO();
		ListModelList<TypeUO> modulesModel = new ListModelList<TypeUO>(typeUO);
		listModule.setModel(modulesModel);
	}
	
	@Listen(" onSelect = #listModule ")
	public void onSelectModule(){
		if(listModule.getSelectedItem() != null ) {
			TypeUO mod = listModule.getSelectedItem().getValue() ;
			bdTypeUOparent.setValue(mod.getLibelle());
			bdTypeUOparent.setAttribute(ID_TYPEUO, mod.getCode());
		}
		
		bdTypeUOparent.close();
		
	}
	
	@Listen("onOK = window")
	public void onOK(Event event)
	{
		if(bdTypeUOparent.getValue().isEmpty()){
			alert("Attention, type UO parente manquante ! ");return;}
		UOService moduleService = (UOService) JNDIUtils.lookUpEJB(EJBRegistry.UOServiceBean);
		String codeMod = ""+bdTypeUOparent.getAttribute(ID_TYPEUO);
		TypeUO parent = codeMod != null ? moduleService.getTypeUO(codeMod) : null;

		typeUO.setParent(parent);
		typeUO.setLibelle(txtDescription.getValue());
		typeUO.setCode(txtCode.getValue());
		UOService uOService = (UOService) JNDIUtils.lookUpEJB(EJBRegistry.UOServiceBean);
		
		try {
			uOService.saveTUO(typeUO);
			Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getParent().getSpaceOwner()), null);
			getSelf().detach();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Listen("onAnnuler = window")
	public void onAnnuler(){
		getSelf().detach();
	}
}
