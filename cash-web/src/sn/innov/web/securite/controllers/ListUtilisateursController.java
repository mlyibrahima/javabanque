package sn.innov.web.securite.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.common.utils.SessionAttributes;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.services.LogActionService;
import sn.innov.be.security.services.UtilisateurService;

public class ListUtilisateursController extends WindowController {


	@Wire
	Listbox listUtilisateurs;
	
	/**
	 * Liste des actions de cet �cran
	 */
	static final String CREER_UTILISATEUR = "CREER_UTILISATEUR", 
						MODIFIER_UTILISATEUR = "MODIFIER_UTILISATEUR",
						SUPPRIMER_UTILISATEUR = "SUPPRIMER_UTILISATEUR",
						VOIR_UTILISATEUR = "VOIR_UTILISATEUR",
						CONFIGURER_LOG_UTILISATEUR = "CONFIGURER_LOG_UTILISATEUR",
						GERER_DEPARTEMENTS="GERER_DEPARTEMENTS";
	/**
	 * Ressources appel�es par cet ecran
	 */
	static final String	URI_DEP="/WEB-INF/utilisateurs/departements.zul";
	static final String	URI_FORM="/WEB-INF/utilisateurs/form.zul";
	static final String	URI_FICHE="/WEB-INF/utilisateurs/fiche.zul";
	static final String	URI_LOGS="/WEB-INF/utilisateurs/logs.zul";
	
	String action = "" ;
						

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}
	
	/**
	 * Ecouteur de l'evenement onListChange pour recharger la liste des fonctions
	 */
	@Listen(EventConstants.ON_LIST_CHANGE+" = window")
	public void afficherListeActions()
	{
		UtilisateurService utilisateurService = (UtilisateurService) JNDIUtils.lookUpEJB(EJBRegistry.UtilisateurServiceBean);
		List<Utilisateur> utilisateurs = utilisateurService.listerUtilisateur();
		ListModelList<Utilisateur> modelAction = new ListModelList<Utilisateur>(utilisateurs);
		listUtilisateurs.setModel(modelAction);
	}
	
	/**
	 * Switch des �venements lanc�s par un action button
	 * @param event
	 */
	@Listen(ON_CLICK_ACTION_BUTTON+" = window")
	public void onClickActionButton(Event event)
	{
		action = (String) event.getData();

		if(action.equalsIgnoreCase(CREER_UTILISATEUR)){
			HashMap<String, Object> params = new HashMap<String, Object>();
			
			params.put("utilisateur", new Utilisateur());
			params.put("action", CREER_UTILISATEUR);
			loadPopUp(URI_FORM, params );
			
		}else if(action.equalsIgnoreCase(MODIFIER_UTILISATEUR)){
			if(listUtilisateurs.getSelectedItem() != null){
				HashMap<String, Object> params = new HashMap<String, Object>();
				
				params.put("utilisateur", (Utilisateur)listUtilisateurs.getSelectedItem().getValue());
				params.put("action", MODIFIER_UTILISATEUR);
				loadPopUp(URI_FORM, params );
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
			
		}else if(action.equalsIgnoreCase(SUPPRIMER_UTILISATEUR)){
			if(listUtilisateurs.getSelectedItem() != null){
				
				confirm("Etes vous sur de bien vouloire supprimer l element?", "Suppression action",
					new EventListener<Messagebox.ClickEvent>() {
					
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if(event.getButton() == Button.YES){
							Events.postEvent(EventConstants.ON_DELETE,((Window)getSelf().getSpaceOwner()), null);	
						}else if(event.getButton() == Button.NO) {
		
						}
					}	
				});
				
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
		}else if(action.equalsIgnoreCase(VOIR_UTILISATEUR)){
			if(listUtilisateurs.getSelectedItem() != null){
				HashMap<String, Object> params = new HashMap<String, Object>();
				
				params.put("utilisateur", (Utilisateur)listUtilisateurs.getSelectedItem().getValue());
				loadPopUp(URI_FICHE, params );
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
			
		}else if(action.equalsIgnoreCase(CONFIGURER_LOG_UTILISATEUR)){
			if(listUtilisateurs.getSelectedItem() != null){
				HashMap<String, Object> params = new HashMap<String, Object>();
				
				params.put("utilisateur", (Utilisateur)listUtilisateurs.getSelectedItem().getValue());
				loadPopUp(URI_LOGS, params );
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
			
		}else if(action.equalsIgnoreCase(GERER_DEPARTEMENTS)){
  			loadEmbedded(URI_DEP, getSelf(), null);
			
		}
			
		
	}
	
	/**
	 * Ecouteur de l'evenment: suppression confirm�e
	 */
	@Listen(EventConstants.ON_DELETE+" = window")
	public void supprimerActions()
	{
		UtilisateurService actionService = (UtilisateurService) JNDIUtils.lookUpEJB(EJBRegistry.UtilisateurServiceBean);
		Utilisateur utilisateur = listUtilisateurs.getSelectedItem().getValue();
		utilisateur.setSupprime(true) ;
		utilisateur = actionService.modifierUtilisateur(utilisateur);
		if(utilisateur == null) 
		{alert("Suppression impossible");return;}
		LogActionService las = (LogActionService) JNDIUtils.lookUpEJB(EJBRegistry.LogActionServiceBean) ;
		Utilisateur connected = (Utilisateur) Sessions.getCurrent().getAttribute(SessionAttributes.CONNECTED_USER);
		las.log(action, connected) ;
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}

}
