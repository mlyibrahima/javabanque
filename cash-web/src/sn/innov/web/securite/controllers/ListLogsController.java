package sn.innov.web.securite.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.common.utils.SessionAttributes;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.authentification.entities.Fonction;
import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.referentiel.services.ReferentielCommonService;
import sn.innov.be.security.entities.LogAction;
import sn.innov.be.security.services.LogActionService;
import sn.innov.be.security.services.UtilisateurService;

public class ListLogsController extends WindowController {


	@Wire
	Listbox listLogs;
	@Wire("paging")
	Paging paging;
	
	
						

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		afficherListeLogs();
		
		
		
	}
	
	/**
	 * Ecouteur de l'evenement onListChange pour recharger la liste des fonctions
	 */
	@Listen(EventConstants.ON_LIST_CHANGE+" = window; onOK = window ")
	public void afficherListeLogs()
	{
		LogActionService las = (LogActionService) JNDIUtils.lookUpEJB(EJBRegistry.LogActionServiceBean) ;
		Utilisateur user = (Utilisateur) Executions.getCurrent().getSession().getAttribute(SessionAttributes.CONNECTED_USER) ;
		List<LogAction> logs =  las.findLogAction(user.getId(), paging.getActivePage() * paging.getPageSize(),
				paging.getPageSize()) ;
		
		ListModelList<LogAction> modelLogs = new ListModelList<LogAction>(
				logs);
		listLogs.setModel(modelLogs);
		
		paging.setTotalSize((int) las.compterLogAction(user.getId()));

	}
	


}
