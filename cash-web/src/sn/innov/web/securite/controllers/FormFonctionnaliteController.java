package sn.innov.web.securite.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Fonctionnalite;
import sn.innov.be.security.entities.Module;
import sn.innov.be.security.services.FonctionnaliteService;
import sn.innov.be.security.services.ModuleService;

public class FormFonctionnaliteController extends WindowController {


	private static final String CODE_MODULE = "codeModule";
	Fonctionnalite  fonctionnalite = null;
	@Wire
	Bandbox bdModule;
	@Wire
	Listbox listModule;
	@Wire 
	Textbox txtCode,txtDescription,txtBookmark, txtImage;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		HashMap<String, Object> params = (HashMap<String, Object>) Executions.getCurrent().getArg();		
		fonctionnalite = (Fonctionnalite) params.get("fonctionnalite");
		txtCode.setValue(fonctionnalite.getCode());
		txtDescription.setValue(fonctionnalite.getDescription());
		txtBookmark.setValue(fonctionnalite.getBookmark());
		txtImage.setValue(fonctionnalite.getImage());
		Module module = fonctionnalite.getModule();
		if(module != null) {
			bdModule.setValue(module.getDescription());
			bdModule.setAttribute(CODE_MODULE, module.getCode());
		}
	
	
	}
	
	
	@Listen("onOpen = #bdModule")
	public void chargerBookmark()
	{
		ModuleService moduleService = (ModuleService) JNDIUtils.lookUpEJB(EJBRegistry.ModuleServiceBean);
		List<Module> modules = moduleService.listerModules();
		ListModelList<Module> modulesModel = new ListModelList<Module>(modules);
		listModule.setModel(modulesModel);
	}
	
	@Listen(" onSelect = #listModule ")
	public void onSelectModule(){
		if(listModule.getSelectedItem() != null ) {
			Module mod = listModule.getSelectedItem().getValue() ;
			bdModule.setValue(mod.getDescription());
			bdModule.setAttribute(CODE_MODULE, mod.getCode());
		}
		
		bdModule.close();
		
	}
	
	@Listen("onOK = window")
	public void onOK(Event event)
	{
		if(txtCode.getValue().isEmpty() || bdModule.getValue().isEmpty() || txtBookmark.getValue().isEmpty() )
		{alert("Attention, champs manquants");return;}
		
		ModuleService moduleService = (ModuleService) JNDIUtils.lookUpEJB(EJBRegistry.ModuleServiceBean);
		String codeMod = (String) bdModule.getAttribute(CODE_MODULE);
		Module module = codeMod != null ? moduleService.getModule(codeMod) : null;
		
		fonctionnalite.setCode(txtCode.getValue());
		fonctionnalite.setModule(module);
		fonctionnalite.setDescription(txtDescription.getValue());
		fonctionnalite.setBookmark(txtBookmark.getValue());
		fonctionnalite.setImage(txtImage.getValue());
		FonctionnaliteService fonctionnaliteService = (FonctionnaliteService) JNDIUtils.lookUpEJB(EJBRegistry.FonctionnaliteServiceBean);
		
		try {
			fonctionnaliteService.modifierFonctionnalite(fonctionnalite);
			Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getParent().getSpaceOwner()), null);
			getSelf().detach();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@Listen("onAnnuler = window")
	public void onAnnuler(){
		getSelf().detach();
	}
}
