package sn.innov.web.main.controllers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.BookmarkEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Toolbarbutton;

import sn.innov.web.common.utils.ContextAttributes;
import sn.innov.web.common.utils.SessionAttributes;
import sn.innov.web.core.bookmarks.State;
import sn.innov.web.core.bookmarks.States;
import sn.innov.web.core.bookmarks.Zone;
import sn.innov.web.core.bookmarks.ZoneContext;
import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Module;
import sn.innov.be.security.services.DroitsAccesService;

public class ApplicationWindow extends WindowController {

	@Wire
	Toolbarbutton tbNom, tbSur, tdDeconnexion;
	
	@Wire
	Div center ;

	@Wire
	Label lblTitrePage, lblModule;
	
	
	DroitsAccesService daService = (DroitsAccesService) JNDIUtils.lookUpEJB(EJBRegistry.DroitsAccesServiceBean);

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		System.out.println(">>>>>>>>>>>>>R�cup�ration des param�tres HTTP");
		HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
		HashMap<String, String> requestParamValues = new HashMap<String, String>();
		Enumeration<String> requestParams = request.getParameterNames();
		while (requestParams.hasMoreElements()) {
			String paramName = (String) requestParams.nextElement();
			requestParamValues.put(paramName, request.getParameter(paramName));
		}
		getSelf().getDesktop().setAttribute(
				SessionAttributes.HTTP_PARAM_VALUES, requestParamValues);
		
		comp.addEventListener("onLoadBookmark", new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				String bookmark = (String) event.getData();
				System.out.println("Loading Bookmark " + bookmark);
				loadBookmark(bookmark);
			}
		});
		
		HttpSession session = ((HttpServletRequest)request).getSession();
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(SessionAttributes.CONNECTED_USER);
		
		
		String nom = "" ;
		if (utilisateur.getPrenom() != null) nom += utilisateur.getPrenom() ;
		if (utilisateur.getNom()!= null) nom += " "+utilisateur.getNom() ;
	//	lblStatusBar.setValue("Utilisateur connect�: "+nom);
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter=   new SimpleDateFormat("dd/MM/yyyy  HH:mm:ss");
		String dateNow = formatter.format(currentDate.getTime());
//		lblTime.setValue(" depuis le "+dateNow);
	}		


	@Listen("onBookmarkChange = #applicationWindow")
	public void onBookmarkChange(BookmarkEvent bookmarkEvent) {
		String bookmark = bookmarkEvent.getBookmark();
		loadBookmark(bookmark);
	}

	public void loadZone(String zone, String uri) {
		
		Component comp = getSelf();
		String q = "div #"+zone ;
		Component component =  center;
		if (component.getFirstChild() != null)
			component.getFirstChild().detach();
		Executions.createComponents(uri, component, null);
	}

	public void loadBookmark(String bookMark) {
		ServletContext context = (ServletContext) Executions.getCurrent()
				.getDesktop().getWebApp().getNativeContext();
		States states = (States) context.getAttribute(ContextAttributes.STATES);
		State state = states.getState(bookMark);
		Module m = daService.findModuleByBookmark(state.getBookmark());
		if(m != null){
			lblModule.setValue(m.getDescription()+" / ");
		}
		lblTitrePage.setValue(state.getTitle());
		List<Zone> zones = state.getZones();
		for (Zone zone : zones) {
			loadZone(zone.getCode(), zone.getUri());
		}
	}

	public void loadZone(String zone, String uri, ZoneContext zoneContext) {
		Component component = getSelf().getFellow(zone);
		if (component.getFirstChild() != null)
			component.getFirstChild().detach();
		Executions.createComponents(uri, component, zoneContext.getParams());
	}

	@Listen("onLogout = #applicationWindow")
	public void logout() {
		try {
			 confirm("Etes vous sur de bien vouloir vous deconnecter ?", "D�connexion",
						new EventListener<Messagebox.ClickEvent>() {

							@Override
							public void onEvent(ClickEvent event) throws Exception {
								if (event.getButton() == Button.YES) {
									//((HttpSession) Sessions.getCurrent().getNativeSession()).invalidate();
									Sessions.getCurrent().removeAttribute(SessionAttributes.CONNECTED_USER);
									//Sessions.getCurrent().invalidate();
									Executions.sendRedirect("/authentication/authentication.zul");
								}
							}
						});
		} catch (Exception e) {e.printStackTrace();}
	}
	
	
	
}
