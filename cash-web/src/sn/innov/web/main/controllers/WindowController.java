package sn.innov.web.main.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.Window.Mode;
import org.zkoss.zul.impl.InputElement;

import sn.innov.be.authentification.entities.SessionUtilisateur;
import sn.innov.web.common.utils.SessionAttributes;
import sn.innov.web.core.bookmarks.BookmarkContext;

public class WindowController extends SelectorComposer<Component> {	
	
	public static final String ON_CLICK_ACTION_BUTTON = "onClickActionButton";
	
	public SessionUtilisateur getCurrentSessionUtilisateur()
	{
		return (SessionUtilisateur) Sessions.getCurrent().getAttribute(SessionAttributes.CURRENT_SESSION_UTILISATEUR);
	}
	
	public String getHttpParamValue(String param)
	{
		return ((HashMap<String, String>)getSelf().getDesktop().getAttribute(SessionAttributes.HTTP_PARAM_VALUES)).get(param);
	}

	protected void check(Component component) {
		checkIsValid(component);

		List<Component> children = component.getChildren();
		for (Component each : children) {
			check(each);
		}
	}

	private void checkIsValid(Component component) {
		if (component instanceof InputElement) {
			if (!((InputElement) component).isValid()) {
				// Force show errorMessage
				((InputElement) component).getText();
			}
		}
	}
	
	/*
	 * Charge en popup
	 */
	public void loadPopUp(String uri, HashMap<String, Object> params) {
		Window window = (Window) Executions.createComponents(uri,
				getSelf(), params);		
		window.setMode(Mode.HIGHLIGHTED);
		
	}

	public void loadEmbedded(String uri, Component container, HashMap<String, Object> params) {
		if(container.getFirstChild()!=null)
			container.getFirstChild().detach();
		Window window = (Window) Executions.createComponents(uri, getSelf(), params);	
		window.setMode(Mode.EMBEDDED);
	}
	
	/*
	 * Charge un bookmark avec des param�tres Http Get
	 * Les valeurs de ces param�tres seront enregistr�s dans le desktop et accessible apr�s �
	 *  travers la m�thode getHttpParamValue.
	 */
	public void loadBookmark(BookmarkContext bookmarkContext)
	{
		HashMap<String, String> params = bookmarkContext.getParams();
		String uri = "/templates/main/index.zul?";
		Set<Entry<String, String>> entries = params.entrySet();
		for (Entry<String, String> entry : entries) {
			uri+=entry.getKey()+"="+entry.getValue().toString();
		}
		Executions.sendRedirect(uri+"#"+bookmarkContext.getBookMark());
	}
	
	public void loadBookmark(String bookmark)
	{
		Component parent = getSelf().getRoot();
		Events.postEvent("onLoadBookmark",(Component) parent,bookmark);
		getSelf().getRoot().getPage().getDesktop().setBookmark(bookmark,false);
	}
	
	public void loadBookmark(String bookmark, Map<String, Object> data)
	{
		Events.postEvent("onLoadBookmark",(Component) getSelf().getParent().getSpaceOwner(),data);
		getSelf().getRoot().getPage().getDesktop().setBookmark(bookmark,false);
	}
	
	public void reload()
	{
		String uri = "/index.zul?";
		Executions.sendRedirect(uri+"#"+getSelf().getDesktop().getBookmark());
	}
	
	public void confirm(String message,String titre,EventListener<Messagebox.ClickEvent> eventListener)
	{
			Messagebox.show(message, titre, new Messagebox.Button[]{
		        Messagebox.Button.YES, Messagebox.Button.NO}, Messagebox.QUESTION, eventListener);
	}
	
	public void confirm(String message,EventListener<Messagebox.ClickEvent> eventListener)
	{
			Messagebox.show(message, "Confirm", new Messagebox.Button[]{
		        Messagebox.Button.YES, Messagebox.Button.NO}, Messagebox.QUESTION, eventListener);
	}
	
	public void alert(String message)
	{
		Messagebox.show(message);
	}	
	
	/*public void handleTransactionException(TransactionException transactionException)
	{
		if(transactionException.getMessage()==null)
		{
			alert(transactionException.getError().name());
		}
		else
		{
			alert(transactionException.getMessage());
		}
	}*/

}
