package sn.innov.web.main.controllers;

import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zul.Image;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tabpanels;
import org.zkoss.zul.Tabs;
import org.zkoss.zul.Toolbarbutton;
import org.zkoss.zul.Vbox;

import sn.innov.web.common.utils.SessionAttributes;
import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Fonctionnalite;
import sn.innov.be.security.entities.Module;
import sn.innov.be.security.services.DroitsAccesService;

public class MenuController extends SelectorComposer<Tabbox> {

	Toolbarbutton currentToolbarbutton = null ;

	@Override
	public void doAfterCompose(Tabbox comp) throws Exception {
		Utilisateur current = (Utilisateur)Sessions.getCurrent().getAttribute(SessionAttributes.CONNECTED_USER);
		String fonctionCode = current.getFonction().getCode();
		DroitsAccesService  droitsService = (DroitsAccesService) JNDIUtils.lookUpEJB(EJBRegistry.DroitsAccesServiceBean);
		List<Module> modules = droitsService.findFonctionModules(fonctionCode);
		Tabs tabs = new Tabs();
		tabs.setParent(getSelf());
		
		Tabpanels tabpanels = new Tabpanels();
		tabpanels.setParent(getSelf());
		
		for (Module module : modules) {
			if(module == null) continue;
			
			String imgMod = module.getImage() != null && !module.getImage().isEmpty() ? module.getImage() : "" ;
			
			Tab tab = new Tab(Labels.getLabel(module.getDescription(),module.getDescription()));
			tab.setWidth("100%");
			tab.setParent(tabs);
			if(!imgMod.isEmpty())
				tab.setImage("/images/"+imgMod);
			
			
			Tabpanel tabpanel = new Tabpanel();
			tabpanel.setWidth("100%");
			tabpanel.setParent(tabpanels);
			
			
			Label lbl = new Label() ;
			
			lbl.setParent(tabpanel);
			
			List<Fonctionnalite> fonctionnalites = droitsService.findFonctionFonctionnalites(fonctionCode, module.getCode());
			
			Vbox vbox = new Vbox();
			vbox.setClass("z-menubar-ver");
			for (Fonctionnalite fonctionnalite : fonctionnalites) {
				String codeFonct = fonctionnalite.getCode() ;
				
				String imgFonct = fonctionnalite.getImage() != null && !fonctionnalite.getImage().isEmpty() ? fonctionnalite.getImage() : "chevron-right.png" ;
								
				Toolbarbutton toolbarbutton = new Toolbarbutton(Labels.getLabel(fonctionnalite.getDescription(),fonctionnalite.getDescription()));
				if(!imgFonct.isEmpty())
					toolbarbutton.setImage("/images/"+imgFonct);
					
				if(currentToolbarbutton == null ) currentToolbarbutton = toolbarbutton ;
				
				
				toolbarbutton.setAttribute("bookmark", fonctionnalite.getBookmark());
				toolbarbutton.setParent(vbox);
				
				vbox.setParent(tabpanel);		
				toolbarbutton.setMode("toggle");
					
				toolbarbutton.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

						@Override
						public void onEvent(Event event) throws Exception {
							currentToolbarbutton.setChecked(false);
							String bookMark = (String) event.getTarget().getAttribute("bookmark");
							
							currentToolbarbutton = (Toolbarbutton) event.getTarget() ;
							currentToolbarbutton.setChecked(true);
							Events.postEvent("onLoadBookmark",(((Include) getSelf().getSpaceOwner()).getParent().getParent()),bookMark);
							getSelf().getRoot().getPage().getDesktop().setBookmark(bookMark,false);
							
							
						}
					});
					
				
			}
			
		
		}
	}

}
