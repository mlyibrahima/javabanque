package sn.innov.web.authentification.controllers;

import java.security.NoSuchAlgorithmException;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Textbox;

import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.authentification.services.AuthentificationService;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.core.utils.Utils;
import sn.innov.web.common.utils.SessionAttributes;
import sn.innov.web.main.controllers.WindowController;

public class ChangementPasswordController extends WindowController {
	@Wire
	Textbox txtAncien, txtNouveau, txtNouveau2, txtLogin, txtPrenom, txtNom;
	
	Utilisateur u = null;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		u = (Utilisateur) Sessions.getCurrent().getAttribute(SessionAttributes.CONNECTED_USER) ;
		txtLogin.setValue(u.getLogin());
		if(u.getFonction().getCode().equalsIgnoreCase("SUPERVISEUR")) txtLogin.setDisabled(false) ;
		txtPrenom.setValue(u.getPrenom());
		txtNom.setValue(u.getNom());
	}


	
	
	@Listen(" onOK = window")
	public void onOK() throws WrongValueException, NoSuchAlgorithmException{
		
		u.setNom(txtNom.getValue());
		u.setPrenom(txtPrenom.getValue());
		u.setLogin(u.getLogin());
		if(!txtNouveau.getValue().equals(txtNouveau2.getValue())){
			alert("Attention ! les nouveaux mots de passe renseignés ne correspondent pas");
			return ;
		}
		if(!u.getPassword().equalsIgnoreCase(Utils.getEncodedPassword(txtAncien.getValue())) ){
			alert("Attention ! ancien mot de passe incorrect ");
			return ;
		}
		if(!txtAncien.getValue().isEmpty() && !txtNouveau.getValue().isEmpty() ){
			u.setPassword(Utils.getEncodedPassword(txtNouveau.getValue())) ;
		}
		AuthentificationService serv = (AuthentificationService) JNDIUtils.lookUpEJB(EJBRegistry.AuthentificationServiceBean) ;
		serv.save(u);
		
		getSelf().detach();
	}
	
	@Listen(" onAnnuler = window ")
	public void onAnnuler(){
		getSelf().detach();
	}

}
