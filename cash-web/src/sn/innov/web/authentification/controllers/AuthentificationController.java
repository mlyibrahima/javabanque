package sn.innov.web.authentification.controllers;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.be.authentification.entities.SessionUtilisateur;
import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.authentification.services.AuthentificationService;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.web.common.utils.SessionAttributes;

public class AuthentificationController extends SelectorComposer<Window> {

	@Wire
	Textbox txtLogin,txtPassword;
	
	@Override
	public void doAfterCompose(Window comp) throws Exception {
		super.doAfterCompose(comp);
		
		//onOK();
		
	}
	
	@Listen("onOK = window")
	public void onOK()
	{
		AuthentificationService authentificationService = 
				(AuthentificationService) JNDIUtils.lookUpEJB(EJBRegistry.AuthentificationServiceBean);
	
		Utilisateur utilisateur = authentificationService.findUtilisateurByLoginPassword(txtLogin.getValue(), txtPassword.getValue());
		
		SessionUtilisateur sessionUtilisateur=authentificationService.login(txtLogin.getValue(), txtPassword.getValue());
		
		if(sessionUtilisateur!=null)
		{
			//authentificationService.save(utilisateur);
			Sessions.getCurrent().setAttribute(SessionAttributes.CONNECTED_USER, sessionUtilisateur.getUtilisateur());
			Sessions.getCurrent().setAttribute(SessionAttributes.CURRENT_SESSION_UTILISATEUR, sessionUtilisateur);
			String fonctionCode = utilisateur.getFonction().getCode();			

			Sessions.getCurrent().setAttribute("fonctionCode", fonctionCode);
			Executions.sendRedirect("/index.zul#"+utilisateur.getFonction().getWelcomeBookmark());
		}else
			alert("Attention, login ou mot de passe incorrect !");
	}
}
