package sn.innov.web.components;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Div;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;

import sn.innov.be.authentification.entities.Fonction;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Action;
import sn.innov.be.security.services.FonctionService;

 
public class DualListbox extends Div implements IdSpace {
 

	/**
     *
     */
    private static final long serialVersionUID = 5183321186606483396L;
     
    @Wire
    private Listbox candidateLb;
    @Wire
    private Listbox chosenLb;
    
    Fonction fonction = null;
 
    private ListModelList<Action> candidateModel;
    private ListModelList<Action> chosenDataModel;
    
    public ListModelList<Action> getCandidateModel() {
 		return candidateModel;
 	}

 	public void setCandidateModel(ListModelList<Action> candidateModel) {
 		this.candidateModel = candidateModel;
        candidateLb.setModel(this.candidateModel ); 
 	}

 	public ListModelList<Action> getChosenDataModel() {
 		return chosenDataModel;
 	}

 	public void setChosenDataModel(ListModelList<Action> chosenDataModel) {
 		this.chosenDataModel = chosenDataModel;
 		chosenLb.setModel(chosenDataModel);
 	}
 
    public DualListbox() {
        Executions.createComponents("/WEB-INF/securite/referentiel/dualListbox.zul", this, null);
        Selectors.wireComponents(this, this, false);
        Selectors.wireEventListeners(this, this);
        chosenLb.setModel(chosenDataModel = new ListModelList<Action>());
    }
 
    @Listen("onClick = #chooseBtn")
    public void chooseItem() {
        Events.postEvent(new ChooseEvent(this, chooseOne()));
    }
 
    @Listen("onClick = #removeBtn")
    public void unchooseItem() {
        Events.postEvent(new ChooseEvent(this, unchooseOne()));
    }
 
    @Listen("onClick = #chooseAllBtn")
    public void chooseAllItem() {
        for (int i = 0, j = candidateModel.getSize(); i < j; i++) {
            chosenDataModel.add(candidateModel.getElementAt(i));
        }
        candidateModel.clear();
    }
 
    @Listen("onClick = #removeAllBtn")
    public void unchooseAll() {
        for (int i = 0, j = chosenDataModel.getSize(); i < j; i++) {
            candidateModel.add(chosenDataModel.getElementAt(i));
        }
        chosenDataModel.clear();
    }
    
    @Listen(" onClick = #btnValider ")
	public  void enregistrer()
	{
		Set<Action> chosen = new HashSet<Action>( candidateModel.getInnerList());
		if(fonction != null)
		{
			fonction.setActions(chosen);
			FonctionService fs = (FonctionService) JNDIUtils.lookUpEJB(EJBRegistry.FonctionServiceBean);
			try {
				fs.modifier(fonction);
				getParent().detach();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
    
    @Listen(" onClick = #btnAnnuler ")
	public void onAnnuler(){
    	getParent().detach();
	}
 
   
    /**
     * Set new candidate ListModelList.
     *
     * @param candidate
     *            is the data of candidate list model
     */
    public void setModel(Set<Action> candidate,Set<Action> choosen) {
    	chosenLb.setModel(this.chosenDataModel = new ListModelList<Action>(choosen));
        candidateLb.setModel(this.candidateModel = new ListModelList<Action>(candidate)); 
    }
 
    /**
     * @return current chosen data list
     */
    public List<Action> getChosenDataList() {
        return new ArrayList<Action>(chosenDataModel);
    }
 
    public Fonction getFonction() {
		return fonction;
	}

	public void setFonction(Fonction fonction) {
		this.fonction = fonction;
	}

	private Set<Action> chooseOne() {
        Set<Action> set = candidateModel.getSelection();
        for (Action selectedItem : set) {
            chosenDataModel.add(selectedItem);
            candidateModel.remove(selectedItem);
        }
        return set;
    }
 
    private Set<Action> unchooseOne() {
        Set<Action> set = chosenDataModel.getSelection();
        for (Action selectedItem : set) {
            candidateModel.add(selectedItem);
            chosenDataModel.remove(selectedItem);
        }
        return set;
    }
 
    // Customized Event
    public class ChooseEvent extends Event {
        /**
         *
         */
        private static final long serialVersionUID = -7334906383953342976L;
 
        public ChooseEvent(Component target, Set<Action> data) {
            super("onChoose", target, data);
        }
    }
    
    
}