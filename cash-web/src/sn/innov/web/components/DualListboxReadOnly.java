package sn.innov.web.components;

import java.util.Set;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Div;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;

import sn.innov.be.authentification.entities.Fonction;
import sn.innov.be.security.entities.Action;

 
public class DualListboxReadOnly extends Div implements IdSpace {
    /**
     *
     */
    private static final long serialVersionUID = 5183321186606483396L;
     
    @Wire
    private Listbox candidateLb;
    @Wire
    private Listbox chosenLb;
    
    Fonction fonction = null;
 
    private ListModelList<Action> candidateModel;
    private ListModelList<Action> chosenDataModel;
 
    public DualListboxReadOnly() {
        Executions.createComponents("/WEB-INF/securite/referentiel/dualListboxReadOnly.zul", this, null);
        Selectors.wireComponents(this, this, false);
        Selectors.wireEventListeners(this, this);
        chosenLb.setModel(chosenDataModel = new ListModelList<Action>());
    }
 
   
    /**
     * Set new candidate ListModelList.
     *
     * @param candidate
     *            is the data of candidate list model
     */
    public void setModel(Set<Action> candidate,Set<Action> choosen) {
    	chosenLb.setModel(this.chosenDataModel = new ListModelList<Action>(choosen));
        candidateLb.setModel(this.candidateModel = new ListModelList<Action>(candidate)); 
    }
 
    public Fonction getFonction() {
		return fonction;
	}

	public void setFonction(Fonction fonction) {
		this.fonction = fonction;
	}
	
  }