package sn.innov.web.components;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.zkoss.image.AImage;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.A;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Span;

import sn.innov.web.common.utils.UIConstants;
import sn.innov.be.security.entities.LogAction;

public class MacroMsge extends A implements IdSpace{
	@Wire
	Image imgUtilisateur ;
	@Wire
	Label lblLogin, lblAction, lblTime;
	
	LogAction log ; 
	


	public MacroMsge(LogAction log) {
		Executions.createComponents("/templates/components/macro_msge.zul", this, null);
        Selectors.wireComponents(this, this, false);
        Selectors.wireEventListeners(this, this);
        
        imgUtilisateur.setTooltip(log.getUtilisateur().getLogin()) ;
		try {
			imgUtilisateur.setContent( new AImage( "t" , new FileInputStream(UIConstants.IMAGES_PROFIL_PATH+log.getUtilisateur().getImageProfil())));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lblLogin.setValue(log.getUtilisateur().getLogin()) ;
		lblAction.setValue(log.getAction().getDescription()) ;
		
		lblTime.setValue(log.getDateAction().toLocaleString());
		
		
	}



	
	public LogAction getLog() {
		return log;
	}



	public void setLog(LogAction log) {
		this.log = log;
	}
	
	
	

}
