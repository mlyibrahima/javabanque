
package sn.innov.web.components;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;

import sn.innov.web.common.utils.SessionAttributes;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.services.DroitsAccesService;
import sn.innov.be.security.services.LogActionService;

public class ActionButton extends Button implements AfterCompose{

	private String action;
	private String hideMode;

	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

	public String getHideMode() {
		return hideMode;
	}
	public void setHideMode(String hideMode) {
		this.hideMode = hideMode;
	}
	@Override
	public void afterCompose() {
		DroitsAccesService  droitsService = (DroitsAccesService) JNDIUtils.lookUpEJB(EJBRegistry.DroitsAccesServiceBean);
		Utilisateur connected = (Utilisateur) Sessions.getCurrent().getAttribute(SessionAttributes.CONNECTED_USER);
		
		if(!droitsService.hasAccess(connected.getFonction().getCode(),action))
		{
			if(hideMode.equals("hide"))
			{
				System.out.println("Hide ");
				detach();
			}
			else
			{
				System.out.println("Disable ");
				setDisabled(true);
			}
		}
		else
		{
			this.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

				@Override
				public void onEvent(Event event) throws Exception {
					
					
					Events.postEvent(WindowController.ON_CLICK_ACTION_BUTTON, (Component) getSpaceOwner(), action);
					
				}
			});
		}
	}
}