package sn.innov.web.common.utils;

public class EventConstants {
	public static final String ON_LIST_CHANGE = "onListChange";
	public static final String ON_DELETE = "onDelete";
	public static final String ON_ADD = "onAdd";

}
