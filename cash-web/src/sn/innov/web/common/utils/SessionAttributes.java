package sn.innov.web.common.utils;

public class SessionAttributes {
	public static final String CONNECTED_USER = "CONNECTED_USER";
	public static final String HTTP_PARAM_VALUES = "HTTP_PARAM_VALUES";
	public static final String CURRENT_SESSION_UTILISATEUR = "CURRENT_SESSION_UTILISATEUR";
}
