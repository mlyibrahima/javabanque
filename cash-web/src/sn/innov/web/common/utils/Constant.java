package  sn.innov.web.common.utils;

import java.math.BigDecimal;

public class Constant {
	
	public static BigDecimal COUT_SMS=new BigDecimal(20);
	
 	public static String MODE_CITIZEN_CONTRAT="CITIZEN_CONTRAT";

 	public static String MODE_CITIZEN_ALL="CITIZEN_ALL";

	
	public boolean verifieTelephone(String telephone){
		
		boolean result=false;
		
		if(telephone!=null)
			telephone=telephone.trim();
		else
			return false;
		
		if(telephone!=null && telephone.length()!=9 && telephone.length()!=12 && telephone.length()!=13 && telephone.length()!=14){
			
			return false;
		
		}else if(telephone!=null && telephone.length()==9){
									
			if(!telephone.substring(0,2).equals("77") && !telephone.substring(0,2).equals("78") && !telephone.substring(0,2).equals("76")
					&& !telephone.substring(0,2).equals("75") && !telephone.substring(0,2).equals("70"))
			{
				return false;
 			}
			
		}else if(telephone!=null && telephone.length()==12){
			 
			if(!telephone.startsWith("221"))
			{
				return false;
				
			}else{
			
			telephone=telephone.substring(0,3);
			
			if(!telephone.substring(0,2).equals("77") && !telephone.substring(0,2).equals("78") && !telephone.substring(0,2).equals("76")
					&& !telephone.substring(0,2).equals("75") && !telephone.substring(0,2).equals("70"))
			{
				return false;
 			}
			
			}			
			
		}else if(telephone!=null && telephone.length()==13 && !telephone.startsWith("+221")){
			
			if(!telephone.startsWith("+221"))
			{
				return false;
				
			}else{
			
			telephone=telephone.substring(0,4);
			
			if(!telephone.substring(0,2).equals("77") && !telephone.substring(0,2).equals("78") && !telephone.substring(0,2).equals("76")
					&& !telephone.substring(0,2).equals("75") && !telephone.substring(0,2).equals("70"))
			{
				return false;
 			}
			
			}			
		
		}else if(telephone!=null && telephone.length()==14 && !telephone.startsWith("00221")){
			
			if(!telephone.startsWith("00221"))
			{
				return false;
				
			}else{
			
			telephone=telephone.substring(0,5);
			
			if(!telephone.substring(0,2).equals("77") && !telephone.substring(0,2).equals("78") && !telephone.substring(0,2).equals("76")
					&& !telephone.substring(0,2).equals("75") && !telephone.substring(0,2).equals("70"))
			{
				return false;
 			}
			
			}			
	    
		} 
		
		
		return true;
		
		
		
	}

}
