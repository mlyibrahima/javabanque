package sn.innov.web.common.utils;

public class UIConstants {
	public static final String EDIT_MODE = "editMode", ADD_MODE="addMode";
	public static final String VIEW_MODE = "viewMode";
	public static final String IMAGES_PROFIL_PATH = "C:\\wamp\\www\\upload\\" ; 
	public static final String IMAGES_PROFIL_SIZE = "250px" ;
	public static final String IMAGES_BANDEAUX_PATH  = "C:\\jee\\images_profil\\";
	public static final String PARAM_EJB = "ejb" ;
	public static final String PARAM_METHOD = "methode" ;
	public static final String PARAM_PARAMS = "params" ;
//	public static final String UPLOAD_DIR =   "C:\\upload\\" ;
//	public static final String UPLOAD_DIR =   "/var/www/upload/";// 
	public static final String UPLOAD_DIR = "C:\\xampp\\htdocs\\upload\\" ;
//	public static final String UPLOAD_URL =  "http://84.246.227.69/upload/";//  "http://localhost/upload/" ;
	public static final String UPLOAD_URL =  "http://localhost:81/upload/" ;

}
