package sn.innov.web.common.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.zkoss.util.media.Media;
import org.zkoss.util.resource.Labels;
import org.zkoss.zul.Fileupload;


public class FileLoader {
	/**
	 * upload role file
	 * 
	 * @param cheminDossierPieces
	 * @return path of file
	 */
	public static String upload(String cheminDossierPieces) {
		String cheminPieceJointe = null;

		try {
			// open select file dialog
			Object media = Fileupload.get();
			if (media instanceof org.zkoss.util.media.Media) {
				org.zkoss.util.media.Media medium = (org.zkoss.util.media.Media) media;

				// get file name
				String nomPiece = cheminDossierPieces + medium.getName();

				// upload file
				boolean boolUpload = uploadFile(medium, nomPiece);
				if (boolUpload) {
					// remember file name
					cheminPieceJointe = nomPiece;
				} else {
					System.out.print("erreur upload fichier");
				}
			}

		} catch (Exception e) {
			// handle ERROR
			e.printStackTrace();
		}
		return cheminPieceJointe;
	}

	public static String uploadPieceDossier(String dgrCode, String pijcode, String cheminDossierPieces) {
		String fichierPieceJointe = null;

		try {
			// open select file dialog
			Object media = Fileupload.get();
			if (media instanceof org.zkoss.util.media.Media) {
				org.zkoss.util.media.Media medium = (org.zkoss.util.media.Media) media;

				String nomPiece = null;
				// get file name
				// if (ToolGrh.isWindows())
				// nomPiece = cheminDossierPieces + "\\" + dgrCode + "_" +
				// pijcode + "_" + medium.getName();
				// else
				// nomPiece = cheminDossierPieces + "/" + dgrCode + "_" +
				// pijcode + "_" + medium.getName();
				
				nomPiece = cheminDossierPieces + dgrCode + "_" + pijcode + "_" + medium.getName();

				// upload file
				boolean boolUpload = uploadFile(medium, nomPiece);
				if (boolUpload) {
					// remember file name
					fichierPieceJointe = dgrCode + "_" + pijcode + "_" + medium.getName();
				} else {
					System.out.print("erreur upload fichier");
				}
			}

		} catch (Exception e) {
			// handle ERROR
			e.printStackTrace();
		}
		return fichierPieceJointe;
	}
	
	
	public static String uploadPieceDossier(String dgrCode, String pijcode, String cheminDossierPieces, Media media) {
		String fichierPieceJointe = null;

		try {
			// open select file dialog
			
			if (media instanceof org.zkoss.util.media.Media) {
				org.zkoss.util.media.Media medium = (org.zkoss.util.media.Media) media;

				String nomPiece = null;
			
				nomPiece = cheminDossierPieces + dgrCode + "_" + pijcode + "_" + medium.getName();

				// upload file
				boolean boolUpload = uploadFile(medium, nomPiece);
				if (boolUpload) {
					// remember file name
					fichierPieceJointe = dgrCode + "_" + pijcode + "_" + medium.getName();
				} else {
					System.out.print("erreur upload fichier");
				}
			}

		} catch (Exception e) {
			// handle ERROR
			e.printStackTrace();
		}
		return fichierPieceJointe;
	}

	/**
	 * upload a file to server
	 * 
	 * @param medium
	 * @param a_strServerPath
	 * @param a_strFileName
	 * @return
	 * @throws IOException 
	 */
	public static boolean uploadFile(org.zkoss.util.media.Media medium, String nomPiece) throws IOException {
		boolean boolUpload = true;
		InputStream inputStream = medium.getStreamData();
		
		OutputStream fileOutputStream = new FileOutputStream(nomPiece);
		try {
			// buffer size in memory
			byte[] buffer = new byte[1024];
			// bytes of reading
			@SuppressWarnings("unused")
			int nread = 0;
		
			while ((nread = inputStream.read(buffer)) != -1) {
				// write to server restore
				fileOutputStream.write(buffer,0,nread);
			}
			// finish writing
			fileOutputStream.flush();
			fileOutputStream.close();
		} catch (Exception e) {
			// handle ERROR
			e.printStackTrace();
			boolUpload = false;
		}finally{
			if(inputStream != null) {
				inputStream.close();
			}
			if(fileOutputStream != null){
				fileOutputStream.close();
			}
		}
		return boolUpload;
	}
	
	public static void copyFile(File source, File dest)
			throws IOException {
		InputStream input = null;
		OutputStream output = null;
		try {
			input = new FileInputStream(source);
			output = new FileOutputStream(dest);
			byte[] buf = new byte[1024];
			int bytesRead;
			while ((bytesRead = input.read(buf)) > 0) {
				output.write(buf, 0, bytesRead);
			}
		} finally {
			input.close();
			output.close();
		}
	}

}
