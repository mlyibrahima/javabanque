package sn.innov.web.common.exceptions;

public class UnknownMethodException extends Exception {
	
	public UnknownMethodException(String message) {
		super(message) ;
	}

}
