package sn.innov.web.common.exceptions;

public class NullParameterException extends Exception {
	
	public NullParameterException(String message) {
		super(message) ;
	}

}
