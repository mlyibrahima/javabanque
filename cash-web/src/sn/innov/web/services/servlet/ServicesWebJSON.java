package sn.innov.web.services.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import sn.innov.web.common.exceptions.NullParameterException;
import sn.innov.web.common.exceptions.UnknownMethodException;
import sn.innov.web.common.utils.UIConstants;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;

/**
 * Servlet implementation class ServicesWebJSON
 */
@WebServlet("/ServicesWebJSON")
public class ServicesWebJSON extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String paramEJB, paramMethod, paramParams, err ;
		Gson g = new Gson() ;
		
		response.setContentType("application/json");
		PrintWriter writer = response.getWriter();
		
		try {
			
			paramEJB = checkNull(request.getParameter(UIConstants.PARAM_EJB)) ;
			paramMethod = checkNull(request.getParameter(UIConstants.PARAM_METHOD)) ;
			paramParams = request.getParameter(UIConstants.PARAM_PARAMS) ;
			EJBRegistry reg =  EJBRegistry.valueOf(paramEJB) ;
			
			
			
			Map<String, Object> map = new HashMap<String, Object>() ;
			map.put("code", "ok") ;
			map.put("result", invokeEJB(reg,paramMethod, paramParams) ) ;
			String json = g.toJson(map) ;
			writer.write(json);
			
		} catch (NullParameterException e) {
			err = "Parametres vides" ;
			Map<String, Object> map = new HashMap<String, Object>() ;
			map.put("code", "ko") ;
			map.put("result",err ) ;
			String json = g.toJson(map) ;
			writer.write(json);
			e.printStackTrace();
		}catch (IllegalArgumentException e) {
			err = "EJB non enregistre ";
			Map<String, Object> map = new HashMap<String, Object>() ;
			map.put("code", "ko") ;
			map.put("result",err ) ;
			String json = g.toJson(map) ;
			writer.write(json);
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (UnknownMethodException e) {
			err = "Methode non retrouvee" ;
			Map<String, Object> map = new HashMap<String, Object>() ;
			map.put("code", "ko") ;
			map.put("result",err ) ;
			String json = g.toJson(map) ;
			writer.write(json);
			e.printStackTrace();
		}
	}
	
	private Object invokeEJB(EJBRegistry reg, String paramMethod,
			String paramParams) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, UnknownMethodException {
		Object ejb = JNDIUtils.lookUpEJB(reg) ;
		String[] params =  new String[]{} ;
		if(paramParams != null && !paramParams.isEmpty())
			params = paramParams.split(",") ;
		Method[] methods = ejb.getClass().getMethods();
		for (int i = 0; i < methods.length; i++) {
			Method meth = methods[i] ;
			int reflecParamLength = meth.getParameterTypes().length;
			int paramLength = params.length;
			if(meth.getName().equalsIgnoreCase(paramMethod) && reflecParamLength == paramLength) {
				Object[] castedParameters = cast(params, meth) ;
				return meth.invoke(ejb, castedParameters) ;
				
			}
				
		}
		throw new UnknownMethodException("Methode non retrouvee");
	}

	private Object[] cast(String[] params, Method meth) {
		Class[] types =   meth.getParameterTypes() ;
		List<Object> castedArgs = new ArrayList<Object>() ;
		
		for (int i = 0; i < params.length; i++) {
			/* definition des cast pour chaque type de parametres*/
			if(types[i].equals(Long.class)) {
				castedArgs.add(Long.parseLong(params[i]));
			}
			else if(types[i].equals(String.class)) {
				castedArgs.add(new String(params[i]));
			} 
			else if(types[i].equals(Integer.class)) {
				castedArgs.add(Integer.parseInt(params[i]));
			}
			else if(types[i].equals(int.class)) {
				castedArgs.add(Integer.parseInt(params[i]));
			}
			else if(types[i].equals(long.class)) {
				castedArgs.add(Long.parseLong(params[i]));
			}
			
		}
		
		return castedArgs.toArray();
	}

	private String checkNull(Object parameter) throws NullParameterException {
		if(parameter == null)
			throw new NullParameterException("parametre null") ;
		else
			return parameter.toString() ;
	}

}
