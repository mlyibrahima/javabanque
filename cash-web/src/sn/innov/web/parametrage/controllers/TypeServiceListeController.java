package sn.innov.web.parametrage.controllers;

import java.util.Date;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.parametrage.entities.TypeService;
import sn.innov.be.referentiel.services.ReferentielCommonService;

public class TypeServiceListeController extends WindowController {
	
	@Wire
	Listbox lstTypeServices;
	
	@Wire
	Groupbox typeServicedetail;
	
	@Wire
	Textbox code,nom;
	
	/**
	 * Liste des actions de cet écran
	 */
	static final String N_TYPESERVICE = "N_TYPESERVICE", 
						U_TYPESERVICE = "U_TYPESERVICE",
						D_TYPESERVICE = "D_TYPESERVICE";
	
	TypeService typeService=null;
							
	/**
	 * Ressources appelées par cet ecran
	 */
	
	ReferentielCommonService contratService = (ReferentielCommonService) JNDIUtils.lookUpEJB(EJBRegistry.ReferentielCommonServiceBean);
	
 
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		

		  super.doAfterCompose(comp);		
		  Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}
	
	/**
	 * Ecouteur de l'evenement onListChange pour recharger la liste des fonctions
	 */
	@Listen(EventConstants.ON_LIST_CHANGE+" = window")
	public void afficherListeTypeService()
	{
		List<TypeService> typeServices = contratService.lister(TypeService.class);
		ListModelList<TypeService> typeServiceModel = new ListModelList<TypeService>(typeServices);
		lstTypeServices.setModel(typeServiceModel);
	}
	
	@Listen("onOK = window")
	public void onOK(){
		
/*	if(search.getValue()!=null )
	{
 		List<TypeService> typeServices = contratService.listerTypeService(search.getValue());
		ListModelList<TypeService> typeServiceModel = new ListModelList<TypeService>(typeServices);
		lstTypeServices.setModel(typeServiceModel);
 
	}*/
 		
 	}

	
	@Listen("onValider = window")
	public void onValider(){
		
		if(typeService==null)
			typeService=new TypeService();
		
		typeService.setCode(code.getValue());
		typeService.setNom(nom.getValue());
 		
		try {
			
			contratService.save(typeService);
			alert("TypeService enregistre");
			Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);

					
		} catch (Exception e) {
			e.printStackTrace();
		}				
	}
	
	/**
	 * Switch des évenements lancés par un action button
	 * @param event
	 */
	@Listen(ON_CLICK_ACTION_BUTTON+" = window")
	public void onClickActionButton(Event event)
	{
		String action = (String) event.getData();

		if(action.equalsIgnoreCase(N_TYPESERVICE)){
 			
			typeService=new TypeService();
 			code.setValue("");
  			nom.setValue("");
			code.setDisabled(false);
		}
		
		else if(action.equalsIgnoreCase(U_TYPESERVICE)){
			if(lstTypeServices.getSelectedItem() != null){
 				
				code.setDisabled(true);
				typeService=(TypeService)lstTypeServices.getSelectedItem().getValue();
 				code.setValue(typeService.getCode());
 				nom.setValue(typeService.getNom());
  				
			}else
				alert("Attention, veuillez selectionner  un elemnt d abord");
			
		}
 		else if(action.equalsIgnoreCase(D_TYPESERVICE)){
 			
 			if(lstTypeServices.getSelectedItem()!=null)
 			{
			
 			 typeService=(TypeService)lstTypeServices.getSelectedItem().getValue();

				Messagebox.show("Voulez vous supprimer la typeService", "Cancel Order", new Messagebox.Button[]{ 
				        Messagebox.Button.YES, Messagebox.Button.NO}, Messagebox.QUESTION, 
	  
						new EventListener<Messagebox.ClickEvent>() {
						
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if(event.getButton() == Button.YES){
								
								try{
									
 		 							if(contratService.supprimer(TypeService.class, typeService.getCode()))
 		 								alert("Suppression effectu�e ! ");
 		 							else
 		 								alert("Erreur los de la Suppression ! ");

								}catch(Exception e){
									
									alert("Erreur lors de la Suppression de la typeService.");
									
								}
								
		 							Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);

							
							}else if(event.getButton() == Button.NO) {
			
							}
						}	
					});
				
				
				 }else
				alert("Attention, veuillez selectionner  un élément d abord");
			
		}
		
		  Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);

	} 
	
	 
	
}
