package sn.innov.web.parametrage.controllers;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Row;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.parametrage.entities.Commission;
import sn.innov.be.parametrage.entities.Grille;
import sn.innov.be.parametrage.entities.TypeCommission;
import sn.innov.be.parametrage.services.ParametrageService;

public class CommissionController extends WindowController {

	@Wire 
	Listbox lstgrille;
	
	@Wire 
	Listbox lsttype;
	
	@Wire
	Row RowGrille,RowPourcentage,RowFixe;
	
 	
	@Wire
	Listbox listcommission;
	
	@Wire
	Textbox code,description;
	
	@Wire
	Decimalbox montant,pourcentage;
	
	
	ParametrageService adminParametersService = (ParametrageService) JNDIUtils.lookUpEJB(EJBRegistry.ParametrageServiceBean);

	private String selectedGrille;
	
	Commission commission=null;
	
	TypeCommission type=null;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		List<Grille> grilles=adminParametersService.findGrilles(-1, -1);
		for (Grille grille : grilles){
			Listitem iListitem=new Listitem(grille.getDescription(),grille.getCode());
			iListitem.setParent(lstgrille);
		}
		
		listcommission.setItemRenderer(new ListitemRenderer<Commission>() {

			@Override
			public void render(Listitem item, Commission data, int index)
					throws Exception {
				
				item.setValue(data);
				
				
				new Listcell(data.getCode()).setParent(item);
				new Listcell(""+data.getDescription()).setParent(item);
				new Listcell(""+data.getType().name()).setParent(item);
				
				if(data.getMontantFixe()!=null)
				new Listcell(""+data.getMontantFixe()).setParent(item);
				else
					new Listcell("").setParent(item);

				if(data.getPourcentage()!=0)
					new Listcell(""+data.getPourcentage()).setParent(item);
				else
					new Listcell("").setParent(item);
	
				if(data.getGrille()!=null)
				new Listcell(""+data.getGrille().getCode()).setParent(item);
				else
				new Listcell("").setParent(item);

 			
			}
		});
		
 		refresh();
	}
	
	private void refresh()
	{
		List<Commission> commissions = adminParametersService.findCommissions(-1,-1);
		listcommission.setModel(new SimpleListModel<Commission>(commissions));
  		
	}
	
	@Listen("onNouveau = window")
	public void onNouveau()
	{
		commission=new Commission();
		code.setValue("");
		description.setValue("");
		
		RowFixe.setVisible(false);
		RowGrille.setVisible(false);
		RowPourcentage.setVisible(false);
	}
	
	@Listen("onSelect = #lsttype")
	public void onSelect()
	{
		type=TypeCommission.valueOf(TypeCommission.class,(String)lsttype.getSelectedItem().getValue());
		
		if(type==TypeCommission.FIXE)			
		{
			RowFixe.setVisible(true);
			RowGrille.setVisible(false);
			RowPourcentage.setVisible(false);
			
		}else if(type==TypeCommission.VARIABLE)			
		{
			RowFixe.setVisible(false);
			RowGrille.setVisible(false);
			RowPourcentage.setVisible(true);
			
		}else if(type==TypeCommission.DEUX_PARTIE)		
		{
			RowFixe.setVisible(true);
			RowGrille.setVisible(false);
			RowPourcentage.setVisible(true);
			
		}else if(type==TypeCommission.BAREME)			
		{
			RowFixe.setVisible(false);
			RowGrille.setVisible(true);
			RowPourcentage.setVisible(false);
			
		}
 		
	}
	
	@Listen("onSupprimer = window")
	public void onSupprimer()
	{
		if(listcommission.getSelectedItem()!=null)
		{
			Messagebox.show("Voulez vous supprimer la commission", "Cancel Order", new Messagebox.Button[]{ 
			        Messagebox.Button.YES, Messagebox.Button.NO}, Messagebox.QUESTION, 
  
					new EventListener<Messagebox.ClickEvent>() {
					
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if(event.getButton() == Button.YES){
							try{
								
								adminParametersService.deleteCommission(((Commission)listcommission.getSelectedItem().getValue()).getCode());
								
								refresh();
								
							}catch(Exception e){
								
								alert("Erreur lors de la Suppression");
								
							}
						
						}else if(event.getButton() == Button.NO) {
		
						}
					}	
				});
			
		}
	}
	
	@Listen("onEditer = window")
	public void onEditer()
	{
		lstgrille.clearSelection();
		pourcentage.setValue(""+0);
		montant.setValue(""+0);
		
		if(listcommission.getSelectedItem()==null)
			alert("selectionner une commission");
		else{
		  commission=(Commission)listcommission.getSelectedItem().getValue();
		
		code.setValue(commission.getCode());
		description.setValue(commission.getDescription());
		
		if(commission.getMontantFixe()!=null)
			montant.setValue(commission.getMontantFixe());

		if(commission.getPourcentage()!=0)
			pourcentage.setValue(""+commission.getPourcentage());
		
		String type=commission.getType().name();
		
 		for (int i = 0; i < lsttype.getItemCount(); i++) {
			
				if(type.equals((String)lsttype.getItemAtIndex(i).getValue()))
				{
					lsttype.setSelectedIndex(i);
				}
			}
			
		 if(type.equals(TypeCommission.BAREME.name()) && commission.getGrille()!=null){
			 
			 for (int i = 0; i < lstgrille.getItemCount(); i++) {
					
					if(commission.getGrille().getCode().equals((String)lstgrille.getItemAtIndex(i).getValue()))
					{
						lstgrille.setSelectedIndex(i);
					}
		 }
			 
		 }
		
		}
 
		 
 		
	}
	 
	@Listen("onValider = window")
	public void onValider()
	{
		if(commission!=null){
			
  		commission.setCode(code.getValue());
 		commission.setDescription(description.getValue());
 		
 		
		if(montant.getValue()!=null)
			commission.setMontantFixe(montant.getValue());

		if(pourcentage.getValue()!=null)
			commission.setPourcentage(pourcentage.getValue().floatValue());
	
		if(lsttype.getSelectedItem()!=null && lsttype.getSelectedItem().getValue()!=null){
			
			alert((String)lsttype.getSelectedItem().getValue());
			
			if(((String)lsttype.getSelectedItem().getValue()).equals("FIXE"))
			type=TypeCommission.FIXE;
			else if(((String)lsttype.getSelectedItem().getValue()).equals("VARIABLE"))
			type=TypeCommission.VARIABLE;
			else if(((String)lsttype.getSelectedItem().getValue()).equals("DEUX_PARTIE"))
			type=TypeCommission.DEUX_PARTIE;
			else if(((String)lsttype.getSelectedItem().getValue()).equals("BAREME"))
			type=TypeCommission.BAREME;
			

 			commission.setType(type);
		}
		if(lstgrille.getSelectedItem()!=null && lstgrille.getSelectedItem().getValue()!=null){
			
			Grille grillle=adminParametersService.findGrille((String)lstgrille.getSelectedItem().getValue());
			commission.setGrille(grillle);
		}
		
		adminParametersService.addCommission(commission);
		
		alert("Commission sauvegardee");
		
		}
		
		
		
		refresh();
	}
	
	
}
