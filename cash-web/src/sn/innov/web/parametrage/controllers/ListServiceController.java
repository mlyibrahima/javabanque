package sn.innov.web.parametrage.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.parametrage.entities.CashService;
import sn.innov.be.parametrage.entities.TypePaiement;
import sn.innov.be.parametrage.entities.TypeService;
import sn.innov.be.referentiel.services.ReferentielCommonService;
import sn.innov.be.security.services.CashServiceService;
import sn.innov.be.uniteorganisationnelle.entities.UO;

public class ListServiceController extends WindowController {
	
	@Wire
	Listbox listServices;
	
	@Wire
	Groupbox servicedetail;
	
	
	/**
	 * 
	 * Liste des actions de cet écran
	 * 
	 */
	static final String N_SERVICE = "N_SERVICE", 
						U_SERVICE = "U_SERVICE",
						D_SERVICE = "D_SERVICE";
	
 	CashService service=null;
								 
 	CashServiceService serviceService = (CashServiceService) JNDIUtils.lookUpEJB(EJBRegistry.CashServiceServiceBean);
 	ReferentielCommonService referentielCommonTypeService = (ReferentielCommonService) JNDIUtils.lookUpEJB(EJBRegistry.ReferentielCommonServiceBean);

	static final String	URI_FORM="/WEB-INF/services/services_detail.zul";

 
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		

		  super.doAfterCompose(comp);		
		  
		  Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}
	
	
	/**
	 * Ecouteur de l'evenement onListChange pour recharger la liste des fonctions
	 */
	
	@Listen(EventConstants.ON_LIST_CHANGE+" = window")
	public void afficherListeService()
	{
		List<CashService> services = serviceService.listerServices();
		ListModelList<CashService> serviceModel = new ListModelList<CashService>(services);
		listServices.setModel(serviceModel);
	}
	
 	
	@Listen("onDelete = window")
	public void onDelete(){

		if(listServices.getSelectedItem()!=null && service!=null)
		{
			service=(CashService)listServices.getSelectedItem().getValue();
			
			confirm("Etes vous sur de bien vouloire supprimer l element?", "Suppression",
				new EventListener<Messagebox.ClickEvent>() {
				
				@Override
				public void onEvent(ClickEvent event) throws Exception {
					if(event.getButton() == Button.YES){
						
						try{
						
 							serviceService.supprimerService(service);
 							Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
							
						}catch(Exception e){
							
							alert("Erreur lors de la Suppression");
							
						}
						
					}else if(event.getButton() == Button.NO) {
	
					}
				}	
			});
			
		
		}
	}
	
	/**
	 * Switch des évenements lancés par un action button
	 * @param event
	 */
	@Listen(ON_CLICK_ACTION_BUTTON+" = window")
	public void onClickActionButton(Event event)
	{
		String action = (String) event.getData();

		if(action.equalsIgnoreCase(N_SERVICE)){
 			
			service=new CashService();	
			HashMap<String, Object> params = new HashMap<String, Object>();
  			params.put("service", service);
			params.put("mode", N_SERVICE);
			loadEmbedded(URI_FORM, getSelf(), params);
			
   		} 
		else if(action.equalsIgnoreCase(U_SERVICE)){
			
			if(listServices.getSelectedItem() != null){
				
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("service", (CashService)listServices.getSelectedItem().getValue());
				params.put("mode", U_SERVICE);
				loadEmbedded(URI_FORM, getSelf(), params);
				
			}else
				alert("Attention, veuillez selectionner  un élément d'abord");
			
		}
 		else if(action.equalsIgnoreCase(D_SERVICE)){
			if(listServices.getSelectedItem() != null){
				
				confirm("Etes vous sur de bien vouloire supprimer l element?", "Suppression action",
					
					new EventListener<Messagebox.ClickEvent>() {
					
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if(event.getButton() == Button.YES){
							
							try{
							
								service=(CashService)listServices.getSelectedItem().getValue();
								
								serviceService.supprimerService(service);
 								
							}catch(Exception e){
								
								alert("Erreur lors de la Suppression");
								
							}
							
						}else if(event.getButton() == Button.NO) {
		
						}
					}	
				});
				
			}else
				alert("Attention, veuillez selectionner  un élément d abord");
			
		}
		
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);

	} 
	
	 
	
}
