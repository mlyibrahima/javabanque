package sn.innov.web.parametrage.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Action;
import sn.innov.be.security.entities.Fonctionnalite;
import sn.innov.be.security.entities.Module;
import sn.innov.be.security.services.FonctionnaliteService;
import sn.innov.be.security.services.ModuleService;
import sn.innov.be.parametrage.services.ProjetService;
import sn.innov.be.parametrage.entities.Projet;


public class ProjetController extends WindowController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Wire
	Listbox listFonctionnalites;
	
	@Wire
	Listbox listafficheprojet;
	
	@Wire
	Bandbox bdModule;
	@Wire
	Listbox listModule;

	
	/**
	 * Liste des actions de cet �cran
	 */
	static final String N_PROJET=  "N_PROJET", 
						U_PROJET = "U_PROJET",
						D_PROJET = "D_PROJET";
	
	/**
	 * Ressources appel�es par cet ecran
	 */
	static final String	URI_FORM="/WEB-INF/exploitation/form.zul";
						
	ModuleService moduleService = (ModuleService) JNDIUtils.lookUpEJB(EJBRegistry.ModuleServiceBean);
	FonctionnaliteService fonctionnaliteService = (FonctionnaliteService) JNDIUtils.lookUpEJB(EJBRegistry.FonctionnaliteServiceBean);
    
	ProjetService projetService = (ProjetService) JNDIUtils.lookUpEJB(EJBRegistry.ProjetServiceBean);

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}
	
	@Listen("onOpen = #bdModule")
	public void chargerModule()
	{

		/*List<Module> fonctionnalites = moduleService.listerModules();
		ListModelList<Module> fonctionnaliteModel = new ListModelList<Module>(fonctionnalites);
		listModule.setModel(fonctionnaliteModel);*/
		
		List<Projet> fonctionnalites = projetService.listerFonctionnalites();
		ListModelList<Projet> modelFonctionnalite = new ListModelList<Projet>(fonctionnalites);
		listafficheprojet.setModel(modelFonctionnalite);
	}
	
	@Listen(" onSelect = #listafficheprojet ")
	public void onChargerModules(){
		if(listafficheprojet.getSelectedItem() != null) {
			Projet projet= listafficheprojet.getSelectedItem().getValue() ;
			try {
				
		 		List<Projet> fonctionnalites = projetService.listerFonctionnalites(projet.getCode());
				ListModelList<Projet> modelFonctionnalite = new ListModelList<Projet>(fonctionnalites);
				listFonctionnalites.setModel(modelFonctionnalite);
			} catch (Exception e) {
				e.printStackTrace();
			}
			bdModule.close();

		}
	}

	/**
	 * Ecouteur de l'evenement onListChange pour recharger la liste des fonctions
	 */
	@Listen(EventConstants.ON_LIST_CHANGE+" = window")
	public void afficherListeFonctionnalites()
	{   
		//ProjetService projetService = (ProjetService) JNDIUtils.lookUpEJB(EJBRegistry.ProjetServiceBean);
 		List<Projet> fonctionnalites = projetService.listerFonctionnalites();
		ListModelList<Projet> modelFonctionnalite = new ListModelList<Projet>(fonctionnalites);
		listFonctionnalites.setModel(modelFonctionnalite);
		listafficheprojet.setModel(modelFonctionnalite);
	}
	
	/**
	 * Switch des �venements lanc�s par un action button
	 * @param event
	 */
	@Listen(ON_CLICK_ACTION_BUTTON+" = window")
	public void onClickActionButton(Event event)
	{
		String action = (String) event.getData();
		if(action.equalsIgnoreCase(N_PROJET)){
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("projet", new Projet());
			loadPopUp(URI_FORM, params );
			
		}else if(action.equalsIgnoreCase(U_PROJET)){
			if(listFonctionnalites.getSelectedItem() != null){
				HashMap<String, Object> params = new HashMap<String, Object>();
				
				params.put("projet", (Projet)listFonctionnalites.getSelectedItem().getValue());
				loadPopUp(URI_FORM, params );
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
			
		}else if(action.equalsIgnoreCase(D_PROJET)){
			if(listFonctionnalites.getSelectedItem() != null){
				confirm("Etes vous sur de bien vouloire supprimer l element?", "Suppression Projet",
					new EventListener<Messagebox.ClickEvent>() {
					
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if(event.getButton() == Button.YES){
							Events.postEvent(EventConstants.ON_DELETE,((Window)getSelf().getSpaceOwner()), null);	
						}else if(event.getButton() == Button.NO) {
							alert("vous avez annuler la suppression");
						}
					}	
				});
				
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
		}
	}
	
	/**
	 * Ecouteur de l'evenment: suppression confirm�e
	 */
	@Listen(EventConstants.ON_DELETE+" = window")
	public void supprimerFonctionnalites()
	{   
		System.out.println("bonjour");
		ProjetService fonctionnaliteService = (ProjetService) JNDIUtils.lookUpEJB(EJBRegistry.ProjetServiceBean);
		Projet fonctionnalite = listFonctionnalites.getSelectedItem().getValue();
		if(fonctionnaliteService.supprimerProjet(fonctionnalite) == null) 
		{alert("Suppression impossible");return;}
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}

}
