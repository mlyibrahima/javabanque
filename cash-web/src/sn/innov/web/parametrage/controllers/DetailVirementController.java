package sn.innov.web.parametrage.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.entities.UOTypes;
import sn.innov.be.parametrage.entities.CashService;
import sn.innov.be.parametrage.entities.TypePaiement;
import sn.innov.be.parametrage.entities.TypeService;
import sn.innov.be.parametrage.entities.UOCashService;
import sn.innov.be.referentiel.services.ReferentielCommonService;
import sn.innov.be.security.services.CashServiceService;
import sn.innov.be.uniteorganisationnelle.entities.UO;
import sn.innov.be.uniteorganisationnelle.services.UOService;

public class DetailVirementController extends WindowController {
	
	@Wire
	Listbox listTypeService,listTypePaiement,listServices,lstOperateurs,lstOperateursChoisis;
	
	@Wire
	Groupbox servicedetail;
	
	@Wire
	Textbox code,nom;
	
	
	/**
	 * 
	 * Liste des actions de cet écran
	 * 
	 */
	static final String N_SERVICE = "N_SERVICE", 
						U_SERVICE = "U_SERVICE",
						D_SERVICE = "D_SERVICE";
	
 	CashService service=null;
								 
 	CashServiceService cashService = (CashServiceService) JNDIUtils.lookUpEJB(EJBRegistry.CashServiceServiceBean);
 	ReferentielCommonService referentielCommonTypeService = (ReferentielCommonService) JNDIUtils.lookUpEJB(EJBRegistry.ReferentielCommonServiceBean);

	String mode="";
	
	UOService uoService = (UOService) JNDIUtils.lookUpEJB(EJBRegistry.UOServiceBean);
	
	String ID_UO="";
	
	UO UO=null;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		
		  super.doAfterCompose(comp);		
		  
	  		List<TypeService> services = referentielCommonTypeService.lister(TypeService.class);
	   		ListModelList<TypeService> parteListModelList = new ListModelList<TypeService>(services);
	   		listTypeService.setModel(parteListModelList);
	   		
	   		List<TypePaiement> paiements = referentielCommonTypeService.lister(TypePaiement.class);
	   		ListModelList<TypePaiement> paiListModelList = new ListModelList<TypePaiement>(paiements);
	   		listTypePaiement.setModel(paiListModelList);
	   		
	   		/**
			 * LISTE OPERATEUR
			 */

			List<UO> uoOperateurs = uoService.getListUOByType(UOTypes.OPERATEUR.name());
			ListModelList<UO> modelOperateurs = new ListModelList<UO>(uoOperateurs);
			lstOperateursChoisis.setModel(modelOperateurs);
			lstOperateursChoisis.setCheckmark(true);
			lstOperateursChoisis.setMultiple(true);
			
			HashMap<String, Object> params = (HashMap<String, Object>) Executions.getCurrent().getArg();		
			mode=(String)params.get("mode");
			service = (CashService) params.get("service");
	 		
	 		if(mode!=null && mode.equals(U_SERVICE))
			{
				code.setValue(service.getCode());
				nom.setValue(service.getNom());
				code.setDisabled(true);
			}
			
	   		
 	}
	
	@Listen("onAfterRender = #listTypeService")
	public void onAfterRenderTypeService()
	{
		if(service.getType()!=null)
			for (int i = 0; i < listTypeService.getItemCount(); i++) {
				 
				 TypeService typeService=(TypeService)listTypeService.getItemAtIndex(i).getValue();

				 //alert(typeService.getCode());
				 
				 if(typeService.getCode().equals(service.getType().getCode()))
				 {
					 listTypeService.setSelectedIndex(i);
					 break;
				 }
			}
	}
	
	
	@Listen("onAfterRender = #listTypePaiement")
	public void onAfterRenderTypePaiement()
	{
		if(service.getPaiement()!=null)
			 for (int i = 0; i < listTypePaiement.getItemCount(); i++) {
					
				 TypePaiement typePaiement=(TypePaiement)(listTypePaiement.getItemAtIndex(i).getValue());
				 if(typePaiement.getCode().equals(service.getPaiement().getCode()))
				 {
					 listTypePaiement.setSelectedIndex(i);
					 break;
				 }
			}
	}
	
	/**
	 *  M�thode permettant de faire le render sur la liste des operateurs
	 */
	
	@Listen("onAfterRender = #lstOperateursChoisis")
	public void onAfterRenderlstOperateursChoisis()
	{
 		Set<Listitem> items=new HashSet<Listitem>();
		List<UOCashService> uoServices=cashService.listerUOCashServices(service.getCode(), UOTypes.OPERATEUR.name());
		
		if(service!=null && mode!=null && mode.equals(U_SERVICE))
		{
			for (int i = 0; i < lstOperateursChoisis.getItemCount(); i++) {
				
				for(UOCashService uoc: uoServices){
					
			        if(uoc.getUo().getId()==((UO)lstOperateursChoisis.getItemAtIndex(i).getValue()).getId()){
 			           
			        	items.add(lstOperateursChoisis.getItemAtIndex(i));
			        } 
			    }
		}
			
        	lstOperateursChoisis.setSelectedItems(items);
		}
	}
	
	/**
	 * Ecouteur de l'evenement onListChange pour recharger la liste des fonctions
	 */
	
	@Listen(EventConstants.ON_LIST_CHANGE+" = window")
	public void afficherListeService()
	{/*
		List<CashService> services = serviceService.listerServices();
		ListModelList<CashService> serviceModel = new ListModelList<CashService>(services);
		listServices.setModel(serviceModel);
	*/}
	
	
	@Listen("onReturn = window")
	public void onReturn() {
		
		loadBookmark("services");
	}
	
	@Listen("onSave = window")
	public void onSave(){
		
		try {

		if(service==null)
		{
			service=new CashService();

		}			
		
		service.setCode(code.getValue());
		service.setNom(nom.getValue());
		if(listTypeService.getSelectedItem()!=null)
			service.setType((TypeService)listTypeService.getSelectedItem().getValue());
		if(listTypePaiement.getSelectedItem()!=null)
			service.setPaiement((TypePaiement)listTypePaiement.getSelectedItem().getValue());
		
		
		Set<Listitem> uos=lstOperateursChoisis.getSelectedItems();
		List<UO>  operateurs=new ArrayList<UO>();
		
		for (Listitem listitem : uos) {
			
			operateurs.add((UO)listitem.getValue());
			
		}
		
		cashService.save(service,operateurs);
 			
		alert("Service enregistre");
 			
			Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);

 		} catch (Exception e) {
			e.printStackTrace();
		}				
	}

	
	@Listen("onDelete = window")
	public void onDelete(){

		if(listServices.getSelectedItem()!=null && service!=null)
		{
			service=(CashService)listServices.getSelectedItem().getValue();
			
			confirm("Etes vous sur de bien vouloire supprimer l element?", "Suppression",
				new EventListener<Messagebox.ClickEvent>() {
				
				@Override
				public void onEvent(ClickEvent event) throws Exception {
					if(event.getButton() == Button.YES){
						
						try{
						
 							cashService.supprimerService(service);
 							Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
							
						}catch(Exception e){
							
							alert("Erreur lors de la Suppression");
							
						}
						
					}else if(event.getButton() == Button.NO) {
	
					}
				}	
			});
			
		
		}
	}
	
	/**
	 * Switch des évenements lancés par un action button
	 * @param event
	 */
	@Listen(ON_CLICK_ACTION_BUTTON+" = window")
	public void onClickActionButton(Event event)
	{
		String action = (String) event.getData();

		if(action.equalsIgnoreCase(N_SERVICE)){
 			
			service=new CashService();	
			code.setValue("");
			nom.setValue("");
			
			code.setDisabled(false);
   		} 
		else if(action.equalsIgnoreCase(U_SERVICE)){
		
			if(listServices.getSelectedItem() != null){
 				
				service=(CashService)listServices.getSelectedItem().getValue();
			
				 code.setValue(service.getCode());
				 nom.setValue(service.getNom());
				 
				 for (int i = 0; i < listTypeService.getItemCount(); i++) {
					
					 TypeService typeService=(TypeService)listTypeService.getItemAtIndex(i).getValue();
					 if(typeService.getCode().equals(service.getType().getCode()))
					 {
						 listTypeService.setSelectedIndex(i);
 					 }
				}
				 
				 for (int i = 0; i < listTypePaiement.getItemCount(); i++) {
						
					 TypePaiement typePaiement=(TypePaiement)listTypePaiement.getItemAtIndex(i).getValue();
					 if(typePaiement.getCode().equals(service.getPaiement().getCode()))
					 {
						 listTypePaiement.setSelectedIndex(i);
 					 }
				}
				 
				 code.setDisabled(true);

				
			}else
				alert("Attention, veuillez selectionner  un �l�ment d'abord");
			
		}
 		else if(action.equalsIgnoreCase(D_SERVICE)){
			if(listServices.getSelectedItem() != null){
				
				confirm("Etes vous sur de bien vouloire supprimer l element?", "Suppression action",
					
					new EventListener<Messagebox.ClickEvent>() {
					
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if(event.getButton() == Button.YES){
							
							try{
							
								service=(CashService)listServices.getSelectedItem().getValue();
								
								cashService.supprimerService(service);
 								
							}catch(Exception e){
								
								alert("Erreur lors de la Suppression");
								
							}
							
						}else if(event.getButton() == Button.NO) {
		
						}
					}	
				});
				
			}else
				alert("Attention, veuillez selectionner  un élément d abord");
			
		}
		
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);

	} 
	
	 
	
}
