package sn.innov.web.parametrage.controllers;

import java.math.BigDecimal;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.parametrage.entities.Grille;
import sn.innov.be.parametrage.entities.Palier;
import sn.innov.be.parametrage.services.GrilleService;

public class GrilleListController extends WindowController {
	
	@Wire
	Listbox lstGrille,lstPaliers;
	
	@Wire
	Groupbox grilledetail;
	
	@Wire
	Textbox code,description;
	
	@Wire
	Decimalbox dbInf,dbSup,dbVal;
	
	/**
	 * Liste des actions de cet écran
	 */
	static final String N_PALIER = "N_PALIER", 
						U_PALIER = "U_PALIER",
						D_PALIER = "D_PALIER";
	
	Grille grille=null;
	Palier palier=null;
							
	 
 	GrilleService grilleService = (GrilleService) JNDIUtils.lookUpEJB(EJBRegistry.GrilleServiceBean);
 	
 
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		

		  super.doAfterCompose(comp);		
		  Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}
	
	public void afficherTouteGrille()
	{
		
  		List<Palier> paliers = grilleService.listerPaliers(grille.getCode());
   		ListModelList<Palier> grilleModel = new ListModelList<Palier>(paliers);
		lstPaliers.setModel(grilleModel);
 	}
	
	
	/**
	 * Ecouteur de l'evenement onListChange pour recharger la liste des fonctions
	 */
	
	@Listen(EventConstants.ON_LIST_CHANGE+" = window")
	public void afficherListeGrille()
	{
		List<Grille> grilles = grilleService.listerGrille();
		ListModelList<Grille> grilleModel = new ListModelList<Grille>(grilles);
		lstGrille.setModel(grilleModel);
	}
	
	@Listen("onOK = window")
	public void onOK(){
		
		try {

		if(grille==null)
		{
			grille=new Grille();
			grille.setCode(code.getValue());
			grille.setDescription(description.getValue());
			grilleService.ajouterGrille(grille);

		}else
		{
			grille.setCode(code.getValue());
			grille.setDescription(description.getValue());
			grilleService.save(grille);

		}
		
 			alert("Grille enregistr�");
			Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);

 		} catch (Exception e) {
			e.printStackTrace();
		}				
	}

	@Listen("onChoice = window")
	public void onChoice(){

		if(lstGrille.getSelectedItem()!=null)
		{
			grille=(Grille)lstGrille.getSelectedItem().getValue();
			description.setValue(grille.getDescription());
			code.setValue(grille.getCode());
 			afficherTouteGrille();
		}
	}
	
	
	
	@Listen("onDelete = window")
	public void onDelete(){

		if(lstGrille.getSelectedItem()!=null && grille!=null)
		{
			grille=(Grille)lstGrille.getSelectedItem().getValue();
			
			confirm("Etes vous sur de bien vouloire supprimer l element?", "Suppression",
				new EventListener<Messagebox.ClickEvent>() {
				
				@Override
				public void onEvent(ClickEvent event) throws Exception {
					if(event.getButton() == Button.YES){
						
						try{
						
 							grilleService.supprimerGrille(grille);
 							Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
							
						}catch(Exception e){
							
							alert("Erreur lors de la Suppression");
							
						}
						
					}else if(event.getButton() == Button.NO) {
	
					}
				}	
			});
			
		
		}
	}
	
	@Listen("onValider = window")
	public void onValider(){
	
	try {
			
		if(palier==null)
		{
			palier=new Palier();
			palier.setBorneInferieure(dbInf.getValue().floatValue());
			palier.setBorneSuperieure(dbSup.getValue().floatValue());
			palier.setValeur(dbVal.getValue().floatValue());

			palier.setGrille(grille);
			grille.getPaliers().add(palier);
			grilleService.save(grille);

		}else
		{
			palier.setBorneInferieure(dbInf.getValue().floatValue());
			palier.setBorneSuperieure(dbSup.getValue().floatValue());
			palier.setValeur(dbVal.getValue().floatValue());

			palier.setGrille(grille);
			grilleService.save(palier);
 		}
		
 			alert("Palier enregistr�");
 			
 			onChoice();
 					
		} catch (Exception e) {
			e.printStackTrace();
		}				
	}
	
	/**
	 * Switch des évenements lancés par un action button
	 * @param event
	 */
	@Listen(ON_CLICK_ACTION_BUTTON+" = window")
	public void onClickActionButton(Event event)
	{
		String action = (String) event.getData();

		if(action.equalsIgnoreCase(N_PALIER)){
 			
			palier=new Palier();	
			dbInf.setValue("0");
			dbSup.setValue("0");
			dbVal.setValue("0");
  		} 
		else if(action.equalsIgnoreCase(U_PALIER)){
		
			if(lstPaliers.getSelectedItem() != null){
 				
				palier=(Palier)lstPaliers.getSelectedItem().getValue();
			
				if(palier.getBorneInferieure()!=null)
   				dbInf.setValue(new BigDecimal(palier.getBorneInferieure()));
				
				if(palier.getBorneSuperieure()!=null)
	   				dbSup.setValue(new BigDecimal(palier.getBorneSuperieure()));

				if(palier.getValeur()!=null)
	   				dbVal.setValue(new BigDecimal(palier.getValeur()));

				
			}else
				alert("Attention, veuillez selectionner  un �l�ment d'abord");
			
		}
 		else if(action.equalsIgnoreCase(D_PALIER)){
			if(lstPaliers.getSelectedItem() != null){
				
				confirm("Etes vous sur de bien vouloire supprimer l element?", "Suppression action",
					
					new EventListener<Messagebox.ClickEvent>() {
					
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if(event.getButton() == Button.YES){
							
							try{
							
								palier=(Palier)lstPaliers.getSelectedItem().getValue();
								grilleService.supprimerPalier(palier);
								onChoice();
								
							}catch(Exception e){
								
								alert("Erreur lors de la Suppression");
								
							}
							
						}else if(event.getButton() == Button.NO) {
		
						}
					}	
				});
				
			}else
				alert("Attention, veuillez selectionner  un élément d abord");
			
		}
		
 
	} 
	
	 
	
}
