package sn.innov.web.parametrage.controllers;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.parametrage.entities.TypeService;
import sn.innov.be.referentiel.services.ReferentielCommonService;

public class ListTypeServiceController extends WindowController {
	
	@Wire
	Listbox lstTypeServices;
	
 	
	@Wire
	Textbox code,nom;
	
	@Wire
	Radiogroup mode;

  	
	/**
	 * Liste des actions de cet écran
	 */
	static final String N_TYPESERVICES = "N_TYPESERVICES", 
						U_TYPESERVICES = "U_TYPESERVICES",
						D_TYPESERVICES = "D_TYPESERVICES";
	
	
	TypeService service=null;
 	 
 	ReferentielCommonService referentielCommonTypeService = (ReferentielCommonService) JNDIUtils.lookUpEJB(EJBRegistry.ReferentielCommonServiceBean);
 	
 
	@Override
	public void doAfterCompose(Component comp) throws Exception {

		  super.doAfterCompose(comp);		
		  Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}
	
	
	/**
	 * Ecouteur de l'evenement onListChange pour recharger la liste des fonctions
	 */
	
	@Listen(EventConstants.ON_LIST_CHANGE+" = window")
	public void afficherListeTypeService()
	{
  		List<TypeService> services = referentielCommonTypeService.lister(TypeService.class);
   		ListModelList<TypeService> parteListModelList = new ListModelList<TypeService>(services);
   		lstTypeServices.setModel(parteListModelList);
	}
	
  
	@Listen("onValider = window")
	public void onValider(){
	
	try {
			
		if(service==null)
		{
			service=new TypeService();
  		} 
		
		
		service.setCode(code.getValue());
		service.setNom(nom.getValue());
		
		referentielCommonTypeService.save(service);
		
		alert("TypeService enregistre !");
 			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);

	}
	
	/**
	 * Switch des évenements lancés par un action button
	 * @param event
	 */
	@Listen(ON_CLICK_ACTION_BUTTON+" = window")
	public void onClickActionButton(Event event)
	{
		String action = (String) event.getData();

		if(action.equalsIgnoreCase(N_TYPESERVICES)){
 			
			service=new TypeService();
			code.setValue("");
			nom.setValue("");
			code.setReadonly(false);

   		} 
		else if(action.equalsIgnoreCase(U_TYPESERVICES)){
		
			if(lstTypeServices.getSelectedItem() != null){

				service=(TypeService)lstTypeServices.getSelectedItem().getValue();

				code.setValue(""+service.getCode());
				nom.setValue(""+service.getNom());
				code.setReadonly(true);
			
			}else
				alert("Attention, veuillez selectionner  un element d'abord");
			
		}
 		else if(action.equalsIgnoreCase(D_TYPESERVICES)){
			
 			if(lstTypeServices.getSelectedItem() != null){
				
				service=(TypeService)lstTypeServices.getSelectedItem().getValue();

				confirm("Etes vous sur de bien vouloire supprimer l'element?", "Suppression",
					new EventListener<Messagebox.ClickEvent>() {
					
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						
						if(event.getButton() == Button.YES){
							
 							if(referentielCommonTypeService.supprimer(TypeService.class,service.getCode()))
 								alert("Suppression effectu�e !");
 							else
 								alert("Erreur lors de la Suppression !");

 							
						}else if(event.getButton() == Button.NO) {
		
						}
					}	
				});
				
			}else
				alert("Attention, veuillez selectionner  un element d abord");
			
		}
		
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
 
	} 
	
	 
	
}
