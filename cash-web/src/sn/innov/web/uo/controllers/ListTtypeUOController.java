package sn.innov.web.uo.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Window;

import sn.innov.web.common.utils.EventConstants;
import sn.innov.web.main.controllers.WindowController;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.Fonctionnalite;
import sn.innov.be.security.services.FonctionnaliteService;
import sn.innov.be.uniteorganisationnelle.entities.TypeUO;
import sn.innov.be.uniteorganisationnelle.services.UOService;

public class ListTtypeUOController extends WindowController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Wire
	Listbox listTypeUO;
	
	/**
	 * Liste des actions de cet �cran
	 */
	static final String CREER_TYPEUNITE = "CREER_TYPEUO", 
						MODIFIER_TYPEUNITE = "MODIFIER_TYPEUO",
						SUPPRIMER_TYPEUNITE = "SUPPRIMER_TYPEUO";
	
	/**
	 * Ressources appel�es par cet ecran
	 */
	static final String	URI_FORM="/WEB-INF/uo/form.zul";
						

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}
	
	/**
	 * Ecouteur de l'evenement onListChange pour recharger la liste des fonctions
	 */
	@Listen(EventConstants.ON_LIST_CHANGE+" = window")
	public void afficherListeFonctionnalites()
	{
		UOService uOService = (UOService) JNDIUtils.lookUpEJB(EJBRegistry.UOServiceBean);
		List<TypeUO> typeServices = uOService.listerTypeUO();
		ListModelList<TypeUO> modelFonctionnalite = new ListModelList<TypeUO>(typeServices);
		listTypeUO.setModel(modelFonctionnalite);
	}
	
	/**
	 * Switch des �venements lanc�s par une action button
	 * @param event
	 */
	@Listen(ON_CLICK_ACTION_BUTTON+" = window")
	public void onClickActionButton(Event event)
	{
		String action = (String) event.getData();

		if(action.equalsIgnoreCase(CREER_TYPEUNITE)){
			HashMap<String, Object> params = new HashMap<String, Object>();
			
			params.put("typeuo", new TypeUO());
			loadPopUp(URI_FORM, params );
			
		}else if(action.equalsIgnoreCase(MODIFIER_TYPEUNITE)){
			if(listTypeUO.getSelectedItem() != null){ 
				HashMap<String, Object> params = new HashMap<String, Object>();
				
				params.put("typeuo", (TypeUO)listTypeUO.getSelectedItem().getValue()); 
				loadPopUp(URI_FORM, params );
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
			
		}else if(action.equalsIgnoreCase(SUPPRIMER_TYPEUNITE)){
			if(listTypeUO.getSelectedItem() != null){ 
				
				confirm("Etes vous sur de bien vouloire supprimer l element?", "Suppression fonctionnalite",
					new EventListener<Messagebox.ClickEvent>() {
					
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if(event.getButton() == Button.YES){
							
							Events.postEvent(EventConstants.ON_DELETE,((Window)getSelf().getSpaceOwner()), null);	
						}else if(event.getButton() == Button.NO) {
		
						}
					}	
				});
				
			}else
				alert("Attention, veuillez selectionner  un �l�ment d abord");
			
		}
	}
	
	/**
	 * Ecouteur de l'evenment: suppression confirm�e
	 */
	@Listen(EventConstants.ON_DELETE+" = window")
	public void supprimerFonctionnalites()
	{
		UOService typeUOService = (UOService) JNDIUtils.lookUpEJB(EJBRegistry.UOServiceBean);
		TypeUO typeUO = (TypeUO)listTypeUO.getSelectedItem().getValue();
		if(!typeUOService.supprimerTUO(typeUO)) 
		{alert("Suppression impossible");return;}
		else
		{
			alert("Suppression effectu�e");
		}
		Events.postEvent(EventConstants.ON_LIST_CHANGE,((Window)getSelf().getSpaceOwner()), null);
	}

}
