package sn.innov.web.core.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sn.innov.web.common.utils.SessionAttributes;
import sn.innov.be.authentification.entities.Utilisateur;

/**
 * Servlet Filter implementation class AuthenticationFilter
 */
@WebFilter("*.zul")
public class AuthenticationFilter implements Filter {

    /**
     * Default constructor. 
     */
    public AuthenticationFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, 
						ServletResponse response, 
						FilterChain chain) throws IOException, ServletException {
		String uri = ((HttpServletRequest)request).getRequestURI();
		HttpSession session = ((HttpServletRequest)request).getSession();
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(SessionAttributes.CONNECTED_USER);
		if(uri.endsWith("authentification.zul"))
		{			
			if(utilisateur!=null)
			{
				((HttpServletResponse)response).sendRedirect(request.getServletContext().getContextPath()+"/templates/mail/index.zul");
			}
			else
				chain.doFilter(request, response);
		}
		else
		{
			if(utilisateur==null)
			{
				((HttpServletResponse)response).sendRedirect(request.getServletContext().getContextPath()+"/authentification/authentification.zul");
			}
			else 
				chain.doFilter(request, response);
		}
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
