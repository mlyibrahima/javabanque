package sn.innov.web.core.bookmarks;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
@XmlRootElement
public class States implements Serializable {
List<State> states ;
@XmlTransient
private HashMap<String, State> statesDB = new HashMap<String, State>();

@XmlElement(name="state")
public List<State> getStates() {
	return states;
}
public void setStates(List<State> states) {
	this.states = states;
}


public State getState(String code)
{
	if(statesDB.containsKey(code))
	{
		return statesDB.get(code);
	}
	else
	{
		boolean trouve = false;
		int i=0;
		State res = null;
		while((i<states.size())&&(trouve==false))
		{			
			if(states.get(i).getBookmark().equals(code))
			{				
				trouve=true;
				res = states.get(i);				
			}
			i++;
		}
		if(res!=null)
		{
			statesDB.put(code, res);
			
		}
		return res;
			
	}
}
}
