package sn.innov.web.core.bookmarks;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class State implements Serializable{
String bookmark,title;
List<Zone> zones;
@XmlAttribute
public String getBookmark() {
	return bookmark;
}

public void setBookmark(String bookmark) {
	this.bookmark = bookmark;
}
@XmlAttribute
public String getTitle() {
	return title;
}

public void setTitle(String title) {
	this.title = title;
}

@XmlElement(name="zone")
public List<Zone> getZones() {
	return zones;
}

public void setZones(List<Zone> zones) {
	this.zones = zones;
}

}
