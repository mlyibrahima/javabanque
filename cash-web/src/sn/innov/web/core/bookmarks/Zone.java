package sn.innov.web.core.bookmarks;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

public class Zone implements Serializable{
String code,uri;

@XmlAttribute
public String getCode() {
	return code;
}

public void setCode(String code) {
	this.code = code;
}
@XmlAttribute
public String getUri() {
	return uri;
}

public void setUri(String uri) {
	this.uri = uri;
}

}
