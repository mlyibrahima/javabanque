package sn.innov.web.core.bookmarks;

import java.util.HashMap;

public class BookmarkContext {
	
	String bookMark;
	
	private HashMap<String, String> params = new HashMap<String, String>();

	public String  getParamValue(String param) {
		return params.get(param);
	}

	public void setParams(String name, String value) {
		params.put(name, value);
	}

	public HashMap<String, String> getParams() {
		return params;
	}

	public void setParams(HashMap<String, String> params) {
		this.params = params;
	}

	public String getBookMark() {
		return bookMark;
	}

	public void setBookMark(String bookMark) {
		this.bookMark = bookMark;
	}

	public BookmarkContext(String bookMark) {
		this.bookMark = bookMark;
	}
	
	
	
}
