package sn.innov.web.core.bookmarks;

import java.util.HashMap;

public class ZoneContext {
	HashMap<String, Object> params = new HashMap<String, Object>();

	public HashMap<String, Object> getParams() {
		return params;
	}

	public void setParams(HashMap<String, Object> params) {
		this.params = params;
	}
	
}
