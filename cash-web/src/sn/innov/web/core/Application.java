package sn.innov.web.core;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import sn.innov.web.common.utils.ContextAttributes;
import sn.innov.web.core.bookmarks.States;

@WebListener
public class Application implements ServletContextListener{
	
	

@Override
	public void contextInitialized(ServletContextEvent event) {
	System.out.println("Loading bookmarks");
	try {
		JAXBContext jaxbContext = JAXBContext.newInstance(States.class); 
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		
		States states = (States) jaxbUnmarshaller.unmarshal(event.getServletContext().getResourceAsStream("/WEB-INF/application-states.xml"));
		System.out.println("States "+states.getStates().size());
		event.getServletContext().setAttribute(ContextAttributes.STATES, states);
	} catch (JAXBException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}

@Override
public void contextDestroyed(ServletContextEvent arg0) {
	// TODO Auto-generated method stub
	
}

}
