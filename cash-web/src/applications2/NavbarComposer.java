package applications2;



import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

import org.zkoss.image.AImage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.A;
import org.zkoss.zul.Image;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Span;
import org.zkoss.zul.Vlayout;

import sn.innov.be.authentification.entities.Utilisateur;
import sn.innov.be.core.utils.EJBRegistry;
import sn.innov.be.core.utils.JNDIUtils;
import sn.innov.be.security.entities.LogAction;
import sn.innov.be.security.services.LogActionService;
import sn.innov.web.common.utils.SessionAttributes;
import sn.innov.web.common.utils.UIConstants;
import sn.innov.web.components.MacroMsge;
import sn.innov.web.main.controllers.WindowController;


public class NavbarComposer extends WindowController {
	
	@Wire
	A  amsg;
	@Wire
	Menu userMenu ;
	@Wire
	Vlayout vlayoutMsge;
	
	
	static final String URI_MOD = "/WEB-INF/securite/referentiel/utilisateurs/modpassword.zul";
	

	
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		Utilisateur user = (Utilisateur) Executions.getCurrent().getSession().getAttribute(SessionAttributes.CONNECTED_USER) ;
		userMenu.setLabel(user.getLogin());
		
		String imgPath = UIConstants.IMAGES_PROFIL_PATH+user.getImageProfil() ;
		
		Image img = new Image();
        try {
			img.setContent( new AImage( "t" , new FileInputStream(imgPath)));
			userMenu.setImageContent( img.getContent());
		} catch (IOException e1) {

			//e1.printStackTrace();
		}
	}
	
	
	
	@Listen("onOpen = #msgpp")
	public void toggleMsgPopup(OpenEvent event) {
		toggleOpenClass(event.isOpen(), amsg);
		LogActionService las = (LogActionService) JNDIUtils.lookUpEJB(EJBRegistry.LogActionServiceBean) ;
		Utilisateur user = (Utilisateur) Executions.getCurrent().getSession().getAttribute(SessionAttributes.CONNECTED_USER) ;
		Components.removeAllChildren(vlayoutMsge);
		for(LogAction log: las.findLogAction(user.getId(),0,5) ){
			MacroMsge a = new MacroMsge(log) ;
			a.setParent(vlayoutMsge) ;
		}
		
		A allactions = new A() ;
		//<a label="See all messages" href="#"><span class="z-icon-arrow-right"/></a>
		Span s = new Span() ;
		s.setClass("z-icon-arrow-right") ;
		allactions.setLabel("Voir tous les logs") ;
		allactions.setParent(vlayoutMsge);
		allactions.addEventListener("onClick", new EventListener<Event>() {

			@Override
			public void onEvent(Event e) throws Exception {
				Events.postEvent("onLoadBookmark",(Component) getSelf().getRoot(),"liste_logs");
				getSelf().getRoot().getPage().getDesktop().setBookmark("liste_logs",false);
				toggleOpenClass(false, amsg);
				
				
			}
		});
		
		
	}
	
	

	// Toggle open class to the component
	public void toggleOpenClass(Boolean open, Component component) {
		HtmlBasedComponent comp = (HtmlBasedComponent) component;
		String scls = comp.getSclass();
		if (open) {
			comp.setSclass(scls + " open");
		} else {
			comp.setSclass(scls.replace(" open", ""));
			Components.removeAllChildren(vlayoutMsge);
		}
	}
	
	@Listen("onClick = #menuLogout")
	public void logout() {
		confirm("Etes-vous sûr de bien vouloir vous déconnecter?",
				null, new EventListener<Messagebox.ClickEvent>() {

					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if (event.getButton() == Button.YES) {
							
							Sessions.getCurrent().invalidate();
							Executions.sendRedirect("/authentication/authentication.zul");
						}
						
					}
				});

	}
	
	@Listen(" onClick = #menuCustomisationProfil ")
	public void customiserProfil(){
		Events.postEvent("onLoadBookmark",(Component) getSelf().getRoot(),"customisation_profils");
		getSelf().getRoot().getPage().getDesktop().setBookmark("customisation_profils",false);
		
	}
	

	@Listen("onClick = #menuChangementMotDePasse")
	public void onChangePassword() {
		try {
			 confirm("Etes-vous sêr de bien vouloir changer votre mot de passe?", "Changement du mot de passe",
						new EventListener<Messagebox.ClickEvent>() {

							@Override
							public void onEvent(ClickEvent event) throws Exception {
								if (event.getButton() == Button.YES) {
									HashMap<String, Object> params = new HashMap<String, Object>();
									loadPopUp(URI_MOD, params );
								}
							}
						});
		} catch (Exception e) {e.printStackTrace();}
	}
}
