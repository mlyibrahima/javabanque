package service;

import java.util.List;

import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import sn.innov.web.banque.entities.Client;
import sn.innov.web.banque.entities.Compte;
import sn.innov.web.banque.entities.Operation;
import sn.innov.web.banque.services.ClientRemote;

@WebService
public class BanqueService {
	@EJB
	private ClientRemote metier;
	
	public void addClient(Client c) {
		metier.addClient(c);
	}
	public List<Client> consulterClientsParNom(String mc) {
		return metier.consulterClientsParNom(mc);
	}
	public Client consulterClient(Long idClient) {
		return metier.consulterClient(idClient);
	}
	public List<Client> consulterClients() {
		return metier.consulterClients();
	}
	public void supprimerClient(Long idClient) {
		metier.supprimerClient(idClient);
	}
	public void addCompte(Compte c) {
		metier.addCompte(c);
	}
	public Compte consulterCompte(Long idCompte) {
		return metier.consulterCompte(idCompte);
	}
	public List<Compte> consulterComptes() {
		return metier.consulterComptes();
	}
	/*public List<Compte> consulterComptes(Long idClient) {
		return metier.consulterComptes(idClient);
	}*/
	/*public void addOperation(Operation op, Long idCompte) {
		metier.addOperation(op, idCompte);
	}*/
	public List<Operation> consulterOperations() {
		return metier.consulterOperations();
	}
	/*public List<Operation> consulterOperationsParCompte(Long idCompte) {
		return metier.consulterOperationsParCompte(idCompte);
	}*/
	/*public void virement(Compte c1, Compte c2, Double montant) {
		metier.virement(c1, c2, montant);
	}*/
	public void prelevement(Compte c1, Compte c2, Double montant) {
		metier.prelevement(c1, c2, montant);
	}
	
	@WebMethod
	public void verser(
			@WebParam(name = "idCompte")Long idCompte,
			@WebParam(name = "montant")double montant) {
		metier.verser(idCompte, montant);
	}
	@WebMethod
	public void retirer(
			@WebParam(name = "idCompte")Long idCompte,
			@WebParam(name = "montant")double montant) {
		metier.retirer(idCompte, montant);
	}
	@WebMethod
	public void virement(
			@WebParam(name = "compte1")Compte c1,
			@WebParam(name = "compte2")Compte c2,
			@WebParam(name = "montant")double montant) {
		metier.virement(c1, c2, montant);
	}

}
